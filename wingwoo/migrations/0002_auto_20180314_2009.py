# Generated by Django 2.0.1 on 2018-03-14 12:09

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wingwoo', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='carpetpurchaseorder',
            name='carpet_type',
        ),
        migrations.AlterField(
            model_name='occupiedtimeslot',
            name='modified_by',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
    ]
