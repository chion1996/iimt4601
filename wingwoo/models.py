import datetime
from decimal import *
from django.db import models, transaction
from django.conf import settings
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.urls import reverse
from django.core.files import File
from templated_email import send_templated_mail, InlineImage
from .choices import *
from .utils import *
import threading
import os

from django_nyt.utils import notify

from abc import ABCMeta, abstractmethod

# Create your models here.
class Member(models.Model):
	user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
	name = models.CharField(max_length=50, null=True, blank=True)
	phone = models.CharField(max_length=15, null=True, blank=True)

	def get_display_name(self):
		return str(self.name)

	def __str__(self):
		return self.get_display_name()

# @receiver(post_save, sender=User)
# def update_user_member(sender, instance, created, **kwargs):
#     if created:
#         Member.objects.create(user=instance)
#     instance.member.save()

class Address(models.Model):
	district = models.IntegerField(choices=ADDRESS_DISTRICT_CHOICES, default=ADDRESS_DISTRICT_SAIKUNG)
	pier = models.CharField(max_length=200)

	def get_detail(self):
		detail = {
			'district': self.district,
			'pier': self.pier
		}

		return detail

	def get_str(self):
		return '[' + self.get_district_display() + '] ' + self.pier

	def __str__(self):
		return self.get_str()

class Boat(models.Model):
	member = models.ForeignKey(Member, on_delete=models.CASCADE)
	address = models.ForeignKey(Address, on_delete=models.CASCADE)
	model = models.CharField(max_length=80)
	num_related_booking = models.IntegerField(default=0)
	locked = models.BooleanField(default=False)
	removed = models.BooleanField(default=False)

	@classmethod
	def create(cls, member, address, model):
		boat = cls(
			member = member, 
			address = address, 
			model = model,
			locked = False,
			num_related_booking = 0
		)
		boat.save()

		MemberLog.create(member, 'Added a new boat')

		return boat

	def get_detail(self, for_admin):
		detail = {
			'id': self.id,
			'model': self.model
		}

		if for_admin:
			detail['address'] = self.address.get_detail()
		else:
			detail['district'] = self.address.district

		return detail

	def edit(self, address, model):
		if self.num_related_booking > 0:
			raise Exception('Boat cannot be edited when it is related to a scheduled measurement booking')

		self.address = address
		self.model = model
		self.save()
		MemberLog.create(self.member, 'Edited boat#' + str(self.id) + ' ' + str(self.model))

	def remove(self):
		if self.num_related_booking > 0:
			raise Exception('Boat cannot be removed when it is related to a scheduled measurement booking')
		
		if Order.objects.filter(boat = self).count() > 0:
			self.removed = True
			self.save()
		else:
			self.delete()

		MemberLog.create(self.member, 'Removed boat#' + str(self.id) + ' ' + str(self.model))

	def get_address_display(self):
		return self.address.get_str()

	def increase_num_related_booking(self):
		self.num_related_booking += 1
		self.save()

	def decrease_num_related_booking(self):
		self.num_related_booking -= 1
		self.save()

	def __str__(self):
		return self.member.get_display_name() + '\'s ' + self.model

class ProductFeature(models.Model):
	description = models.CharField(max_length=100)

	def __str__(self):
		return self.description

class Product(models.Model):
	product_type = models.IntegerField(choices=PRODUCT_TYPE_CHOICES, default=PRODUCT_TYPE_GENERAL)
	name = models.CharField(max_length=50)
	description = models.CharField(max_length=300, null=True, blank=True)
	feature = models.ManyToManyField(ProductFeature, through='ProductFeatureLine')
	area_calculation = models.BooleanField(default=True)
	purchase_unit_price = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)

	def get_dimension(self, length, width, height):
		if self.area_calculation == True:
			dimension = get_max_area(length, width, height)
		else:
			dimension = get_max_length(length, width, height)
		return dimension

	def get_price(self, size, color, pattern, waterproof, underlay, edge):
		if self.product_type == PRODUCT_TYPE_GENERAL:
			return GeneralProduct.objects.get(id = self.id).get_price(size)
		else:
			return CarpetProduct.objects.get(id = self.id).get_price(size, pattern, waterproof, underlay, edge)

	def get_price_detail(self, length, width, height, pattern, waterproof, underlay, edge, quantity):
		if self.product_type == PRODUCT_TYPE_GENERAL:
			return GeneralProduct.objects.get(id = self.id).get_price_detail(length, width, height, quantity)
		else:
			return CarpetProduct.objects.get(id = self.id).get_price_detail(length, width, height, pattern, waterproof, underlay, edge, quantity)

	def __str__(self):
		return self.name

class GeneralProduct(Product):
	purchase_unit_price_for_color = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)

	def get_price_base(self, length, width, height):
		dimension = self.get_dimension(length, width, height)

		unit_price = self.purchase_unit_price + self.purchase_unit_price_for_color

		return dimension * unit_price

	def get_price(self, size):
		return self.get_price_base(size.length, size.width, size.height)

	def get_price_detail(self, length, width, height, quantity):
		detail = {
			'purchase_unit_price': self.purchase_unit_price,
			'purchase_unit_price_for_color': self.purchase_unit_price_for_color,
			'dimension': self.get_dimension(length, width, height),
			'total_unit_price': self.get_price_base(length, width, height),
			'quantity': quantity
		}

		return detail

class CarpetProduct(Product):
	shaggy_pattern_purchase_unit_price = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
	pile_pattern_purchase_unit_price = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
	waterproof_purchase_unit_price = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
	underlay_purchase_unit_price = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
	edge_purchase_unit_price = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)

	def get_pattern_price(self, pattern, length, width, height):
		area = get_max_area(length, width, height)
		if pattern.pattern_type == CARPET_TYPE_SHAGGY:
			return self.shaggy_pattern_purchase_unit_price * area
		else:
			return self.pile_pattern_purchase_unit_price * area

	def get_waterproof_price(self, waterproof, length, width, height):
		if waterproof == True:
			return self.waterproof_purchase_unit_price * get_max_area(length, width, height)
		else:
			return Decimal(0.0)

	def get_underlay_price(self, underlay, length, width, height):
		if underlay == True:
			return self.underlay_purchase_unit_price * get_max_area(length, width, height)
		else:
			return Decimal(0.0)

	def get_edge_price(self, edge, length, width, height):
		if edge == True:
			return self.edge_purchase_unit_price * get_max_perimeter(length, width, height)
		else:
			return Decimal(0.0)

	def get_price_base(self, length, width, height, pattern, waterproof, underlay, edge):
		area = get_max_area(length, width, height)

		add_on_price = self.get_pattern_price(pattern, length, width, height)
		add_on_price = add_on_price + self.get_waterproof_price(waterproof, length, width, height)
		add_on_price = add_on_price + self.get_underlay_price(underlay, length, width, height)
		add_on_price = add_on_price + self.get_edge_price(edge, length, width, height)

		return area * self.purchase_unit_price + add_on_price

	def get_price(self, size, pattern, waterproof, underlay, edge):
		return self.get_price_base(size.length, size.width, size.height, pattern, waterproof, underlay, edge)

	def get_price_detail(self, length, width, height, pattern, waterproof, underlay, edge, quantity):
		detail = {
			'dimension': self.get_dimension(length, width, height),
			'purchase_unit_price': self.purchase_unit_price,

			'pattern_price': self.get_pattern_price(pattern, length, width, height),

			'waterproof_unit_price': self.waterproof_purchase_unit_price,
			'waterproof_price': self.get_waterproof_price(waterproof, length, width, height),

			'underlay_unit_price': self.underlay_purchase_unit_price,
			'underlay_price': self.get_underlay_price(underlay, length, width, height),

			'edge_unit_price': self.edge_purchase_unit_price,
			'edge_price': self.get_edge_price(edge, length, width, height),

			'total_unit_price': self.get_price_base(length, width, height, pattern, waterproof, underlay, edge),
			'quantity': quantity
		}

		if pattern.pattern_type == CARPET_TYPE_SHAGGY:
			detail['pattern_unit_price'] = self.shaggy_pattern_purchase_unit_price
		else:
			detail['pattern_unit_price'] = self.pile_pattern_purchase_unit_price

		return detail

class ProductImage(models.Model):
	product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='images')
	image = models.ImageField(upload_to='image/product/')

class ProductFeatureLine(models.Model):
	product = models.ForeignKey(Product, on_delete=models.CASCADE)
	product_feature = models.ForeignKey(ProductFeature, on_delete=models.CASCADE)

	def __str__(self):
		return self.product.name + ": " + self.product_feature.description

class Color(models.Model):
	code = models.CharField(max_length=50)
	name = models.CharField(max_length=50)
	image = models.ImageField(upload_to='image/color/')

	def get_detail(self):
		detail = {
			'id': self.id,
			'code': self.code,
			'name': self.name,
			'image': static(self.image.url)
		}

		return detail

	def __str__(self):
		return self.code + " " + self.name

class CarpetPattern(models.Model):
	code = models.CharField(max_length=50)
	name = models.CharField(max_length=50)
	image = models.ImageField(upload_to='image/color/')
	pattern_type =  models.IntegerField(choices=CARPET_TYPE_CHOICES, default=CARPET_TYPE_SHAGGY)

	def get_detail(self):
		detail = {
			'id': self.id,
			'code': self.code,
			'name': self.name,
			'image': static(self.image.url)
		}

		return detail

	def __str__(self):
		return "[" + self.get_pattern_type_display() + "] " + self.code + " " + self.name

class OccupiedTimeslot(models.Model):
	date = models.DateField(default=datetime.date.today)
	timeslot = models.IntegerField(choices=BOOKING_TIMESLOT_CHOICES, default=BOOKING_TIMESLOT_1)
	timeslot_type = models.IntegerField(choices=OCCUPIED_TIMESLOT_TYPE_CHOICES, default=OCCUPIED_TIMESLOT_TYPE_BOOKING)
	modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, blank=True)
	modified_at = models.DateTimeField(auto_now=True)
	created_at = models.DateTimeField(auto_now_add=True)

	@classmethod
	def is_datetime_available(cls, date, timeslot):
		if OccupiedTimeslot.objects.filter(Q(date = date) & Q(timeslot = timeslot)).count() > 0:
			return { 'result': False, 'message': 'The time slot has been scheduled' }

		return { 'result': True }

	@classmethod
	def is_datetime_available_edit(cls, self_id, date, timeslot):
		if OccupiedTimeslot.objects.filter(Q(date = date) & Q(timeslot = timeslot)).exclude(pk = self_id).count() > 0:
			return { 'result': False, 'message': 'The time slot has been scheduled' }

		return { 'result': True }

	@classmethod
	def get_timeslot_choice(cls, timeslot_string):
		timeslot_choice = None

		for choice in BOOKING_TIMESLOT_CHOICES:
			if choice[1][0:5] == timeslot_string[0:5]:
				timeslot_choice = choice[0]

		return timeslot_choice

	@classmethod
	def get_timeslots(cls, district, start_date, end_date):
		if district != None:
			if district  == 0:
				return OccupiedTimeslot.objects.filter(Q(date__week_day__in=[2, 4]) & Q(date__gte = start_date) & Q(date__lte = end_date))
			elif district == 1 or district == 2:
				return OccupiedTimeslot.objects.filter(Q(date__week_day__in=[3, 5]) & Q(date__gte = start_date) & Q(date__lte = end_date))
			else:
				return OccupiedTimeslot.objects.filter(Q(date__week_day__in=[6]) & Q(date__gte = start_date) & Q(date__lte = end_date))
		else:
			return OccupiedTimeslot.objects.filter(Q(date__gte = start_date) & Q(date__lte = end_date))

	@classmethod
	def get_timeslots_as_event_data(cls, district, start_date, end_date, user):
		timeslots = cls.get_timeslots(district, start_date, end_date)

		events = []
		for timeslot in timeslots:
			start_time = timeslot.date.isoformat() + "T" + timeslot.get_timeslot_display()[0:5]
			end_time = timeslot.date.isoformat() + "T" + timeslot.get_timeslot_display()[8:13]

			data = {
				'id': timeslot.id,
				'start': start_time,
				'end': end_time
			}

			if user.is_staff:
				show_detail = True
				data['className'] = 'ot-event' if timeslot.timeslot_type == OCCUPIED_TIMESLOT_TYPE_EVENT else 'ot-measurementbooking'
			else:
				data['className'] = 'ot-event'
				show_detail = False
				if timeslot.timeslot_type != OCCUPIED_TIMESLOT_TYPE_EVENT:
					try:
						if MeasurementBooking.objects.get(id = timeslot.id).member == user.member:
							show_detail = True
					except ObjectDoesNotExist:
						show_detail = False

			if show_detail:
				data['title'] = timeslot.get_title()
			else:
				data['title'] = 'Unavailable'
				
			events.append(data)

		#print(events)
		return events

	@classmethod
	def get_detail_by_id(cls, id):
		return OccupiedTimeslot.objects.get(id = id).get_detail()

	def get_title(self):
		if self.timeslot_type == OCCUPIED_TIMESLOT_TYPE_EVENT:
			return Event.objects.get(id = self.id).get_title()
		else:
			return MeasurementBooking.objects.get(id = self.id).get_title()

	def get_detail(self):
		if self.timeslot_type == OCCUPIED_TIMESLOT_TYPE_EVENT:
			detail = Event.objects.get(id = self.id).get_detail()
		else:
			detail = MeasurementBooking.objects.get(id = self.id).get_detail(True)

		detail['date'] = self.date
		detail['timeslot'] = self.get_timeslot_display()
		detail['timeslot_type'] = self.get_timeslot_type_display()

		return detail
	
	def __str__(self):
		return "#" + str(self.id) + " " + str(self.date) + " " + self.get_timeslot_display()

class Event(OccupiedTimeslot):
	title = models.CharField(max_length=100)

	@classmethod
	def create(cls, date, timeslot, modified_by, title):
		# Checking whether there is time clash with existing bookings
		available_result_data = OccupiedTimeslot.is_datetime_available(date, timeslot)
		if available_result_data.result == False:
			raise Exception(available_result_data.message)
		#

		event = cls(
			date = date,
			timeslot = timeslot,
			timeslot_type = OCCUPIED_TIMESLOT_TYPE_EVENT, 
			modified_by = modified_by,
			occupiedtimeslot_ptr = occuiped_timeslot,
			title = title
		)
		event.save()

		return event

	def get_title(self):
		return self.title

	def get_detail(self):
		return {
			'title': self.title
		}

	def __str__(self):
		return "#" + str(self.id) + " " + self.title + " on " + str(self.date) + " " + self.get_timeslot_display()

class MeasurementBooking(OccupiedTimeslot):
	member = models.ForeignKey(Member, on_delete=models.PROTECT)
	status = models.IntegerField(choices=BOOKING_STATUS_CHOICES, default=BOOKING_STATUS_SCHEDULED)
	
	@classmethod
	def is_date_creatable(cls, date):
		if (date.date() - datetime.date.today()).days < 3:
			return { 'result': False, 'message': 'You can only book the timeslot 2 days afterwards' }

		return { 'result': True }

	@classmethod
	def is_datetime_allowed(cls, district, date, timeslot):
		weekday = date.weekday()
		if district == ADDRESS_DISTRICT_SAIKUNG:
			if (weekday != 0) and (weekday != 2):
				return  { 'result': False, 'message': 'Available time slots for Sai Kung district are on Mondays and Wednesdays only' }
		elif (district == ADDRESS_DISTRICT_HONGKONGISlAND) or (district == ADDRESS_DISTRICT_TUENMUN):
			if (weekday != 1) and (weekday != 3):
				return { 'result': False, 'message': 'Available time slots for Hong Kong Island and Tuen Mun districts are on Tuesdays and Thursdays only' }
		else:
			if weekday != 4:
				return { 'result': False, 'message': 'Available time slots for other districts are on Fridays only' }

		return { 'result': True }

	@classmethod
	def create(cls, member, order, date, timeslot):
		# Checking whether there is time clash with existing bookings
		available_result_data = OccupiedTimeslot.is_datetime_available(date, timeslot)
		if available_result_data['result'] == False:
			raise Exception(available_result_data['message'])
		#

		# Check whether the chosen date is allowed or not
		creatable_result_data = MeasurementBooking.is_date_creatable(date)
		if creatable_result_data['result'] == False:
			raise Exception(creatable_result_data['message'])
		#

		# Check whether the order is waiting for booking measurement or not
		if order.status > ORDER_STATUS_WAIT_FOR_PAYMENT:
			raise Exception('The selected order has been paid')
		#

		district = order.boat.address.district

		# Check whether the weekday matches with the district
		allowed_result_data = MeasurementBooking.is_datetime_allowed(district, date, timeslot)
		if allowed_result_data['result'] == False:
			raise Exception(allowed_result_data['message'])
		#

		# Create
		target_address = order.boat.address

		created_measurement_booking = cls(
			date = date,
			timeslot = timeslot,
			timeslot_type = OCCUPIED_TIMESLOT_TYPE_BOOKING, 
			modified_by = member.user,
			member = member,
			status = BOOKING_STATUS_SCHEDULED
		)
		created_measurement_booking.save()

		boats = set()

		# Update related orders
		related_orders = Order.objects.select_for_update().filter(
			Q(member = member) &
			Q(boat__address = target_address) &
			Q(status = ORDER_STATUS_BOOK_MEASUREMENT)
		).select_related('boat')

		for related_order in related_orders:
			related_order.proceed_to_measurement(member, created_measurement_booking)
			boats.add(related_order.boat)
		#

		# Update related boats
		for boat in boats:
			boat.increase_num_related_booking()
		#

		notify(
	        member.get_display_name() + 
	        " booked a measurement booking #" + 
	        str(created_measurement_booking.id) + 
	        " on " + created_measurement_booking.date.strftime('%Y-%m-%d') + 
	        ' ' + created_measurement_booking.get_timeslot_display() + '.',

	        "admin"
	    )


		MemberLog.create(created_measurement_booking.member, 'Created measurement booking #' + 
			str(created_measurement_booking.id) + ' scheduled on ' + 
			str(created_measurement_booking.date) + " " + 
			created_measurement_booking.get_timeslot_display() + " @ " + 
			str(created_measurement_booking.get_address())
		)

		return created_measurement_booking

	def is_deletable(self):
		if self.status == BOOKING_STATUS_SCHEDULED:
			if (self.date - datetime.date.today()).days < 2:
				return { 'result': False, 'message': 'You can only edit 2 day before the booked date' }
			else:
				return { 'result': True }
		else:
			return { 'result': False, 'message': 'Finished booking cannot be deleted' }

	def edit(self, date, timeslot):
		# Check whether the chosen date is allowed or not
		deletable_result_data = self.is_deletable()
		if deletable_result_data['result'] == False:
			raise Exception(deletable_result_data['message'])

		creatable_result_data = MeasurementBooking.is_date_creatable(date)
		if creatable_result_data['result'] == False:
			raise Exception(creatable_result_data['message'])
		#

		district = self.get_address().district

		# Check whether the weekday matches with the district
		allowed_result_data = MeasurementBooking.is_datetime_allowed(district, date, timeslot)
		if allowed_result_data['result'] == False:
			raise Exception(allowed_result_data['message'])
		#

		# Checking whether there is time clash with existing bookings
		available_result_data = MeasurementBooking.is_datetime_available(date, timeslot)
		if available_result_data['result'] == False:
			raise Exception(available_result_data['message'])
		#

		self.date = date
		self.timeslot = timeslot
		self.save()

		notify(
	        self.member.get_display_name() + 
	        " edited measurement booking #" + 
	        str(self.id) + 
	        ", scheduled on " + self.date.strftime('%Y-%m-%d') + 
	        ' ' + self.get_timeslot_display() + '.',

	        "admin"
	    )

		MemberLog.create(self.member, 'Edited measurement booking #' + 
			str(self.id) + ', scheduled on ' + 
			str(self.date) + " " + 
			self.get_timeslot_display() + " @ " + 
			str(self.get_address())
		)

	def finish(self, admin):
		self.status = BOOKING_STATUS_FINISHED
		self.modified_by = admin
		self.save()

		boats = set()

		related_orders = Order.objects.select_for_update().filter(measurement_booking = self)
			
		for related_order in related_orders:
			boats.add(related_order.boat)

		for boat in boats:
			boat.decrease_num_related_booking()

		notify(
	        "Your measurement booking #" + str(self.id) + " has been finished.", 
	        self.member.user.username + "_normal",
	        target_object = self.member.user,
	        url = reverse('orders')
	    )

		EmailThread(self.member.user.username,
		"Measurement booking finished",
		"your measurement booking #" + str(self.id) + " has been finished.",
		self.member.user.email	
		).start()

	def fee_received(self):
		related_orders = Order.objects.filter(measurement_booking = self)
		for related_order in related_orders:
			if related_order.status in [ORDER_STATUS_MANUFACTURING, ORDER_STATUS_READY_TO_DELIVER, ORDER_STATUS_RECEIVED]:
				return True

	def is_receivable(self):
		if self.status == BOOKING_STATUS_SCHEDULED:
			return False

		if (datetime.date.today() - self.date).days <= 120:
			return False

		if self.fee_received():
			return False

		return True

	def create_receivable(self):
		related_orders = Order.objects.select_for_update().filter(measurement_booking = self)

		for related_order in related_orders:
			related_order.status = ORDER_STATUS_UNFULFILLED
			related_order.save()
			
		AccountReceivable.create(self.member, Decimal('300.0'), self)

	def remove(self, who):
		# Checking whether it is allowed
		deletable_result_data = self.is_deletable()
		if deletable_result_data['result'] == False:
			raise Exception(deletable_result_data['message'])
		#

		boats = set()

		related_orders = Order.objects.select_for_update().filter(measurement_booking = self)
		
		for related_order in related_orders:
			related_order.cancel_booking(who)
			boats.add(related_order.boat)

		for boat in boats:
			boat.decrease_num_related_booking()

		if self.status == BOOKING_STATUS_FINISHED:
			if who == self.member.user:
				AccountReceivable.create(self.member, Decimal('300.0'), self)

		if who == self.member.user:
			MemberLog.create(self.member, 'Removed measurement booking #' + 
				str(self.id) + ' which was scheduled on ' + 
				str(self.date) + " " + 
				self.get_timeslot_display() + " @ " + 
				str(self.get_address())
			)
			
			notify(
		        self.member.get_display_name() + 
		        " removed measurement booking #" + 
		        str(self.id) + 
		        "which was scheduled on " + self.date.strftime('%Y-%m-%d') + 
		        ' ' + self.get_timeslot_display() + '.',

		        "admin"
		    )

		self.delete()

	def get_title(self):
		return self.member.get_display_name() + " @ " + str(self.get_address())

	def get_address(self):
		related_orders = Order.objects.filter(measurement_booking = self)
		if related_orders.first() == None:
			return None
		else:
			return related_orders.first().boat.address

	def get_address_detail(self):
		related_orders = Order.objects.filter(measurement_booking = self)
		if related_orders.first() == None:
			return None
		else:
			return related_orders.first().boat.address.get_detail()

	def get_extra_detail(self, for_admin):
		order_details = []
		address = None

		related_orders = Order.objects.filter(measurement_booking = self)
		for related_order in related_orders:
			address = related_order.boat.get_address_display()
			if for_admin:
				order_details.append(related_order.get_detail(False))
			else:
				order_details.append(related_order.get_detail_for_booking())

		return order_details

	def get_detail(self, for_admin):
		detail = {
			'id': self.id,
			'date': self.date,
			'timeslot': self.timeslot,
			'status': self.get_status_display(),
			'orders': self.get_extra_detail(for_admin),
			'removable': self.is_deletable()['result']
		}

		if for_admin:
			detail['member'] = self.member.get_display_name()
			detail['address'] = str(self.get_address())
		else:
			detail['address'] = self.get_address_detail()

		return detail

	def __str__(self):
		return "#" + str(self.id) + " " + self.member.get_display_name() + "\'s ' booking on " + str(self.date) + " " + self.get_timeslot_display() + " @ " + str(self.get_address())

class Order(models.Model):
	member = models.ForeignKey(Member, on_delete=models.PROTECT)
	order_type = models.IntegerField(choices=ORDER_TYPE_CHOICES, default=ORDER_TYPE_PURCHASE)
	product = models.ForeignKey(Product, on_delete=models.PROTECT, null=False, blank=False)
	boat = models.ForeignKey(Boat, on_delete=models.PROTECT)
	status = models.IntegerField(choices=ORDER_STATUS_CHOICES, default=ORDER_STATUS_BOOK_MEASUREMENT)
	locked = models.BooleanField(default=False)
	measurement_booking = models.ForeignKey(MeasurementBooking, on_delete=models.SET_NULL, default=None, null=True, blank=True, related_name="in_booking")
	modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, blank=True)
	modified_at = models.DateTimeField(auto_now=True)
	created_at = models.DateTimeField(auto_now_add=True)

	def get_removable_data(self):
		data = {
			'removable': True,
			'only_related': False,
			'payable': False
		}

		if self.status >= ORDER_STATUS_MANUFACTURING:
			data['removable'] = False

		if self.measurement_booking != None:
			if Order.objects.filter(measurement_booking = self.measurement_booking).count() == 1:
				data['only_related'] = True

				if self.measurement_booking.status == BOOKING_STATUS_FINISHED:
					data['payable'] = True

				days_diff = (self.measurement_booking.date - datetime.date.today()).days
				if days_diff < 2 and days_diff > 0:
					data['removable'] = False
				else:
					data['removable'] = True
		return data

	def create_receivable(self):
		if not self.is_account_receivable():
			raise Exception('The order is not eligible')

		self.measurement_booking.create_receivable()

	def is_account_receivable(self):
		if self.status == ORDER_STATUS_WAIT_FOR_PAYMENT:
			if self.measurement_booking != None:
				if self.measurement_booking.is_receivable():
					return True
		return False 

	def remove(self, who):
		if self.status >= ORDER_STATUS_MANUFACTURING:
			raise Exception('Order cannot be removed when it is after payment')

		if self.measurement_booking != None:
			if Order.objects.filter(measurement_booking = self.measurement_booking).count() == 1:
				self.measurement_booking.remove(who)

		if who == self.member.user:
			MemberLog.create(self.member, 'Removed order #' + str(self.id))

		self.delete()

	def proceed_to_measurement(self, member, measurement_booking):
		self.measurement_booking = measurement_booking
		self.status = ORDER_STATUS_WAIT_FOR_QUOTATION
		self.modified_by = member.user
		self.locked = True
		self.save()

	def quote(self, length, width, height, repair_price, admin):
		if self.status < ORDER_STATUS_WAIT_FOR_PAYMENT:
			self.status = ORDER_STATUS_WAIT_FOR_PAYMENT
		self.modified_by = admin
		self.locked = True
		self.save()

		#print("order quote " + str(length) + ' ' + str(width) + ' ' + str(height))

		if self.order_type == ORDER_TYPE_PURCHASE:
			PurchaseOrder.objects.get(id = self.id).quote(length, width, height, admin)
		else:
			RepairOrder.objects.get(id = self.id).quote(length, width, height, repair_price, admin)

		notify(
	        "Your order#" + str(self.id) + " has been quoted.", 
	        self.member.user.username + "_normal",
	        target_object = self.member.user,
	        url = reverse('orders')
	    )

		EmailThread(self.member.user.username,
		"Order quoted",
		"your order#" + str(self.id) + " has been quoted.",
		self.member.user.email	
		).start()

	def made(self, admin):
		if self.status < ORDER_STATUS_READY_TO_DELIVER:
			self.status = ORDER_STATUS_READY_TO_DELIVER
			self.modified_by = admin
			self.save()

			notify(
		        "Your order#" + str(self.id) + " has been made.", 
		        self.member.user.username + "_normal",
		        target_object = self.member.user,
		        url = reverse('orders')
		    )

			EmailThread(self.member.user.username,
			"Order made",
			"your order#" + str(self.id) + " has been made.",
			self.member.user.email	
			).start()

	def received_payment(self, admin):
		if self.status < ORDER_STATUS_MANUFACTURING:
			self.status = ORDER_STATUS_MANUFACTURING
			self.modified_by = admin
			self.save()

			notify(
		        "Payment of your order#" + str(self.id) + " has been received. Thank You!", 
		        self.member.user.username + "_normal",
		        target_object = self.member.user,
		        url = reverse('orders')
		    )

			EmailThread(self.member.user.username,
			"Order payment received",
			"payment of your order#" + str(self.id) + " has been received. Thank You!",
			self.member.user.email	
			).start()

	def delivered(self, admin):
		if self.status < ORDER_STATUS_RECEIVED:
			self.status = ORDER_STATUS_RECEIVED
			self.modified_by = admin
			self.save()

			notify(
		        "Your order#" + str(self.id) + " has been delivered.", 
		        self.member.user.username + "_normal",
		        target_object = self.member.user,
		        url = reverse('orders')
		    )

			EmailThread(self.member.user.username,
			"Order delivered",
			"your order#" + str(self.id) + " has been delivered.",
			self.member.user.email	
			).start()

	def reverse(self, admin):
		if self.status == ORDER_STATUS_WAIT_FOR_PAYMENT:
			if self.order_type == ORDER_TYPE_PURCHASE:
				PurchaseOrder.objects.get(id = self.id).quote(Decimal('0.00'), Decimal('0.00'), Decimal('0.00'), admin)
			else:
				RepairOrder.objects.get(id = self.id).quote(Decimal('0.00'), Decimal('0.00'), Decimal('0.00'), Decimal('0.00'), admin)

		if self.status >= ORDER_STATUS_WAIT_FOR_PAYMENT:
			self.status = self.status - 1
			self.modified_by = admin
			self.save()

			notify(
		        "Your order#" + str(self.id) + "\'s status has been changed.", 
		        self.member.user.username + "_normal",
		        target_object = self.member.user,
		        url = reverse('orders')
		    )

			EmailThread(self.member.user.username,
			"Order status changed",
		    "your order#" + str(self.id) + "\'s status has been changed.",
			self.member.user.email	
			).start()
			

	def cancel_booking(self, who):
		self.measurement_booking = None
		self.status = ORDER_STATUS_BOOK_MEASUREMENT
		self.modified_by = who
		self.locked = False
		self.save()

	def get_id(self):
		return self.id

	def get_price(self):
		if self.order_type == ORDER_TYPE_PURCHASE:
			return PurchaseOrder.objects.get(id = self.id).get_price()
		else:
			return RepairOrder.objects.get(id = self.id).get_price()

	def get_detail(self, for_admin):
		detail = {}
		
		if self.order_type == ORDER_TYPE_PURCHASE:
			detail['extra'] = PurchaseOrder.objects.get(id = self.id).get_detail(for_admin)
		else:
			detail['extra'] = RepairOrder.objects.get(id = self.id).get_detail(for_admin)

		if for_admin:
			detail['extra']['id'] = self.id

		detail['id'] = self.id
		detail['member'] = self.member.get_display_name()
		detail['order_type'] = self.get_order_type_display()
		detail['locked'] = self.locked
		detail['product'] = self.product.name
		detail['boat'] = self.boat.get_detail(for_admin)
		detail['status'] = self.get_status_display()
		if self.measurement_booking != None:
			detail['measurement_booking'] = self.measurement_booking.id
		detail['size'] = Size.objects.get(order = self).get_detail()
		detail['price'] = self.get_price()
		detail['removable_data'] = self.get_removable_data()

		if for_admin:
			detail['is_account_receivable'] = self.is_account_receivable()

		return detail

	def get_detail_for_booking(self):
		if self.order_type == ORDER_TYPE_PURCHASE:
			return PurchaseOrder.objects.get(id = self.id).get_detail_for_booking()
		else:
			return RepairOrder.objects.get(id = self.id).get_detail_for_booking()

	def __str__(self):
		return "#" + str(self.id) + " [" + self.get_order_type_display() + "] " + self.member.get_display_name() + "\'s " + self.product.name

class PurchaseOrder(Order):
	quantity = models.PositiveIntegerField(default=1, validators=[MinValueValidator(1)])
	purchase_order_type = models.IntegerField(choices=PURCHASE_ORDER_TYPE_CHOICES, default=PURCHASE_ORDER_TYPE_GENERAL)

	def quote(self, length, width, height, who):
		#print("purchase order quote " + str(length) + ' ' + str(width) + ' ' + str(height))
		size = Size.objects.get(order = self.order_ptr)
		size.edit(length, width, height, who)

	def get_id(self):
		return self.order_ptr.id

	def get_price(self):
		order = self.order_ptr
		size = Size.objects.get(order = order)

		if self.purchase_order_type == PURCHASE_ORDER_TYPE_GENERAL:
			per_unit_price = GeneralPurchaseOrder.objects.get(id = self.get_id()).get_price()
		else:
			per_unit_price = CarpetPurchaseOrder.objects.get(id = self.get_id()).get_price()

		return per_unit_price * self.quantity

	def get_detail(self, for_admin):
		if self.purchase_order_type == PURCHASE_ORDER_TYPE_GENERAL:
			detail = GeneralPurchaseOrder.objects.get(id = self.get_id()).get_detail(for_admin)
		else:
			detail = CarpetPurchaseOrder.objects.get(id = self.get_id()).get_detail(for_admin)

		detail['quantity'] = self.quantity

		return detail

	def get_detail_for_booking(self):
		if self.purchase_order_type == PURCHASE_ORDER_TYPE_GENERAL:
			return GeneralPurchaseOrder.objects.get(id = self.get_id()).get_detail_for_booking()
		else:
			return CarpetPurchaseOrder.objects.get(id = self.get_id()).get_detail_for_booking()

class GeneralPurchaseOrder(PurchaseOrder):
	color = models.ForeignKey(Color, on_delete=models.PROTECT, default=None)

	@classmethod
	def create(cls, member, product, boat, length, width, height, known_size, quantity, color):
		if known_size == False:
			status = ORDER_STATUS_BOOK_MEASUREMENT
		else:
			status = ORDER_STATUS_WAIT_FOR_PAYMENT

		general_purchase_order = cls(
			member = member,
			order_type = ORDER_TYPE_PURCHASE,
			product = product,
			boat = boat,
			status = status,
			locked = False,
			measurement_booking = None,
			modified_by = member.user,
			purchase_order_type = PURCHASE_ORDER_TYPE_GENERAL,
			quantity = quantity,
			color = color
		)
		general_purchase_order.save()

		order = Order.objects.get(id = general_purchase_order.id)
		Size.create(order, known_size, length, width, height, member)

		MemberLog.create(member, 'Created general purchase order #' + 
			str(general_purchase_order.id) + ' ' + 
			general_purchase_order.color.name + " " + 
			general_purchase_order.product.name + " x" + 
			str(general_purchase_order.quantity) + ' ' + 
			'[' + str(general_purchase_order.size) + ']'
		)

		return general_purchase_order

	def edit(self, length, width, height, modified_by, quantity, color):
		if self.status < ORDER_STATUS_MANUFACTURING:
			if self.locked == False:
				size = Size.objects.get(order = self)
				size.edit(length, width, height, modified_by)

			self.quantity = quantity
			self.color = color
			
			self.save()

			MemberLog.create(self.member, 'Edit general purchase order #' + 
				str(self.id) + ' ' + 
				self.color.name + " " + 
				self.product.name + " x" + 
				str(self.quantity) + ' ' + 
				'[' + str(self.size) + ']'
			)

	def get_id(self):
		return self.purchaseorder_ptr.order_ptr.id

	def get_price(self):
		purchase_order = self.purchaseorder_ptr
		product = purchase_order.order_ptr.product
		size = Size.objects.get(order = purchase_order.order_ptr)
		return product.get_price(size, self.color, None, None, None, None)

	def get_detail(self, for_admin):
		detail = {
			'color': self.color.get_detail()
		}

		return detail

	def get_detail_for_booking(self):
		return "#" + str(self.id) + " [" + self.get_order_type_display() + "] " + self.color.name + " " + self.product.name + " x" + str(self.quantity)

class CarpetPurchaseOrder(PurchaseOrder):
	pattern = models.ForeignKey(CarpetPattern, on_delete=models.PROTECT, default=None)
	waterproof = models.BooleanField(default=False)
	underlay = models.BooleanField(default=False)
	edge = models.BooleanField(default=False)

	@classmethod
	def create(cls, member, product, boat, length, width, height, quantity, pattern, known_size, waterproof, underlay, edge):
		if known_size == False:
			status = ORDER_STATUS_BOOK_MEASUREMENT
		else:
			status = ORDER_STATUS_WAIT_FOR_PAYMENT

		carpet_purchase_order = cls(
			member = member,
			order_type = ORDER_TYPE_PURCHASE,
			product = product,
			boat = boat,
			status = status,
			locked = False,
			measurement_booking = None,
			modified_by = member.user,
			purchase_order_type = PURCHASE_ORDER_TYPE_CARPET,
			pattern = pattern,
			waterproof = waterproof,
			underlay = underlay,
			edge = edge
		)
		carpet_purchase_order.save()
		
		order = Order.objects.get(id = carpet_purchase_order.id)
		Size.create(order, known_size, length, width, height, member)

		MemberLog.create(member, 'Created carpet purchase order #' + 
			str(carpet_purchase_order.id) + ' ' + 
			carpet_purchase_order.pattern.name + " " + 
			carpet_purchase_order.product.name + " x" + 
			str(carpet_purchase_order.quantity) + ' ' + 
			'[' + str(carpet_purchase_order.size) + ']'
		)
		
		return carpet_purchase_order

	def edit(self, length, width, height, modified_by, quantity, pattern, waterproof, underlay, edge):
		if self.status < ORDER_STATUS_MANUFACTURING:
			if self.locked == False:
				size = Size.objects.get(order = self)
				size.edit(length, width, height, modified_by)
			
			self.quantity = quantity
			self.pattern = pattern
			self.waterproof = waterproof
			self.underlay = underlay
			self.edge = edge
			
			self.save()

			MemberLog.create(self.member, 'Edited carpet purchase order #' + 
				str(self.id) + ' ' + 
				self.pattern.name + " " + 
				self.product.name + " x" + 
				str(self.quantity) + ' ' + 
				'[' + str(self.size) + ']'
			)

	def get_id(self):
		return self.purchaseorder_ptr.order_ptr.id

	def get_price(self):
		purchase_order = self.purchaseorder_ptr
		product = purchase_order.order_ptr.product
		size = Size.objects.get(order = purchase_order.order_ptr)
		return product.get_price(size, None, self.pattern, self.waterproof, self.underlay, self.edge)

	def get_detail(self, for_admin):
		detail = {
			'carpet_type': self.pattern.get_pattern_type_display(),
			'pattern': self.pattern.get_detail(),
			'waterproof': self.waterproof,
			'underlay': self.underlay,
			'edge': self.edge
		}

		return detail

	def get_detail_for_booking(self):
		return "#" + str(self.id) + " [" + self.get_order_type_display() + "] " + self.pattern.name + " " + self.product.name + " x" + str(self.quantity)

class RepairOrder(Order):
	image = models.ImageField(null=True, blank=True, upload_to='image/repair/')
	price = models.DecimalField(max_digits=10, decimal_places=2, default=None, null=True, blank=True)

	@classmethod
	def create(cls, member, product, boat, length, width, height, known_size, image):
		if known_size == False:
			status = ORDER_STATUS_BOOK_MEASUREMENT
		else:
			status = ORDER_STATUS_WAIT_FOR_QUOTATION

		repair_order = cls(
			member = member,
			order_type = ORDER_TYPE_REPAIR,
			product = product,
			boat = boat,
			status = status,
			locked = False,
			measurement_booking = None,
			modified_by = member.user,
			image = image
		)
		repair_order.save()
		
		order = Order.objects.get(id = repair_order.id)
		Size.create(order, known_size, length, width, height, member)

		MemberLog.create(member, 'Created repair order #' + 
			str(repair_order.id) + ' ' + 
			repair_order.product.name + ' ' + 
			'[' + str(repair_order.size) + ']'
		)
		
		return repair_order

	def edit(self, length, width, height, modified_by, image, image_changed):
		if self.status < ORDER_STATUS_MANUFACTURING:
			if self.locked == False:
				size = Size.objects.get(order = self)
				size.edit(length, width, height, modified_by)

			if image_changed:
				self.image = image
				
			self.save()

			MemberLog.create(self.member, 'Edited repair order #' + 
				str(self.id) + ' ' + 
				self.product.name + ' ' + 
				'[' + str(self.size) + ']'
			)

	def quote(self, length, width, height, price, who):
		size = Size.objects.select_for_update().get(order = self.order_ptr)
		size.edit(length, width, height, who)
		self.price = price
		self.save()

	def get_id(self):
		return self.order_ptr.id

	def get_price(self):
		return self.price

	def get_detail(self, for_admin):
		detail = {}
		detail['price'] = self.price

		if self.image:
			detail['image'] = static(self.image.url)
		else:
			detail['image'] = ''

		return detail

	def get_detail_for_booking(self):
		return "#" + str(self.id) + " [" + self.get_order_type_display() + "] " + self.product.name

class Size(models.Model):
	order = models.OneToOneField(Order, on_delete=models.CASCADE)
	known_size = models.BooleanField(default=False)
	length = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
	width = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
	height = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
	modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
	modified_at = models.DateTimeField(auto_now=True)

	@classmethod
	def create(cls, order, known_size, length, width, height, member):
		size = cls(
			order = order,
			known_size = known_size, 
			length = length, 
			width = width, 
			height = height,
			modified_by = member.user
		)
		
		size.save()

		return size

	def edit(self, length, width, height, modified_by):
		#print("size edit " + str(length) + ' ' + str(width) + ' ' + str(height))
		self.length = length
		self.width = width
		self.height = height
		self.modified_by = modified_by
		self.save()

	def member_edit(self, length, width, height, modified_by):
		self.known_size = True
		self.edit(length, width, height, modified_by)

	def get_area(self):
		if self.known_size == True:
			return self.length * self.width
		else:
			return None

	def get_perimeter(self):
		if self.known_size == True:
			return (self.length + self.width) * 2
		else:
			return None

	def get_detail(self):
		detail = {
			'known_size': self.known_size,
			'length': self.length,
			'width': self.width,
			'height': self.height
		}

		return detail

	def __str__(self):
		return str(self.length) + " x " + str(self.width) + " x " + str(self.height)

class AccountReceivable(models.Model):
	member = models.ForeignKey(Member, on_delete=models.PROTECT)
	amount = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
	booking_date = models.DateField(default=datetime.date.today)
	booking_timeslot = models.IntegerField(choices=BOOKING_TIMESLOT_CHOICES, default=BOOKING_TIMESLOT_1)
	received = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True)

	@classmethod
	def create(cls, member, amount, booking):
		account_receivable = cls(
			member = member, 
			amount = amount, 
			booking_date = booking.date, 
			booking_timeslot = booking.timeslot, 
			received = False
		)

		account_receivable.save()
		notify(
	        "You need to pay for a HK$300 fee for measurement service on " + account_receivable.booking_date.strftime('%Y-%m-%d') + ' ' + account_receivable.get_booking_timeslot_display() +'.', 
	        account_receivable.member.user.username + "_normal",
	        target_object = account_receivable.member.user,
	        url = reverse('orders')
	    )

		EmailThread(account_receivable.member.user.username,
		"Measurement service payment needed",
		"you need to pay for a HK$300 fee for measurement service on " + account_receivable.booking_date.strftime('%Y-%m-%d') + ' ' + account_receivable.get_booking_timeslot_display() +'.',
		account_receivable.member.user.email	
		).start()

		return account_receivable

	def receive(self):
		self.received = True
		self.save()

	def __str__(self):
		return self.member.get_display_name() + "\'s $" + str(self.amount)# + " on " + str(utc2local(self.created_at).strftime("%Y-%m-%d %H:%M"))

class MemberLog(models.Model):
	member = models.ForeignKey(Member, on_delete=models.PROTECT)
	description = models.CharField(max_length=300)
	created_at = models.DateTimeField(auto_now_add=True)

	@classmethod
	def create(cls, member, description):
		log = cls(
			member = member,
			description = description
		)

		log.save()

		return log

	def __str__(self):
		return "[" + utc2local(self.created_at).strftime('%Y-%m-%d %H:%M') + "] " + self.member.get_display_name() + ": " + self.description

class ContactMessage(models.Model):
	name = models.CharField(max_length=100)
	email = models.CharField(max_length=100)
	content = models.CharField(max_length=10240)
	read = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True)

	@classmethod
	def create(cls, name, email, content):
		contact_message = cls(
			name = name,
			email = email,
			content = content,
			read = False
		)

		contact_message.save()

		notify(
	        'You have a new message from ' + str(name),  
	        "admin",
	        url = reverse('contactmessage')
	    )

		return contact_message

	def get_detail(self):
		detail = {
			'id': self.id,
			'name': self.name,
			'email': self.email,
			'content': self.content,
			'created_at': utc2local(self.created_at).strftime('%Y-%m-%d %H:%M')
		}
		return detail

	def process_read(self):
		self.read = True
		self.save()

	def process_unread(self):
		self.read = False
		self.save()

	def __str__(self):
		return "[" + utc2local(self.created_at).strftime('%Y-%m-%d %H:%M') + "] " + self.name + ' (' + self.email + ')'

class EmailThread(threading.Thread):

	@classmethod
	def __init__(self, name, title, content, recipient_list):
		self.recipient_list = recipient_list
		self.content = content
		self.title = title
		self.name = name
		threading.Thread.__init__(self)

	def run (self):
		with open('wingwoo/static/images/wwlogo2.png', 'rb') as logo:
			image = logo.read()
		inline_image = InlineImage(filename='wwlogo2.png', content=image)
		send_templated_mail(
			template_name='notifyemail',
			from_email='wingwoosailmaker@gmail.com',
			recipient_list=[self.recipient_list],
			context= {
				'title':self.title,
				'name':self.name,
				'content':self.content,
				'logo':inline_image
			},
		)
