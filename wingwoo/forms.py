from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from django.contrib.auth.models import User
from .models import *

# class UserForm(forms.ModelForm):
#     password = forms.CharField(widget=forms.PasswordInput)
#     class Meta:
#         model = User

class RegisterForm(UserCreationForm):
    name = forms.CharField(max_length=60, required=False, widget=forms.HiddenInput())
    first_name = forms.CharField(max_length=30, required=True)
    last_name = forms.CharField(max_length=30, required=True)
    email = forms.EmailField(max_length=254, required=True)
    phone = forms.CharField(max_length=15, required=True)


    class Meta:
        model = User
        fields = ('username', 'name', 'first_name','last_name','email','phone','password1', 'password2',)

class EditUserForm(UserChangeForm):
    first_name = forms.CharField(max_length=30, required=True)
    last_name = forms.CharField(max_length=30, required=True)
    email = forms.EmailField(max_length=254, required=True)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email','password')
    def __init__(self, *args, **kargs):
        super(EditUserForm, self).__init__(*args, **kargs)
        del self.fields['password']

class EditMemberForm(UserChangeForm):
    name = forms.CharField(max_length=60, required=False, widget=forms.HiddenInput())
    phone = forms.CharField(max_length=15, required=True)

    class Meta:
        model = User
        fields = ('name', 'phone', 'password')
    def __init__(self, *args, **kargs):
        super(EditMemberForm, self).__init__(*args, **kargs)
        del self.fields['password']

class MemberBoatForm(forms.Form):
    boat = forms.ModelChoiceField(queryset=Boat.objects.all(), required=True)
    def __init__(self, member_id, *args, **kwargs):
        super(MemberBoatForm, self).__init__(*args, **kwargs)
        self.fields['boat'].queryset = Boat.objects.filter(member_id = member_id)

class ContactForm(forms.Form):
    name = forms.CharField(max_length=60, required=True)
    email = forms.CharField( required=True)
    content = forms.CharField(required=True, widget=forms.Textarea)

class BoatForm(forms.Form):
    address = forms.ModelChoiceField(queryset=Address.objects.all())
    model = forms.CharField(max_length=80)

class EditBoatForm(forms.Form):
    boatid = forms.IntegerField(required = True)
    address = forms.ModelChoiceField(queryset=Address.objects.all())
    model = forms.CharField(max_length=80)

    def __init__(self, boatid, *args, **kwargs):
        super(EditBoatForm, self).__init__(*args, **kwargs)
        self.fields['boatid'].initial = boatid
        
class GeneralPurchaseOrderForm(forms.Form):
    member = forms.IntegerField(widget = forms.HiddenInput(), required=True)
    boat = forms.ModelChoiceField(queryset=Boat.objects.all(), required=True)
    quantity = forms.IntegerField(min_value=1, required=True)
    length = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    width = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    height = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    known_size = forms.BooleanField(required=False, label='I don\'t know the size', widget=forms.CheckboxInput(attrs={'id': 'id_known_size_purchase'}))
    color = forms.ModelChoiceField(queryset=Color.objects.all(), required=True)

    def __init__(self, member_id, *args, **kwargs):
        super(GeneralPurchaseOrderForm, self).__init__(*args, **kwargs)
        self.fields['member'].initial = member_id
        self.fields['boat'].queryset = Boat.objects.filter(member_id = member_id)

class CarpetPurchaseOrderForm(forms.Form):
    member = forms.IntegerField(widget = forms.HiddenInput())
    boat = forms.ModelChoiceField(queryset=Boat.objects.all())
    length = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    width = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    height = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    known_size = forms.BooleanField(required=False, label='I don\'t know the size', widget=forms.CheckboxInput(attrs={'id': 'id_known_size_purchase'}))
    quantity = forms.IntegerField(min_value=1)
    pattern = forms.ModelChoiceField(queryset=CarpetPattern.objects.all())
    waterproof = forms.BooleanField(required=False)
    underlay = forms.BooleanField(required=False)
    edge = forms.BooleanField(required=False)

    def __init__(self, member_id, *args, **kwargs):
      super(CarpetPurchaseOrderForm, self).__init__(*args, **kwargs)
      self.fields['member'].initial = member_id
      self.fields['boat'].queryset = Boat.objects.filter(member_id = member_id)

class RepairOrderForm(forms.Form):
    member = forms.IntegerField(widget = forms.HiddenInput())
    boat = forms.ModelChoiceField(queryset=Boat.objects.all())
    length = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    width = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    height = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    known_size = forms.BooleanField(required=False, label='I don\'t know the size', widget=forms.CheckboxInput(attrs={'id': 'id_known_size_repair'}))
    image = forms.ImageField(required=False)

    def __init__(self, member_id, *args, **kwargs):
       super(RepairOrderForm, self).__init__(*args, **kwargs)
       self.fields['member'].initial = member_id
       self.fields['boat'].queryset = Boat.objects.filter(member_id = member_id)

class GeneralPurchaseOrderEditForm(forms.Form):
    quantity = forms.IntegerField(min_value=1)
    length = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    width = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    height = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    color = forms.ModelChoiceField(queryset=Color.objects.all())

class CarpetPurchaseOrderEditForm(forms.Form):
    quantity = forms.IntegerField(min_value=1)
    length = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    width = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    height = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    pattern = forms.ModelChoiceField(queryset=CarpetPattern.objects.all())
    waterproof = forms.BooleanField(required=False)
    underlay = forms.BooleanField(required=False)
    edge = forms.BooleanField(required=False)

class RepairOrderEditForm(forms.Form):
    length = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    width = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    height = forms.DecimalField(min_value=Decimal('0.01'), decimal_places=2, required=False)
    image = forms.ImageField(required=False)