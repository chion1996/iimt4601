import datetime

from django.contrib import admin
from django import forms
from django.db import models

# Register your models here.
from .models import *
from .choices import *

class ModifiedByForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ModifiedByForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['modified_by'].disabled = True

    def save(self, *args, **kwargs):
        instance = super(ModifiedByForm, self).save(commit=False)
        instance.modified_by = self.current_user
        return instance

class ModifiedByAdmin(admin.ModelAdmin):
    readonly_fields = ('modified_at', )

    def get_form(self, request, *args, **kwargs):
        form = super(ModifiedByAdmin, self).get_form(request, *args, **kwargs)
        form.current_user = request.user
        return form

class ProductImageInline(admin.TabularInline):
    model = ProductImage
    extra = 1

class ProductFeatureInline(admin.TabularInline):
    model = ProductFeatureLine
    extra = 1

class ProductForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['product_type'].disabled = True

class GeneralProductForm(ProductForm):
    def __init__(self, *args, **kwargs):
        super(GeneralProductForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['product_type'].initial = PRODUCT_TYPE_GENERAL

    def save(self, *args, **kwargs):
        instance = super(GeneralProductForm, self).save(commit=False)
        instance.product_type = PRODUCT_TYPE_GENERAL
        instance.save()
        return instance

class CarpetProductForm(ProductForm):
    def __init__(self, *args, **kwargs):    
        super(CarpetProductForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['product_type'].initial = PRODUCT_TYPE_CARPET

    def save(self, *args, **kwargs):
        instance = super(CarpetProductForm, self).save(commit=False)
        instance.product_type = PRODUCT_TYPE_CARPET
        instance.save()
        return instance

class ProductAdmin(admin.ModelAdmin):
    form = ProductForm
    inlines = (ProductImageInline, ProductFeatureInline, )

class GeneralProductAdmin(ProductAdmin):
    form = GeneralProductForm

class CarpetProductAdmin(ProductAdmin):
    form = CarpetProductForm

class OrderSizeInline(admin.StackedInline):
	model = Size
	extra = 1
	max_num = 1
	min_num = 1
	verbose_name = 'Size'
	verbose_name_plural = 'Size'

class OrderForm(ModifiedByForm):
    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['order_type'].disabled = True

class PurchaseOrderForm(OrderForm):
    def __init__(self, *args, **kwargs):
        super(PurchaseOrderForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['purchase_order_type'].disabled = True
            self.fields['order_type'].initial = ORDER_TYPE_PURCHASE

    def save(self, *args, **kwargs):
        instance = super(PurchaseOrderForm, self).save(commit=False)
        instance.order_type = ORDER_TYPE_PURCHASE
        instance.save()
        return instance

class GeneralPurchaseOrderForm(PurchaseOrderForm):
    def __init__(self, *args, **kwargs):
        super(GeneralPurchaseOrderForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['purchase_order_type'].initial = PURCHASE_ORDER_TYPE_GENERAL

    def save(self, *args, **kwargs):
        instance = super(GeneralPurchaseOrderForm, self).save(commit=False)
        instance.purchase_order_type = PURCHASE_ORDER_TYPE_GENERAL
        instance.save()
        return instance

class CarpetPurchaseOrderForm(PurchaseOrderForm):
    def __init__(self, *args, **kwargs):
        super(CarpetPurchaseOrderForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['product'].disabled = True
            self.fields['product'].initial = Product.objects.get(name = 'Carpet').id
            self.fields['purchase_order_type'].initial = PURCHASE_ORDER_TYPE_CARPET

    def save(self, *args, **kwargs):
        instance = super(CarpetPurchaseOrderForm, self).save(commit=False)
        instance.purchase_order_type = PURCHASE_ORDER_TYPE_CARPET
        instance.save()
        return instance

class RepairOrderForm(OrderForm):
    def __init__(self, *args, **kwargs):
        super(RepairOrderForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['order_type'].initial = ORDER_TYPE_REPAIR

    def save(self, *args, **kwargs):
        instance = super(RepairOrderForm, self).save(commit=False)
        instance.order_type = ORDER_TYPE_REPAIR
        instance.save()
        return instance

class OrderAdmin(ModifiedByAdmin):
    form = OrderForm
    inlines = (OrderSizeInline, )
    list_filter = ('id', 'member', 'product', 'status', 'measurement_booking', )

class PurchaseOrderAdmin(OrderAdmin):
    form = PurchaseOrderForm

class GeneralPurchaseOrderAdmin(PurchaseOrderAdmin):
    form = GeneralPurchaseOrderForm

class CarpetPurchaseOrderAdmin(PurchaseOrderAdmin):
    form = CarpetPurchaseOrderForm

class RepairOrderAdmin(OrderAdmin):
    form = RepairOrderForm

class OccupiedTimeslotForm(ModifiedByForm):
    def __init__(self, *args, **kwargs):
        super(OccupiedTimeslotForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['timeslot_type'].disabled = True

    def clean(self):
        # Check whether the timeslot is available
        date = self.cleaned_data.get('date')
        timeslot = self.cleaned_data.get('timeslot')

        if OccupiedTimeslot.is_datetime_available_edit(self.instance.id, date, timeslot)['result'] == False:
            raise forms.ValidationError("The timeslot is occupied")

        return self.cleaned_data

class EventForm(OccupiedTimeslotForm):
    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['timeslot_type'].initial = OCCUPIED_TIMESLOT_TYPE_EVENT

    def save(self, *args, **kwargs):
        instance = super(EventForm, self).save(commit=False)
        instance.timeslot_type = OCCUPIED_TIMESLOT_TYPE_EVENT
        instance.save()
        return instance

class MeasurementBookingForm(OccupiedTimeslotForm):
    class Meta:
        model = MeasurementBooking
        fields = ['date', 'timeslot', 'timeslot_type', 'member', 'orders', 'status', 'modified_by']

    orders = forms.ModelMultipleChoiceField(queryset=Order.objects.all())

    def __init__(self, *args, **kwargs):
        super(MeasurementBookingForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['timeslot_type'].initial = OCCUPIED_TIMESLOT_TYPE_BOOKING
            if self.instance.in_booking:
                self.fields['orders'].initial = self.instance.in_booking.all()
            else:
                self.fields['orders'].initial = []
    
    def save(self, *args, **kwargs):
        instance = super(MeasurementBookingForm, self).save(commit=False)
        instance.timeslot_type = OCCUPIED_TIMESLOT_TYPE_BOOKING
        instance.save()

        removed_orders = list(self.fields['orders'].initial.difference(self.cleaned_data['orders']))
        added_orders = list(self.cleaned_data['orders'].difference(self.fields['orders'].initial))

        if len(removed_orders) > 0:
            for removed_order in removed_orders:
                removed_order.status = ORDER_STATUS_BOOK_MEASUREMENT
                removed_order.measurement_booking = None
                removed_order.save()

        if len(added_orders) > 0:
            for added_order in added_orders:
                added_order.status = ORDER_STATUS_WAIT_FOR_QUOTATION
                added_order.measurement_booking = instance
                added_order.save()

        return instance

class OccupiedTimeslotAdmin(ModifiedByAdmin):
    form = OccupiedTimeslotForm

    def get_changeform_initial_data(self, request):
        timeslot_string = request.GET.get('timeslot', '12:00:00')
        date = request.GET.get('date', datetime.date.today)

        timeslot_choice = OccupiedTimeslot.get_timeslot_choice(timeslot_string)

        return {
            'date': date,
            'timeslot': timeslot_choice
        }

class EventAdmin(OccupiedTimeslotAdmin):
    form = EventForm

class MeasurementBookingAdmin(OccupiedTimeslotAdmin):
    form = MeasurementBookingForm

class CreatedByAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at', )

admin.site.register(Member)
admin.site.register(Address)
admin.site.register(Boat)
admin.site.register(Product, ProductAdmin)
admin.site.register(GeneralProduct, GeneralProductAdmin)
admin.site.register(CarpetProduct, CarpetProductAdmin)
admin.site.register(ProductFeature)
admin.site.register(Color)
admin.site.register(Size, ModifiedByAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(PurchaseOrder, PurchaseOrderAdmin)
admin.site.register(GeneralPurchaseOrder, GeneralPurchaseOrderAdmin)
admin.site.register(CarpetPattern)
admin.site.register(CarpetPurchaseOrder, CarpetPurchaseOrderAdmin)
admin.site.register(RepairOrder, RepairOrderAdmin)
admin.site.register(OccupiedTimeslot, OccupiedTimeslotAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(MeasurementBooking, MeasurementBookingAdmin)
admin.site.register(AccountReceivable, CreatedByAdmin)
admin.site.register(MemberLog)
admin.site.register(ContactMessage)