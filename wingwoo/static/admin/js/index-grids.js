var timeslotChoices = ["12:00 - 14:00", "14:00 - 16:00", "16:00 - 18:00"];
var districtChoices = ["Sai Kung", "Hong Kong Island", "Tuen Mun", "Others"];
var orderTypes = ["", "Purchase", "Repair"];
var orderTypeValues = ["", 0, 1];
var products = ["", "Carpet", "Cover", "Cushion", "Top"];

function formatNumberForDisplay(numberToFormat) {
	var formatter = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'USD',
		digits: 2,
	});
	
	return formatter.format(numberToFormat);
}

$(document).ready(function() {
	var loadOrders = function(filter, status) {
		var d;

		$.ajax({
			url: '/adminorders/' + status + '/',
			type: 'POST',
			data: filter,
			headers: {"X-CSRFToken": getCookie("csrftoken")},
			async: false
		}).done(function(response) {
			d = response;
		});

		return {
			data: d.data,
			itemsCount: d.total_count
		};
	}

	var getFieldsArray = function(status, grid) {
		fields = [
			{ name: "id", title: "ID", type: "number", width: 70, editing: false, filtering: true, sorting: true, align: 'center', 
				itemTemplate: function(value, item) {
					var a = $('<a>').text(value);

					var url;
					if (item.order_type == 'Purchase') {
						if (item.extra.carpet_type) {
							url = '/admin/wingwoo/carpetpurchaseorder/';
						} else {
							url = '/admin/wingwoo/generalpurchaseorder/';
						}
					} else {
						url = '/admin/wingwoo/repairorder/';
					}
					url +=  (value + '/change/');
					a.attr('href', url);
					return a;
			    },
				filterTemplate: function() {
					var input = $('<input>').addClass('form-control').attr({'type': 'number', 'min': '0', 'step': '1'});
					
					return this._filterPicker = input;
				},
				filterValue: function() {
					return this._filterPicker.val();
				}
			},
			{ name: "order_type", title: "Type", type: "text", width: 75, editing: false, filtering: true, sorting: true, visible: true, 
				filterTemplate: function() {
					var select = $('<select>').addClass('form-control');
					for (var i = 0; i < orderTypes.length; i++) {
						select.append($('<option>').text(orderTypes[i]).attr({'value': orderTypeValues[i]}));
					}
					return this._filterPicker = select;
				},
				filterValue: function() {
					return this._filterPicker.val();
				}
			},
			{ name: "member", title: "Member", type: "text", width: 120, editing: false, filtering: true, sorting: true, align: 'center', 
				filterTemplate: function() {
					return this._filterPicker = $('<input>').addClass('form-control').attr({'type': 'text'});
				},
				filterValue: function() {
					return this._filterPicker.val();
				}
			},
			{ name: "product", title: "Product", type: "text", width: 75, editing: false, filtering: true, sorting: true, 
				filterTemplate: function() {
					var select = $('<select>').addClass('form-control');
					for (var i = 0; i < products.length; i++) {
						select.append($('<option>').text(products[i]).attr({'value': products[i]}));
					}
					return this._filterPicker = select;
				},
				filterValue: function() {
					return this._filterPicker.val();
				}
			},
			{ name: "boat", title: "Boat", type: "text", width: 150, editing: false, filtering: true, sorting: false,
				itemTemplate: function(value) {
					return value.model + ' @</br>[' + districtChoices[parseInt(value.address.district)] + ']</br>' + value.address.pier;
				}, 
				filterTemplate: function() {
					return this._filterPicker = $('<input>').addClass('form-control').attr({'type': 'text'});
				},
				filterValue: function() {
					return this._filterPicker.val();
				}
			},
			{ name: "size", title: "Size", type: "number", width: 80, editing: false, filtering: false, sorting: false, visible: true, align: 'center',
				itemTemplate: function(value) {
					if (value.length == '0.00' && value.width == '0.00' && value.height == '0.00') {
						return '-';
					} else {
						return value.length + ' x </br>' + value.width + ' x </br>' + value.height + '&emsp;';
					}
				}
			}
		];

		fields.push(
			{name: "extra", title: "Details", type: "text", width: 120, sorting: false, filtering: false, editing: false,
				itemTemplate: function(value) {
					var itemDiv = $('<div>').addClass('grid-detail-item-div');
					var label = $('<label>').addClass('grid-detail-label');

					var createImageA = function(image, description) {
						var div = $('<div>').addClass('thumbnail');

						var imageElem = $('<img>')
							.addClass('grid-detail-image')
							.attr('src', image);

						var imageA = $('<a>')
							.addClass('grid-detail-image-a')
							.attr({'href': image, 'data-max-width': '600'})
							.append(imageElem)
							.on('click', function (e) {
						        e.preventDefault();
						        $(this).ekkoLightbox();
						    });

						if (description != '')
							var p = $('<p>').text(description)

						return div.append(imageA).append(p);
					};

					if (value.quantity != null && value.quantity != '') {
						var quantitySpan = $('<span>').text(value.quantity);
						var quantityDiv = itemDiv.clone().append(label.clone().text('Quantity: ').append(quantitySpan));
					}

					if (value.waterproof != null) {
						var result = value.waterproof ? $('<i>').addClass('fa fa-check') : $('<i>').addClass('fa fa-times');
						var waterproofDiv = itemDiv.clone().append(label.clone().text('Waterproof: ').append(result));
					}

					if (value.underlay != null) {
						var result = value.underlay ? $('<i>').addClass('fa fa-check') : $('<i>').addClass('fa fa-times');
						var underlayDiv = itemDiv.clone().append(label.clone().text('Underlay: ').append(result));
					}

					if (value.edge != null) {
						var result = value.edge ? $('<i>').addClass('fa fa-check') : $('<i>').addClass('fa fa-times');
						var edgeDiv = itemDiv.clone().append(label.clone().text('Edge: ').append(result));
					}

					if (value.color != null) {
						var imageA = createImageA(value.color.image, value.color.code + ' ' + value.color.name);
						var imageDiv = itemDiv.clone().append(label.clone().text('Color: ')).append(imageA);
					}

					if (value.pattern != null) {
						var imageA = createImageA(value.pattern.image, value.pattern.code + ' ' + value.pattern.name);
						var imageDiv = itemDiv.clone().append(label.clone().text('Patttern: ')).append(imageA);
					}
					
					if (value.image != null && value.image != '') {
						var imageA = createImageA(value.image, '');
						var imageDiv = itemDiv.clone().append(label.clone().text('Image:')).append(imageA);
					}

					if (quantityDiv != null || waterproofDiv != null || underlayDiv != null || edgeDiv != null || imageDiv != null) {
						var collapseA = $('<a>')
						.attr({
							'data-toggle': 'collapse', 
							'class': 'collapsed',
							'href': '#grid-detail-collapse-div-' + value.id
						})
						.collapse();

						var extra = $('<div>')
							.addClass('collapse')
							.attr({
								'id': 'grid-detail-collapse-div-' + value.id
							});
						
						return $('<div>').addClass('grid-detail-div')
							.append(collapseA)
							.append(extra
								.append(quantityDiv)
								.append(waterproofDiv)
								.append(underlayDiv)
								.append(edgeDiv)
								.append(imageDiv)
							);
					} else {
						return null;
					}
				}
			}
		);

		if (parseInt(status) > 1) {
			fields.push({
				name: "price", title: "Price", type: "number", width: 100, editing:false, filtering: false, sorting: true, css: "grid_order_price", 
					/*filterTemplate: function() {
						this._fromPicker = $('<input>').addClass('form-control').attr({'type': 'number', 'step': '0.01'});
						this._toPicker = $('<input>').addClass('form-control').attr({'type': 'number', 'step': '0.01'});
						return $("<div>")
							.append($('<label>').text('From: ').css('float', 'left'))
							.append(this._fromPicker)
							.append($('<label>').text('To: ').css('float', 'left'))
							.append(this._toPicker);
					},
					filterValue: function() {
						return {
				            from: this._fromPicker.val(),
				            to: this._toPicker.val()
				        };
					},*/
					itemTemplate: function(value) {
						return formatNumberForDisplay(value);
					}
				}
			);
		}
		

		fields.push({
			type: "control", width: 120, editButton:true, deleteButton:true,
				_createFilterSwitchButton: function() {
			   		return this._createOnOffSwitchButton("filtering", this.searchModeButtonClass, false);
				},
            	itemTemplate: function(value, item) {
                    var result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);

                    var mainDiv = $('<div>');

                    // Edit redirect button
                    var editIcon = $("<i>").attr({class: "fa fa-lg fa-pencil-square-o jsgrid-button-icon jsgrid-edit-icon"});
                    var editButton = $("<a>")
                    .attr({"class": "jsgrid-custom-button jsgrid-custom-edit-button", "data-toggle": "tooltip", "data-placement": "top", "title":"Edit"})
                    .on('click', function(e) {
                    	var type;
                    	if (item.order_type == 'Repair') {
                    		type = 'repairorder';
                    	} else {
                    		if (item.product == 'Carpet') {
                    			type = 'carpetpurchaseorder';
                    		} else {
                    			type = 'generalpurchaseorder';
                    		}
                    	}
                		location.href = 'wingwoo/' + type + '/' + item.id + '/change/';
                    })
                	editButton.append(editIcon);

                    var editDiv = $('<span>').addClass('jsgrid-button-wrapper').append(editButton);

                    editDiv.tooltip({title: "Edit", container: 'body'});
                    // Edit redirect button

                    if (status != '5') {
	                    var doneIcon = $("<i>").attr({class: "fa fa-lg fa-check jsgrid-button-icon jsgrid-done-icon"});
	                    var doneButton = $("<a>").attr({"class": "jsgrid-custom-button", "data-toggle": "tooltip", "data-placement": "top"}).append(doneIcon);
	                    var doneDiv = $('<span>').addClass('jsgrid-button-wrapper').append(doneButton);

	                    mainDiv.append(doneDiv);
	                }

                    if (item.is_account_receivable) {
                    	if (status == '2') {
	                    	var receivableIcon = $("<i>").attr({class: "fa fa-lg fa-user-times jsgrid-button-icon jsgrid-receivable-icon"});
	                    	var receivableButton = $("<a>").attr({"class": "jsgrid-custom-button", "data-toggle": "tooltip", "data-placement": "top"}).append(receivableIcon);
	                    	var receivableDiv = $('<span>').addClass('jsgrid-button-wrapper').append(receivableButton);

	                    	receivableButton.confirmation({
								title: 'Confirm?',
								placement: 'left',
								singleton: true,
								popout: true,
								rootSelector: receivableButton,
								container: $('body'),
								onConfirm: function() {
									$.ajax({
							            url: '/createreceivable/',
							            type: 'POST',
							            data: 'order_id=' + item.id,
							            headers: {"X-CSRFToken": getCookie("csrftoken")},
							            success: function(data) {
							                if (data.result == true) {
							                	$("#waiting-orders-grid").jsGrid("loadData");
							                	$("#unfulfilled-orders-grid").jsGrid("loadData");
							                	generalNotify('success', 'Updated Successfully', null);
							                } else {
							                    generalNotify('danger', 'Operation Failed!', null);
							                }
							            }
							        });
								}
							});

	                    	receivableDiv.tooltip({title: "Change to Unfulfilled & Create $300 Account Receivable", container: 'body'});
	                    	mainDiv.append(receivableDiv);
	                    }
                    }
                    

                    var backIcon = $("<i>").attr({class: "fa fa-lg fa-repeat jsgrid-button-icon jsgrid-back-icon"});
                    var backButton = $("<a>").attr({"class": "jsgrid-custom-button", "data-toggle": "tooltip", "data-placement": "top"}).append(backIcon);
                    var backDiv = $('<span>').addClass('jsgrid-button-wrapper').append(backButton);
                    
                    if (status == '1') {
                    	doneDiv.tooltip({"title": "Quote"});

	                    doneButton
	                    .on('click', function(e) {
	                    	console.log('quote');
	                    	$('#quoteModal').modal('show');
	                    	$('#quoteModal').find('.modal-title').text('Quote Order#' + item.id);
	                    	$('#quoteModal').find('.order-id').val(item.id);

	                    	if (item.order_type == 'Repair') {
	                    		$('#quoteModal').find('.price-input').show();
	                    	} else {
	                    		$('#quoteModal').find('.price-input').hide();
	                    	}
	                    });
                    } else if (status == '2') {
                    	doneDiv.tooltip({"title": "Received Payment"});
	                    doneButton.confirmation({
							title: 'Confirm?',
							placement: 'left',
							singleton: true,
							popout: true,
							rootSelector: doneButton,
							container: $('body'),
							onConfirm: function() {
								$.ajax({
						            url: '/receivedpayment/',
						            type: 'POST',
						            data: 'order_id=' + item.id,
						            headers: {"X-CSRFToken": getCookie("csrftoken")},
						            success: function(data) {
						                if (data.result == true) {
						                	$("#waiting-orders-grid").jsGrid("loadData");
						                	$("#to-make-orders-grid").jsGrid("loadData");
						                	generalNotify('success', 'Updated Successfully', null);
						                } else {
						                    generalNotify('danger', 'Operation Failed!', null);
						                }
						            }
						        });
							}
						});
	                   

	                    backDiv.tooltip({"title": "Reverse to To-Quote Orders"});

	                    backButton.confirmation({
							title: 'Quoted size and price will be reset and it will be moved back to To-Quote Orders, confirm?',
							placement: 'left',
							singleton: true,
							popout: true,
							rootSelector: backButton,
							container: $('body'),
							onConfirm: function() {
								$.ajax({
						            url: '/reverseorderstatus/',
						            type: 'POST',
						            data: 'order_id=' + item.id,
						            headers: {"X-CSRFToken": getCookie("csrftoken")},
						            success: function(data) {
						                if (data.result == true) {
						                	$("#to-quote-orders-grid").jsGrid("loadData");
						                	$("#waiting-orders-grid").jsGrid("loadData");
						                	generalNotify('success', 'Updated Successfully', null);
						                } else {
						                    generalNotify('danger', 'Operation Failed!', null);
						                }
						            }
						        });
							}
						});

	                    mainDiv.append(backDiv);
                    } else if (status == '3') {
                    	doneDiv.tooltip({"title": "Made"});

	                    doneButton.confirmation({
							title: 'Confirm?',
							placement: 'left',
							singleton: true,
							popout: true,
							rootSelector: doneButton,
							container: $('body'),
							onConfirm: function() {
								$.ajax({
						            url: '/madeorder/',
						            type: 'POST',
						            data: 'order_id=' + item.id,
						            headers: {"X-CSRFToken": getCookie("csrftoken")},
						            success: function(data) {
						                if (data.result == true) {
						                	$("#to-make-orders-grid").jsGrid("loadData");
						                	$("#to-deliver-orders-grid").jsGrid("loadData");
						                	generalNotify('success', 'Updated Successfully', null);
						                } else {
						                    generalNotify('danger', 'Operation Failed!', null);
						                }
						            }
						        });
							}
						});

	                    backDiv.tooltip({"title": "Reverse to Waiting Orders"});

	                    backButton.confirmation({
							title: 'It will be moved back to Waiting Orders, confirm?',
							placement: 'left',
							singleton: true,
							popout: true,
							rootSelector: backButton,
							container: $('body'),
							onConfirm: function() {
								$.ajax({
						            url: '/reverseorderstatus/',
						            type: 'POST',
						            data: 'order_id=' + item.id,
						            headers: {"X-CSRFToken": getCookie("csrftoken")},
						            success: function(data) {
						                if (data.result == true) {
						                	$("#waiting-orders-grid").jsGrid("loadData");
						                	$("#to-make-orders-grid").jsGrid("loadData");
						                	generalNotify('success', 'Updated Successfully', null);
						                } else {
						                    generalNotify('danger', 'Operation Failed!', null);
						                }
						            }
						        });
							}
						});

	                    mainDiv.append(backDiv);
                    } else if (status == '4') {
                    	doneDiv.tooltip({"title": "Delivered"});

	                    doneButton.confirmation({
							title: 'Confirm?',
							placement: 'left',
							singleton: true,
							popout: true,
							rootSelector: doneButton,
							container: $('body'),
							onConfirm: function() {
								$.ajax({
						            url: '/deliveredorder/',
						            type: 'POST',
						            data: 'order_id=' + item.id,
						            headers: {"X-CSRFToken": getCookie("csrftoken")},
						            success: function(data) {
						                if (data.result == true) {
						                	$("#to-deliver-orders-grid").jsGrid("loadData");
						                	generalNotify('success', 'Updated Successfully', null);
						                } else {
						                    generalNotify('danger', 'Operation Failed!', null);
						                }
						            }
						        });
							}
						});

	                    backDiv.tooltip({"title": "Reverse to To-Make Orders"});

	                    backButton.confirmation({
							title: 'It will be moved back to To-Make Orders, confirm?',
							placement: 'left',
							singleton: true,
							popout: true,
							rootSelector: backButton,
							container: $('body'),
							onConfirm: function() {
								$.ajax({
						            url: '/reverseorderstatus/',
						            type: 'POST',
						            data: 'order_id=' + item.id,
						            headers: {"X-CSRFToken": getCookie("csrftoken")},
						            success: function(data) {
						                if (data.result == true) {
						                	$("#to-make-orders-grid").jsGrid("loadData");
						                	$("#to-deliver-orders-grid").jsGrid("loadData");
						                	generalNotify('success', 'Updated Successfully', null);
						                } else {
						                    generalNotify('danger', 'Operation Failed!', null);
						                }
						            }
						        });
							}
						});

	                    mainDiv.append(backDiv);
                    }

					return mainDiv.append(editDiv);;
                }
		});
		
		return fields;
	};

	var generalConfig = {
		width: "100%",
		height: "auto",
		heading: true,
		inserting: false,
		filtering: true,
		editing: false,
		sorting: true,
		paging: true,
		autoload: true,
		pageLoading: true,
		confirmDeleting: false,

		pagerContainer: null,
	    pageIndex: 1,
	    pageSize: 4,

	    pageButtonCount: 10,
		pagerFormat: "{first} {prev} {pages} {next} {last}",
		pagePrevText: "Prev",
		pageNextText: "Next",
		pageFirstText: "<<",
		pageLastText: ">>",
		pageNavigatorNextText: "...",
		pageNavigatorPrevText: "...",
	    pagerContainerClass: "jsgrid-pager-container",
        pagerClass: "jsgrid-pager",
        pagerNavButtonClass: "jsgrid-pager-nav-button",
        pagerNavButtonInactiveClass: "jsgrid-pager-nav-inactive-button",
        pageClass: "jsgrid-pager-page",
        currentPageClass: "jsgrid-pager-current-page"
	}

	var getGridConfig = function(status, grid) {
		var config = {
			controller: {
				loadData: function(filter) {
					return loadOrders(filter, status);
				}
			},
			fields: getFieldsArray(status, grid)
		};

		return $.extend(generalConfig, config);
	};

	$("#to-quote-orders-grid").jsGrid(getGridConfig(1, $("#to-quote-orders-grid")));
	$("#waiting-orders-grid").jsGrid(getGridConfig(2, $("#waiting-orders-grid")));
	$("#to-make-orders-grid").jsGrid(getGridConfig(3, $("#to-make-orders-grid")));
	$("#to-deliver-orders-grid").jsGrid(getGridConfig(4, $("#to-deliver-orders-grid")));
	$("#unfulfilled-orders-grid").jsGrid(getGridConfig(6, $("#unfulfilled-orders-grid")));

	$("#to-quote-orders-grid").jsGrid("option", "filtering", false);
	$("#waiting-orders-grid").jsGrid("option", "filtering", false);
	$("#to-make-orders-grid").jsGrid("option", "filtering", false);
	$("#to-deliver-orders-grid").jsGrid("option", "filtering", false);
	$("#unfulfilled-orders-grid").jsGrid("option", "filtering", false);

	$('#btn-orders-quote').confirmation({
		title: 'Confirm?',
		placement: 'left',
		singleton: true,
		popout: true,
		rootSelector: $('#btn-orders-quote'),
		container: $('#quoteModal'),
		onConfirm: function() {
			var data = $('#quoteModal').find('form').eq(0).serialize();

        	$.ajax({
	            url: '/quoteorder/',
	            type: 'POST',
	            data: data,
	            headers: {"X-CSRFToken": getCookie("csrftoken")},
	            success: function(data) {
	                if (data.result == true) {
	                	$("#to-quote-orders-grid").jsGrid("loadData");
	                	$("#waiting-orders-grid").jsGrid("loadData");
	                	$('#quoteModal').modal('hide');
	                	generalNotify('success', 'Quoted Successfully', null);
	                } else {
	                	generalNotify('warning', data.message, $('#quoteModal'));
	                }
	            }
	        });
		}
	})
});