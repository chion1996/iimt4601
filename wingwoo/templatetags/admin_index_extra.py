from pprint import pprint
from django import template
from django.urls import reverse
from wingwoo.models import *
from wingwoo.choices import *

register = template.Library()

@register.simple_tag
def get_all_members():
	return Member.objects.all()

@register.simple_tag
def get_object_admin_url(object):
	return reverse('admin:%s_%s_change' % (object._meta.app_label, object._meta.model_name), args=[object.id])

@register.simple_tag
def get_order_admin_url(order):
	if order.order_type == ORDER_TYPE_PURCHASE:
		purchase_order = PurchaseOrder.objects.get(id = order.id)
		if purchase_order.purchase_order_type == PURCHASE_ORDER_TYPE_GENERAL:
			return get_object_admin_url(GeneralPurchaseOrder.objects.get(id = order.id))
		else:
			return get_object_admin_url(CarpetPurchaseOrder.objects.get(id = order.id))
	else:
		return get_object_admin_url(RepairOrder.objects.get(id = order.id))

@register.simple_tag
def get_to_quote_orders():
	return Order.objects.filter(status = ORDER_STATUS_WAIT_FOR_QUOTATION)

@register.simple_tag
def get_waiting_orders():
	orders = []
	orders_queryset = Order.objects.filter(status = ORDER_STATUS_WAIT_FOR_PAYMENT)
	for order in orders_queryset:
		orders.append({
			'object': order,
			'calculated_price': order.get_price()
		})

	return orders

@register.simple_tag
def get_to_make_orders():
	return Order.objects.filter(status = ORDER_STATUS_MANUFACTURING)

@register.simple_tag
def get_to_deliver_orders():
	return Order.objects.filter(status = ORDER_STATUS_READY_TO_DELIVER)

@register.simple_tag
def is_repair_order(order):
	return (order.order_type == ORDER_TYPE_REPAIR)