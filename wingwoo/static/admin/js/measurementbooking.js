$(document).ready(function() {
    var updateOrderOptions = function() {
        var href = window.location.href
        var booking_id;
        if (href.indexOf('add') >= 0)
            booking_id = '';
        else
            booking_id = href.substring(href.lastIndexOf("booking/")+8, href.lastIndexOf("/change/"));

        $.ajax({
            url: '/getorderoptions/',
            type: 'POST',
            data: 'member_id=' + $('select#id_member').val() + '&mb_id=' + booking_id,
            headers: {"X-CSRFToken": getCookie("csrftoken")},
            success: function(data) {
                $('select#id_orders').multiselect('dataprovider', data);
                console.log(data)
            }
        });
    }

    $('select#id_member').multiselect({
        buttonWidth: '79%',
        enableFiltering: true,
        disableIfEmpty: true,
        templates: {
            ul: '<ul class="multiselect-container dropdown-menu" style="width:100%"></ul>',
            filter: '<li class="multiselect-item filter"><div class="input-group" style="width:auto"><span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
            filterClearBtn: ''
        }
    });

    $('select#id_orders').multiselect({
    	buttonWidth: '100%',
    	enableFiltering: true,
        disableIfEmpty: true,
    	templates: {
    		ul: '<ul class="multiselect-container dropdown-menu" style="width:100%"></ul>',
    		filter: '<li class="multiselect-item filter"><div class="input-group" style="width:auto"><span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
    		filterClearBtn: ''
    	}
    });

    $('select#id_member').change(function() {
    	updateOrderOptions();
    });

    updateOrderOptions();
});