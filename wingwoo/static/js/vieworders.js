var selectedDate = null;
var calendarModalWork = null;
var timeslotCreateId = null;
var timeslotId = null;
var timeslotDistrict = null;
var checkboxNum = 0;
var lastRowEdit
var timeslotChoices = ["12:00 - 14:00", "14:00 - 16:00", "16:00 - 18:00"];
var districtChoices = ["Sai Kung", "Hong Kong Island", "Tuen Mun", "Others"];
var orderTypes = ["", "Purchase", "Repair"];
var products = ["Carpet", "Cover", "Cushion", "Top"];
var orderStatusChoices = ['Book Measurement', 'Wait For Quotation', 'Waiting For Payment', 'Manufacturing', 'Ready To Deliver', 'Received'];
var bookingStatusChoices = ['Scheduled', 'Finished'];

var updateFileInput = function() {
	var inputElem = $('input#repair-image-input')
	var src = inputElem.attr("data-src")

	var options = {
	    overwriteInitial: true,
	    maxFileSize: 1500,
	    showClose: false,
	    showCaption: false,
	    showBrowse: true,
	    browseOnZoneClick: true,
	    removeClass: "btn btn-secondary",
    	removeLabel: "Delete",
    	removeIcon: "<i class=\"glyphicon glyphicon-trash\"></i> ",
	    elErrorContainer: '#kv-avatar-errors-2',
	    msgErrorClass: 'alert alert-block alert-danger',
	    defaultPreviewContent: '<h6 class="text-muted">Click to select</h6>',
	    layoutTemplates: {
	    	main2: '{preview} {remove} {browse}',
	    	footer: '<div class="file-thumbnail-footer">\n{actions}\n</div>'
	    },
	    allowedFileExtensions: ["jpg", "jpeg", "png", "gif"],
        ajaxDeleteSettings: {
	        headers: {"X-CSRFToken": getCookie("csrftoken")}
	    },
	    fileActionSettings: {
	    	showDrag: false
	    }
	}

	if (src != '') {
		options['initialPreview'] = [
            '<img class="kv-preview-data file-preview-image" src="' + src + '">'
        ];
        options['initialPreviewConfig'] = [
        	{downloadUrl: src, url: "/removeupload/", key: 1}
        ];
	}

	inputElem.fileinput(options)
	.on('change', function(event) {
	    inputElem.attr('data-changed', '1');
	})
	.on('filedeleted', function(event, key, jqXHR, data) {
	    inputElem.attr('data-changed', '1');
	});
};

var IdField = function(config) {
	jsGrid.Field.call(this, config);
}

var SizeField = function(config) {
	jsGrid.Field.call(this, config);
}

var ColorPatternField = function(config) {
    jsGrid.Field.call(this, config);
};

var MultipleCheckboxField = function(config) {
    jsGrid.Field.call(this, config);
};

var ImageField = function(config) {
    jsGrid.Field.call(this, config);
};

var imageDivStr = `
<div class="thumbnail">
	<img class="img-rounded" src="" alt="">
	<p></p>
</div>`;

var imageEditDivStr = 
`<button type="button" class="btn btn-default imagePickerModalButton" data-toggle="modal">
	<img class="img-rounded" src="" alt="">
	<p></p>
</button>`;

var checkboxDivStr = `
<div class='real-form-group form-inline'>
	<input type="checkbox" id="" disabled>
	<label class="multiples-checkbox-label" for=""></label>
</div>`;

var imageUploadItemDivStr = `
<div>
	<img class="repair-item-image" src="" alt="">
</div>`;

var imageUploadDivStr = `
<div class="kv-avatar">
    <div class="">
        <input id="repair-image-input" type="file" name="id_image" data-changed="0">
    </div>
</div>`;

IdField.prototype = new jsGrid.Field({
	filterTemplate: function() {
		var input = $('<input>').addClass('form-control').attr({'type': 'number', 'min': '0', 'step': '1'});
		
		return this._filterPicker = input;
	},
	filterValue: function() {
		return this._filterPicker.val();
	}
});

SizeField.prototype = new jsGrid.Field({
	align: "right",
	visible: true,
	filtering: true,
	sorting: true,
	itemTemplate: function(value, item) {
		if (value == '0.00') {
			return "-";
		} else {
			return value;
		}
	},
	filterTemplate: function() {
		this._inputFromPicker = $('<input>').addClass('form-control').attr({'type': 'number', 'min': '0', 'step': '0.01'});
		this._inputToPicker = $('<input>').addClass('form-control').attr({'type': 'number', 'min': '0', 'step': '0.01'});
		
		return $('<div>')
			.append($('<label>').text('From:').addClass('js-grid-filter-label'))
			.append(this._inputFromPicker)
			.append($('<label>').text('To:').addClass('js-grid-filter-label'))
			.append(this._inputToPicker);
	},
	filterValue: function() {
		return {
			from: this._inputFromPicker.val(),
			to: this._inputToPicker.val()
		}
	},
    editTemplate: function(value, item) {
		var inputObj;
		
		if (item.locked) {
			if (value == '0.00') {
				inputObj = '-';
				this._editPicker = $('<input>').attr('step', '0.01').attr('min', '0.01').attr('class', 'form-control').val('0.00');
			}
			else{
				inputObj = value;
				this._editPicker = $('<input>').attr('step', '0.01').attr('min', '0.01').attr('class', 'form-control').val(value);
			}
		}
		else {
			inputObj = $('<input>').attr('step', '0.01').attr('min', '0.01').attr('class', 'form-control').val(value);
			this._editPicker = inputObj
		}
			
		return inputObj;
	}, 
	editValue: function() {
		if (this._editPicker == '-') {
			return "0.00";
		}
		else {
			return this._editPicker.val();
		}
	}
});

ColorPatternField.prototype = new jsGrid.Field({
	align: "center",
	visible: true,
	filtering: false,
	sorting: true,

    itemTemplate: function(value, item) {
    	var imageDivObj;

		imageDivObj = $($.parseHTML(imageDivStr));
		imageDivObj.find('img').attr('src', value.image).attr('alt', value.code);
		imageDivObj.find('p').text(value.name);
		
    	return imageDivObj;
    },
    filterTemplate: function() {
		var input = $('<input>').addClass('form-control').attr({'type': 'text'});
		return this._filterPicker = input;
	},
	filterValue: function() {
		return this._filterPicker.val();
	},
    editTemplate: function(value, item) {
    	var imageEditDivObj;

    	imageEditDivObj = $($.parseHTML(imageEditDivStr));
		imageEditDivObj.find('img').attr('src', value.image).attr('alt', value.code).attr('data-id', value.id);
		imageEditDivObj.find('p').text(value.name);

		if (item.extra.carpet_type == null) {
			imageEditDivObj.on('click', function() {
				var selectedCode = $(this).find('img').eq(0).attr('alt');
				var selectedId = $(this).find('img').eq(0).attr('data-id');
				$("#colorModal").find(".modal-body #color-picker-wrapper ul li div").removeClass('selected');
				$("#colorModal").find(".modal-body #color-picker-wrapper ul li div img[alt='" + selectedCode + "']").parent().addClass('selected');
				$("#colorModal").find('#id_color').val(selectedId);
				$("#colorModal").modal('show');
				$("#colorModal").find("#button-color-submit").off().on('click', function() {
					var selected_div = $("#colorModal ul.image_picker_selector li div.selected").eq(0);
					imageEditDivObj.html(selected_div.html());
					imageEditDivObj.find("img").addClass("img-rounded");
					$("#colorModal").modal('hide');
				});
			});

			this._isColor = true;
		} else {
			imageEditDivObj.on('click', function() {
				var selectedCode = $(this).find('img').eq(0).attr('alt');
				var selectedId = $(this).find('img').eq(0).attr('data-id');
				$("#patternModal").find(".modal-body #pattern-picker-wrapper ul li div").removeClass('selected');
				$("#patternModal").find(".modal-body #pattern-picker-wrapper ul li div img[alt='" + selectedCode + "']").parent().addClass('selected');
				$("#colorModal").find('#id_pattern').val(selectedId);
				$("#patternModal").modal('show');
				$("#patternModal").find("#button-pattern-submit").off().on('click', function() {
					var selected_div = $("#patternModal ul.image_picker_selector li div.selected").eq(0);
					imageEditDivObj.html(selected_div.html());
					imageEditDivObj.find("img").addClass("img-rounded");
					$("#patternModal").modal('hide');
				});
			});

			this._isColor = false;
		}

		return this._editPicker = imageEditDivObj;
    }, 
    editValue: function() {
		var imgObj = this._editPicker.find('img').eq(0);
		var pObj = this._editPicker.find('p').eq(0);
		var id;
		
		if (!imgObj.attr('data-id')) {
			var selectObj = this._isColor ? $("select#id_color") : $("select#id_pattern");
			id = selectObj.data('picker').select.find(':selected').eq(0).val();
		} else {
			id = imgObj.attr('data-id');
		}
		
		return {
			id: id,
			image: imgObj.attr('src'),
			code: imgObj.attr('alt'),
			name: pObj.text()
		}
	}
});

var generateMultipleCheckboxHTML = function(objects, enabled) {
	var divObj = $("<div>");
	var checkboxDivObj
	checkboxDivObj = $($.parseHTML(checkboxDivStr));

	var objectsArray = $.map(objects, function(value, index) {
	    return [[index, value]];
	});

	for (var i = 0; i < objectsArray.length; i++) {
		var clone = checkboxDivObj.clone();
		clone.find('label').text(objectsArray[i][0]).attr('for', 'checkbox_' + checkboxNum);
		clone.find('input[type="checkbox"]').attr('id', 'checkbox_' + checkboxNum)
		checkboxNum++;

		if (objectsArray[i][1]) {
			clone.find('input[type="checkbox"]').prop('checked', true);
		}

		if (enabled) {
			clone.find('input[type="checkbox"]').prop('disabled', false);
		}

		divObj.append(clone);
	}

	return divObj;
};

MultipleCheckboxField.prototype = new jsGrid.Field({
	visible: true,
	filtering: false,
	sorting: false,

    itemTemplate: function(objects, item) {
    	return generateMultipleCheckboxHTML(objects, false);
    },
    filterTemplate: function() {
    	var objects = {
    		'waterproof': false,
    		'underlay': false,
    		'edge': false
    	}
    	return this._filterPicker = generateMultipleCheckboxHTML(objects, true);
    },
    filterValue: function() {
    	var objects = {};
    	var divObj = this._filterPicker;

    	var checkboxGroups = divObj.children();

    	for (var i = 0; i < checkboxGroups.length; i++) {
    		var checkboxGroup = checkboxGroups.eq(i);
    		var label = checkboxGroup.find('label').eq(0).text();
    		var value = checkboxGroup.find('input[type="checkbox"]').eq(0).is(':checked');
    		objects[label] = value;
    	}
    	return objects;
    },
    editTemplate: function(objects, item) {
    	return this._editPicker = generateMultipleCheckboxHTML(objects, true);
    },
    editValue: function() {
    	var objects = {};
    	var divObj = this._editPicker;

    	var checkboxGroups = divObj.children();

    	for (var i = 0; i < checkboxGroups.length; i++) {
    		var checkboxGroup = checkboxGroups.eq(i);
    		var label = checkboxGroup.find('label').eq(0).text();
    		var value = checkboxGroup.find('input[type="checkbox"]').eq(0).is(':checked');
    		objects[label] = value;
    	}
    	return objects;
    }
});

var btnCust = '<button type="button" class="btn btn-secondary" title="Add picture tags" ' + 
    'onclick="alert(\'Call your custom code here.\')">' +
    '<i class="glyphicon glyphicon-tag"></i>' +
    '</button>'; 

ImageField.prototype = new jsGrid.Field({
	visible: true,
	filtering: false,
	sorting: false,

	itemTemplate: function(value, item) {
		var imageUploadItemDivObj;
		imageUploadItemDivObj = $($.parseHTML(imageUploadItemDivStr));
		imageUploadItemDivObj.find("img").attr("src", value);
		return imageUploadItemDivObj;
	},
	editTemplate: function(value, item) {
		var imageUploadDivObj;
		imageUploadDivObj = $($.parseHTML(imageUploadDivStr));
		imageUploadDivObj.find("input").attr("data-src", value);
		return this._editPicker = imageUploadDivObj;
	},
	editValue: function() {
		var src = this._editPicker.find("div.kv-file-content > img").eq(0).attr("src")
		return src == undefined ? '' : this._editPicker.find("div.kv-file-content > img").eq(0).attr("src");
	}
});

jsGrid.fields.id = IdField;
jsGrid.fields.size = SizeField;
jsGrid.fields.colorpattern = ColorPatternField;
jsGrid.fields.multiplecheckbox = MultipleCheckboxField;
jsGrid.fields.image = ImageField;

$(document).ready(function() {
	$(document).click(function(event) {
		$('.jsgrid').each(function(index) {
			if ($(this).find('.jsgrid-edit-row').is(":visible")) {
				if (!$(event.target).closest($(this).find('.jsgrid-grid-body')).length) {
					if (!$('.modal').is(":visible")) {
						$(this).jsGrid("cancelEdit");
					}
		        }
			}
		});       
	});
	
	$("#id_color, #id_pattern").imagepicker({
		show_label: false
	});

	var bookButtonToggle = function() {
    	if (selectedDate != null && $('#receivable-agree').is(':checked')) {
    		$('#button-book').prop('disabled', false);
    		$('#button-book-wrapper').tooltip('destroy');
    	}
    	else {
    		$('#button-book').prop('disabled', true);
    		$('#button-book-wrapper').tooltip();
    	}
    };

    $('#receivable-agree').on('change', function() {
    	bookButtonToggle();
    });

	function formatNumberForDisplay(numberToFormat) {
		var formatter = new Intl.NumberFormat('en-US', {
			style: 'currency',
			currency: 'USD',
			digits: 2,
		});
		
		return formatter.format(numberToFormat);
	}

	var loadOrders = function(filter, is_booking, is_paid, is_purchase, grid) {
		var url, d;

		if (is_booking) {
			url = "/viewbookings/";
		} else {
			url = "/vieworders/" + is_paid + "/" + is_purchase + "/";
		}

		$.ajax({
			url: url,
			type: 'POST',
			data: filter,
			headers: {"X-CSRFToken": getCookie("csrftoken")},
			async: false
		}).done(function(response) {
			if (response.total_count <= 0) {
				grid.find('.jsgrid-grid-header').removeClass('jsgrid-header-scrollbar');
				grid.find('.jsgrid-grid-header').css({
					'overflow-x': 'auto'
				});
			} else {
				grid.find('.jsgrid-grid-header').scrollLeft(grid.find('.jsgrid-grid-body')[0].scrollLeft);
				grid.find('.jsgrid-grid-header').scrollLeft();
				grid.find('.jsgrid-grid-header').css({
					'overflow-x': 'hidden'
				});
			}

			d = response;
		});

		return {
			data: d.data,
			itemsCount: d.total_count
		};
	}

	var getFieldsArray = function(is_paid, is_purchase, grid) {
		fields = [
			{ name: "id", title: "ID", type: "id", width: 80, editing: false, filtering: true, sorting: true, align:'center'
			},
			{ name: "order_type", title: "Type", type: "text", width: 80, editing: false, visible: false },
			{ name: "product", title: "Product", type: "text", width: 100, editing: false, filtering: true, 
				filterTemplate: function() {
					var select = $('<select>').addClass('form-control');
					select.append($('<option>').text('').attr({'value': ''}));
					for (var i = 0; i < products.length; i++) {
						select.append($('<option>').text(products[i]).attr({'value': products[i]}));
					}
					return this._filterPicker = select;
				},
				filterValue: function() {
					return this._filterPicker.val();
				}
			},
			{ name: "boat.model", title: "Boat", type: "text", width: 100, editing: false, filtering: true,	
				filterTemplate: function() {
					var input = $('<input>').addClass('form-control').attr({'type': 'text'});
					return this._filterPicker = input;
				},
				filterValue: function() {
					return this._filterPicker.val();
				}
			},
			{ name: "boat.district", title: "District", type: "number", width: 0, editing: false, visible: false },
			{ name: "locked", title: "Locked", type: "checkbox", width: 0, editing: false, visible: false },
			{ name: "status", title: "Status", type: "text", width: 180, editing: false, filtering: true, sorting: true,  
				filterTemplate: function() {
					var select = $('<select>').addClass('form-control');
					var from = is_paid ? 3 : 0;
					var to = is_paid ? 6 : 3;

					select.append($('<option>').text(orderStatusChoices[i]).attr({'value': ''}));

					for (var i = from; i < to; i++) {
						select.append($('<option>').text(orderStatusChoices[i]).attr({'value': i}));
					}
					return this._filterPicker = select;
				},
				filterValue: function() {
					return this._filterPicker.val();
				}
			},
			{ name: "size.length", title: "Length (ft)", type: "size", width: 85, editing: true, sorting: true },
			{ name: "size.width", title: "Width (ft)", type: "size", width: 85, editing: true, sorting: true }, 
			{ name: "size.height", title: "Height (ft)", type: "size", width: 85, editing: true, sorting: true }
		];

		if (is_purchase) {
			fields.push({ name: "extra.quantity", title: "Quantity", type: "number", width: 75, editing: true, filtering: true, sorting: true, 
				filterTemplate: function() {
					this._inputFromPicker = $('<input>').addClass('form-control').attr({'type': 'number', 'min': '0', 'step': '1'});
					this._inputToPicker = $('<input>').addClass('form-control').attr({'type': 'number', 'min': '0', 'step': '1'});
					
					return $('<div>')
						.append($('<label>').text('From:').addClass('js-grid-filter-label'))
						.append(this._inputFromPicker)
						.append($('<label>').text('To:').addClass('js-grid-filter-label'))
						.append(this._inputToPicker);
				},
				filterValue: function() {
					return {
						from: this._inputFromPicker.val(),
						to: this._inputToPicker.val()
					}
				},
				editTemplate: function(value, item) {
					var inputObj = $('<input>').attr('step', '1').attr('min', '1').attr('class', 'form-control').val(value);
					return this._editPicker = inputObj;
				}, 
				editValue: function() {
					return this._editPicker.val();
				}
			});
			fields.push({ name: "extra.colorpattern", title: "Color/Pattern", width: 110, type: "colorpattern", editing: true, filtering: true, sorting: true });
			fields.push({ name: "extra.customization", title: "Add-ons", width: 90, type: "multiplecheckbox", editing: true, filtering: true, sorting: false });
		} else {
			fields.push({ name: "extra.image", title: "Image", width: 200, type: "image", editing: true, filtering: false, sorting: false });
		}

		fields.push({
			name: "price", title: "Price", type: "number", width: 100, editing:false, filtering: true, sorting: true, css: "grid_order_price",
				filterTemplate: function() {
					this._inputFromPicker = $('<input>').addClass('form-control').attr({'type': 'number', 'min': '0', 'step': '0.01'});
					this._inputToPicker = $('<input>').addClass('form-control').attr({'type': 'number', 'min': '0', 'step': '0.01'});
					
					return $('<div>')
						.append($('<label>').text('From:').addClass('js-grid-filter-label'))
						.append(this._inputFromPicker)
						.append($('<label>').text('To:').addClass('js-grid-filter-label'))
						.append(this._inputToPicker);
				},
				filterValue: function() {
					return {
						from: this._inputFromPicker.val(),
						to: this._inputToPicker.val()
					}
				}, 
				itemTemplate: function(value) {
					return formatNumberForDisplay(value);
				}
			}
		)

		
		fields.push({
			type: "control", width: 90, editButton:true, deleteButton:true,
				_createFilterSwitchButton: function() {
			   		return this._createOnOffSwitchButton("filtering", this.searchModeButtonClass, false);
				},
            	itemTemplate: function(value, item) {
            		if (!is_paid) {
	                    var result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);

	                    var editIcon = $("<i>").attr({class: "fa fa-lg fa-pencil-square-o jsgrid-button-icon jsgrid-edit-icon"});
	                    var editButton = $("<a>")
	                    .attr({"class": "jsgrid-custom-button jsgrid-custom-edit-button", "data-toggle": "tooltip", "data-placement": "top", "title":"Edit"})
	                    .on('click', function(e) {
	                		grid.jsGrid('editItem', item);
	                    })
	                	editButton.append(editIcon)

	                    var deleteIcon = $("<i>").attr({class: "fa fa-lg fa-trash-o jsgrid-button-icon jsgrid-delete-icon"});
	                    var deleteButton = $("<a>")
	                    .attr({"class": "jsgrid-custom-button jsgrid-custom-delete-button"})
	                    .on('click', function(e) {
	                		grid.jsGrid('deleteItem', item);
							e.stopPropagation();
	                    })
	                	deleteButton.append(deleteIcon)

	                    var deleteButtonTooltipTitle;
	                    if (item.removable_data.removable == false) {
	                    	deleteButtonTooltipTitle = "This order has a related measurement booking that is not deletable!";
	                    	deleteButton.attr('data-disabled', 'disabled');
	                    } else {
	                    	deleteButtonTooltipTitle = "Delete";
	                    }

	                    var editDiv = $('<span>').addClass('jsgrid-button-wrapper').append(editButton);
						var deleteDiv = $('<span>').addClass('jsgrid-button-wrapper').append(deleteButton);
						
						deleteDiv.tooltip({
							"title": deleteButtonTooltipTitle
						});

	                    if (item.status == "Book Measurement") {
	                    	var bookIcon = $("<i>").attr({class: "fa fa-lg fa-calendar-plus-o jsgrid-book-icon"});
	                        var bookButton = $("<a>")
	                        .attr({class: "jsgrid-custom-button jsgrid-book-button", "data-toggle": "tooltip", "data-placement": "top", "title":"Book Measurement"})
	                        .on('click', function(e) {
	                        	calendarModalWork = 'create';
	                        	timeslotId = item.id;
	                    		timeslotDistrict = item.boat.district;
	                    		$('#button-book').text('Book');
	                    		$('#receivable-agree').prop('checked', false);
	                    		$('#timeslotModal').modal('show');
	                    		e.stopPropagation();
	                        })
	                    	bookButton.append(bookIcon)
	                    	var bookDiv = $('<span>').addClass('jsgrid-button-wrapper').append(bookButton);
	                    }

						return $("<div>").append(editDiv).append(deleteDiv).append(bookDiv);
					}
                }
		});
		
		return fields;
	};

	var getOnItemEditingCallback = function(args, is_paid, is_purchase) {
		var item = args.item;
		var row = args.row;

		if (!is_paid) {
			if (!is_purchase) {
				console.log(item);
			}
		} else {
			args.cancel = true;
		}
	}

	var getOnItemUpdatingCallback = function(args, is_purchase, grid) {
		var item = args.item;
		var formData = new FormData();

		console.log(item)

		formData.append("id", item.id);

		if (item.size.length != "0.00")
			formData.append("length", item.size.length);

		if (item.size.width != "0.00")
			formData.append("width", item.size.width);

		if (item.size.height != "0.00")
			formData.append("height", item.size.height);

		if (is_purchase) {
			console.log("purchase order update item callback");

			var url;

			formData.append("quantity", item.extra.quantity);

			if (item.product != "Carpet") {
				formData.append("color", item.extra.colorpattern.id);
				url = '/editgeneralpurchaseorder/';
			} else {
				formData.append("pattern", item.extra.colorpattern.id);
				formData.append("waterproof", item.extra.customization.waterproof);
				formData.append("underlay", item.extra.customization.underlay);
				formData.append("edge", item.extra.customization.edge);
				url = '/editcarpetpurchaseorder/';
			}

			$.ajax({
	            url: url,
	            type: 'POST',
	            processData: false,
				contentType: false,
	            data: formData,
	            headers: {"X-CSRFToken": getCookie("csrftoken")},
	            success: function(data) {
	            	if (data.result == true) {
	            		var gridData = args.grid.data;
	                	gridData[args.itemIndex].price = data.data.price;
	                	grid.jsGrid("option", "data", gridData);
	                	generalNotify('success', 'Updated Successfully', null);
	                } else {
	                	var errorMessage = 'Update Failed';
	                	if (data.message)
	                		errorMessage += (': ' + data.message);
	                	generalNotify('danger', errorMessage, null);
	                	setTimeout(function() { location.reload(); }, 1500);
	                }
	            }
	        });
		} else {
			console.log("repair order update item callback")

			var imageInputElem = $('input#repair-image-input')

			var files = imageInputElem.get(0).files;
			formData.append("image", files[0]);
			formData.append("image_changed", imageInputElem.attr('data-changed'));

			$.ajax({
	            url: '/editrepairorder/',
	            type: 'POST',
	            processData: false,
				contentType: false,
	            data: formData,
	            headers: {"X-CSRFToken": getCookie("csrftoken")},
	            success: function(data) {
	                if (data.result == true) {
	                	var gridData = args.grid.data;
	                	gridData[args.itemIndex].image = data.data.image;
	                	gridData[args.itemIndex].price = data.data.price;
	                	grid.jsGrid("option", "data", gridData);
	                	//grid.find('.repair-item-image').eq(args.itemIndex).attr('src', data.data.image);
	                	generalNotify('success', 'Updated Successfully', null);
	                } else {
	                    var errorMessage = 'Update Failed';
	                	if (data.message)
	                		errorMessage += (': ' + data.message);
	                	generalNotify('danger', errorMessage, null);
	                	//setTimeout(function() { location.reload(); }, 1500);
	                }
	            }
	        });
		}
	};

	var getRowClickCallback = function(args, is_paid, is_purchase, grid) {
		if (grid.jsGrid('option', 'editing')) {
			grid.jsGrid('editItem', ($(args.event.target).closest("tr")));
			if (!is_paid && !is_purchase) {
				updateFileInput();
			}
		}
	};

	var generalConfig = {
		width: "100%",
		height: "auto",
		heading: true,
		inserting: false,
		filtering: true,
		editing: true,
		sorting: true,
		paging: true,
		autoload: true,
		pageLoading: true,
		confirmDeleting: false,

		pagerContainer: null,
	    pageIndex: 1,
	    pageSize: 3,

	    pageButtonCount: 10,
		pagerFormat: "{first} {prev} {pages} {next} {last}",
		pagePrevText: "Prev",
		pageNextText: "Next",
		pageFirstText: "<<",
		pageLastText: ">>",
		pageNavigatorNextText: "...",
		pageNavigatorPrevText: "...",
	    pagerContainerClass: "jsgrid-pager-container",
        pagerClass: "jsgrid-pager",
        pagerNavButtonClass: "jsgrid-pager-nav-button",
        pagerNavButtonInactiveClass: "jsgrid-pager-nav-inactive-button",
        pageClass: "jsgrid-pager-page",
        currentPageClass: "jsgrid-pager-current-page"
	}

	var getGridConfig = function(is_paid, is_purchase, grid) {
		var config = {
			controller: {
				loadData: function(filter) {
					return loadOrders(filter, false, is_paid, is_purchase, grid);
				}
			},
			fields: getFieldsArray(is_paid, is_purchase, grid),
			rowClick: function(args) {
				getRowClickCallback(args, is_paid, is_purchase, grid)
	        },
			onItemUpdating: function(args) {
		        getOnItemUpdatingCallback(args, is_purchase, grid);
		    },
		    onItemEditing: function(args) {
		    	getOnItemEditingCallback(args, is_paid, is_purchase)
		    },
		    onItemDeleting: function(args) {
		    	console.log('deleting')
		    	if (!args.item.deleteDone) { // custom property for confirmation
			        args.cancel = true; // cancel deleting

			        var deleteButton = args.row.find(".jsgrid-custom-delete-button").eq(0);

			        deleteButton.tooltip('hide');
			        $("div.confirmation").confirmation('hide');

			        if (args.item.removable_data.removable) {
			        	if (!args.item.deleting) {
				        	$.ajax({
				                url: '/candeleteorder/',
				                type: 'POST',
				                data: 'id=' + args.item.id,
				                headers: {"X-CSRFToken": getCookie("csrftoken")},
				                success: function(data) {
				                    if (data.result == true) {
				                    	var confirmMessage;
				                    	if (data.only_related && data.payable) {
				                    		confirmMessage = 'Deletion would cause $300 measurement fee, confirm?';
				                    	} else {
				                    		confirmmessage = 'Confirm?';
				                    	}
				                    	
				                        deleteButton.confirmation({
											title: confirmMessage,
											placement: 'left',
											singleton: true,
											popout: true,
											rootSelector: deleteButton,
											container: $('#fullpage'),
											onConfirm: function() {
												args.item.deleting = true;
												$.ajax({
									                url: '/deleteorder/',
									                type: 'POST',
									                data: 'id=' + args.item.id,
									                headers: {"X-CSRFToken": getCookie("csrftoken")},
									                success: function(data) {
									                	if (data.result == true) {
									                		args.item.deleteDone = true;
									                		grid.jsGrid('deleteItem', args.item);
									                		$("#view-booking-grid").jsGrid("loadData");
									                		generalNotify('success', 'Deleted Successfully', null);
									                	} else {
									                		var extraMessage = '';
									                		if (data.message)
									                			extraMessage = (": " + data.message);
									                		generalNotify('warning', 'Deletion Failed' + extraMessage, null);
									                	}
									                	
									                }
									            });
											}
										}).confirmation('toggle');
				                    } else {
				                    	deleteButton.popover('destroy');
				                    	generalNotify('warning', 'This order cannot be deleted because it has a related measurement booking which is within 2 days', null);
				                    }
				                }
				            });
			        	}
			        } else {
			        	generalNotify('warning', 'This order cannot be deleted because it has a related measurement booking which is within 2 days', null);
			        }
			        
			    } else {
			    	console.log(args.item.deleteDone)
			    }
		    }
		};

		return $.extend(generalConfig, config);
	};

	$("#view-unpaid-purchase-orders-grid").jsGrid(getGridConfig(0, 1, $("#view-unpaid-purchase-orders-grid")));
	$("#view-unpaid-repair-orders-grid").jsGrid(getGridConfig(0, 0, $("#view-unpaid-repair-orders-grid")));
	$("#view-paid-purchase-orders-grid").jsGrid(getGridConfig(1, 1, $("#view-paid-purchase-orders-grid")));
	$("#view-paid-repair-orders-grid").jsGrid(getGridConfig(1, 0, $("#view-paid-repair-orders-grid")));
	$("#view-booking-grid").jsGrid($.extend(
		generalConfig, 
		{
			controller: {
				loadData: function(filter) {
					return loadOrders(filter, true, null, null, $("#view-booking-grid"));
				}
			},
			fields: [
				{ name: "id", title: "ID", type: "id", width: 80, editing: false, filtering: true, sorting: true, align: 'center' }, 
				{ name: "date", title: "Date", type: "string", width: 100, editing: false, filtering: true, sorting: true, align: 'center',
					filterTemplate: function() {
						var div = $('<div>').addClass('row')
						var div2 = $('<div>').addClass('col-md-12')

						var input =  this._filterPicker = $('<input>').addClass('form-control').attr({'type': 'text'}).datetimepicker({
							format: 'YYYY-MM-DD',
							showClear: true,
							showClose: true,
							showTodayButton: true,
							toolbarPlacement: 'top',
							widgetParent: $("#view-booking-grid"),
							widgetPositioning: {
								horizontal: 'auto',
								vertical: 'bottom'
							},
							useCurrent: false
						});

						return this._filterPicker = div.append(div2.append(input))
					},
					filterValue: function() {
						return this._filterPicker.find('input').eq(0).val();
					}
				}, 
				{ name: "timeslot", title: "Timeslot", type: "string", width: 135, editing: false, filtering: true, sorting: true, align: 'center', 
					itemTemplate: function(value, item) {
				    	return timeslotChoices[parseInt(value)];
				    }, 
					filterTemplate: function() {
						var select = $('<select>').addClass('form-control');

						select.append($('<option>').text('').attr({'value': ''}));

						for (var i = 0; i < timeslotChoices.length; i++) {
							select.append($('<option>').text(timeslotChoices[i]).attr({'value': i}));
						}
						return this._filterPicker = select;
					},
					filterValue: function() {
						return this._filterPicker.val();
					}
				}, 
				{ name: "address", title: "Address", type: "string", width: 180, editing:false, filtering: true, sorting:true, align:'center', 
					itemTemplate: function(value, item) {
				    	return '[' + districtChoices[parseInt(value.district)] + '] ' + value.pier;
				    },
				    filterTemplate: function() {
						var input = $('<input>').addClass('form-control').attr({'type': 'text'});
						return this._filterPicker = input;
					},
					filterValue: function() {
						return this._filterPicker.val();
					}
				},
				{ name: "status", title: "Status", type: "string", width: 135, editing:false, filtering: true, sorting:true, align:'center', 
					filterTemplate: function() {
						var select = $('<select>').addClass('form-control');

						select.append($('<option>').text('').attr({'value': ''}));

						for (var i = 0; i < bookingStatusChoices.length; i++) {
							select.append($('<option>').text(bookingStatusChoices[i]).attr({'value': i}));
						}
						return this._filterPicker = select;
					},
					filterValue: function() {
						return this._filterPicker.val();
					}
				}, 
				{ name: "orders", title: "Including Orders", type: "string", width: 250, editing:false, sorting:false, align:'center' , 
					itemTemplate: function(value, item) {
						var str = '';
				    	for (var i = 0; i < value.length; i++) {
				    		str += (value[i] + '</br>');
				    	}
				    	return str;
				    }
				},
				{ type: "control", width: 110, editButton:true, deleteButton:true,
					_createFilterSwitchButton: function() {
				   		return this._createOnOffSwitchButton("filtering", this.searchModeButtonClass, false);
					},
	            	itemTemplate: function(value, item) {
	            		var result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);

	            		if (item.status == 'Scheduled') {
	            			var editIcon = $("<i>").attr({class: "fa fa-lg fa-calendar jsgrid-button-icon jsgrid-edit-icon"});
	                        var editButton = $("<a>")
	                        .attr({"class": "jsgrid-custom-button jsgrid-custom-edit-button"})
	                        .on('click', function(e) {
	                        	$.ajax({
					                url: '/candeletebooking/',
					                type: 'POST',
					                data: 'id=' + item.id,
					                headers: {"X-CSRFToken": getCookie("csrftoken")},
					                success: function(data) {
					                    if (data.result == true) {
					                    	console.log('can edit')
					                    	calendarModalWork = 'edit';
					                    	timeslotId = item.id;
				                    		timeslotDistrict = item.address.district;
				                    		$('#button-book').text('Change');
				                    		$('#receivable-agree').prop('checked', false);
				                    		$('#timeslotModal').modal('show');
					                    } else {
					                    	generalNotify('warning', 'This booking cannot be deleted because it will be held within 2 days', null);
					                    }
					                }
					            });
	                    		//$("#view-booking-grid").jsGrid('editItem', item);
	                        })
	                    	editButton.append(editIcon)

		                    var deleteIcon = $("<i>").attr({class: "fa fa-lg fa-trash-o jsgrid-button-icon jsgrid-delete-icon"});
	                        var deleteButton = $("<a>")
	                        .attr({"class": "jsgrid-custom-button jsgrid-custom-delete-button"})
	                        .on('click', function(e) {
	                    		$("#view-booking-grid").jsGrid('deleteItem', item);
								e.stopPropagation();
	                        })
	                    	deleteButton.append(deleteIcon)

		                    var editButtonTooltipTitle, deleteButtonTooltipTitle;
		                    if (item.removable == false) {
		                    	editButtonTooltipTitle = "You can only edit 2 days before the booked date"
		                    	deleteButtonTooltipTitle = "You can only delete 2 days before the booked date";
		                    	editButton.attr('disabled', 'disabled')
		                    	deleteButton.attr('data-disabled', 'disabled');
		                    } else {
		                    	editButtonTooltipTitle = "Change Timeslot";
		                    	deleteButtonTooltipTitle = "Delete";
		                    }

		                    var editDiv = $('<span>').addClass('jsgrid-button-wrapper').append(editButton);
							var deleteDiv = $('<span>').addClass('jsgrid-button-wrapper').append(deleteButton);
							
							editDiv.tooltip({
								"title": editButtonTooltipTitle
							});

							deleteDiv.tooltip({
								"title": deleteButtonTooltipTitle
							});
		                    
							return $('<div>').append(editDiv).append(deleteDiv);
	            		} else {
	            			return '';
	            		}
	            		
	            	}
	            }
			],
			rowClick: function(args) {
			},
			onItemUpdating: function(args) {
				
		    },
		    onItemDeleting: function(args) {
		    	if (!args.item.deleteDone) { // custom property for confirmation
		    		console.log(args.item.deleteDone)
			        args.cancel = true; // cancel deleting

			        var deleteButton = args.row.find(".jsgrid-custom-delete-button").eq(0);

			        deleteButton.tooltip('hide');
			        $("div.confirmation").confirmation('hide');

			        if (args.item.removable) {
			        	if (!args.item.deleting) {
				        	$.ajax({
				                url: '/candeletebooking/',
				                type: 'POST',
				                data: 'id=' + args.item.id,
				                headers: {"X-CSRFToken": getCookie("csrftoken")},
				                success: function(data) {
				                    if (data.result == true) {
				                    	var confirmMessage;
				                    	confirmmessage = 'Confirm?';
				                    	
				                        deleteButton.confirmation({
											title: confirmMessage,
											placement: 'left',
											singleton: true,
											popout: true,
											rootSelector: deleteButton,
											container: $('#fullpage'),
											onConfirm: function() {
												console.log('confirmed');
												args.item.deleting = true;
												$.ajax({
									                url: '/deletebooking/',
									                type: 'POST',
									                data: 'id=' + args.item.id,
									                headers: {"X-CSRFToken": getCookie("csrftoken")},
									                success: function(data) {
									                	if (data.result == true) {
									                		args.item.deleteDone = true;
									                		$("#view-booking-grid").jsGrid('deleteItem', args.item);
									                		$("#view-unpaid-purchase-orders-grid").jsGrid("loadData");
									                		$("#view-unpaid-repair-orders-grid").jsGrid("loadData");
									                		generalNotify('success', 'Deleted Successfully', null);
									                	} else {
									                		var extraMessage = '';
									                		if (data.message)
									                			extraMessage = (": " + data.message);
									                		generalNotify('warning', 'Deletion Failed' + extraMessage, null);
									                	}
									                	
									                }
									            });
											}
										}).confirmation('toggle');
				                    } else {
				                    	deleteButton.popover('destroy');
				                    	generalNotify('warning', 'This booking cannot be deleted because it will be held within 2 days', null);
				                    }
				                }
				            });
			        	}
			        } else {
			        	generalNotify('warning', 'This booking cannot be deleted because it will be held within 2 days', null);
			        }
			        
			    } else {
			    	console.log(args.item.deleteDone)
			    }
		    }
		}
	));

	$("#view-unpaid-purchase-orders-grid").jsGrid("option", "filtering", false);
	$("#view-unpaid-repair-orders-grid").jsGrid("option", "filtering", false);
	$("#view-paid-purchase-orders-grid").jsGrid("option", "filtering", false);
	$("#view-paid-repair-orders-grid").jsGrid("option", "filtering", false);
	$("#view-booking-grid").jsGrid("option", "filtering", false);

	$(".config-panel input[type=checkbox]").on("click", function() {
        var $cb = $(this);
        $("#view-orders-Grid").jsGrid("option", $cb.attr("id"), $cb.is(":checked"));
    });

    $('#calendar').fullCalendar({
        themeSystem: 'bootstrap4',
        header: {
            left: 'month,agendaWeek',
            right: 'today,prev,next',
            center: 'title'
        },
        titleFormat: 'MMMM D, YYYY',
        buttonText: {
            today: 'Today',
            month: 'Month',
            week: 'Week',
            day: 'Day',
            list: 'List'
        },
        defaultView: 'agendaWeek',
        allDaySlot: false,	
        slotDuration: '02:00:00',
        slotLabelInterval: '02:00:00',
        slotLabelFormat: 'kk:mm',
        minTime: '12:00:00',
        maxTime: '18:00:00',
        contentHeight: 'auto',
        displayEventTime: true,
        displayEventEnd: true,
        nowIndicator: false,
        selectable: true,
        unselectAuto: false,
        selectOverlap: false,
        weekends: true,
        timezoneParam: 'Asia/Hong_Kong',
        selectConstraint: "businessHours",
        showNonCurrentDates: false,
        fixedWeekCount: false,
        events: {
            url: '/schedule',
            type: 'GET',
			data: function() {
				return {district: timeslotDistrict};
			},
            cache: false
        },
        loading: function(isLoading, view) {
        	if (isLoading) {
        		$('#calendar').LoadingOverlay("show");
        	} else {
        		$("#calendar").LoadingOverlay("hide", true);
        	}
        },
        viewRender: function (view, element) {
		    var minDate = moment(),
			maxDate = moment().add(6, 'months');
			// Past
			if (minDate >= view.start) {
				view.calendar.header.disableButton('prev');
			}
			else {
				view.calendar.header.enableButton('prev');
			}
			// Future
	        if (maxDate >= view.start && maxDate <= view.end) {
	            view.calendar.header.disableButton('next');
	        } else {
	            view.calendar.header.enableButton('next');
	        }
		},
        dayRender: function(date, cell) {
        	if (moment.duration(date.local().startOf('day').diff(moment().startOf('day').startOf('day'))).asDays() < 3) {
        		cell.addClass('fc-nonbusiness fc-bgevent');
        	}
        },
        select: function(start, end) {
			$(".fc-highlight").css("background", "#ffff80");
			selectedDate = start;
            bookButtonToggle();
		},
		unselect: function(jsEvent, view) {
			selectedDate = null;
			bookButtonToggle();
		},
		selectAllow: function(selectInfo) {
			var start = selectInfo.start;
			var end = selectInfo.end;
			var duration = moment.duration(end.diff(start));
			var hours = duration.asHours();

			if (end.local() < moment() || 
				moment.duration(end.local().startOf('day').diff(moment().startOf('day'))).asDays() < 3 || 
				hours > 2
				) {
				selectedDate = null;
				bookButtonToggle();
				return false;
			}

			return true;
		},
        dayClick: function(date, jsEvent, view) {
            if (view.name == 'agendaWeek') {
            	
            } else if (view.name == 'month') {
				$('#calendar').fullCalendar('changeView', 'agendaWeek');
				$('#calendar').fullCalendar('gotoDate', date);    
            }
        }
    });

    

    $('#calendar').fullCalendar('addEventSource', [{
    	title: 'previousDays',
		start: moment().startOf('month'),
		end: moment().startOf('day').add(3, 'days'),
		rendering: 'background',
		backgroundColor: 'rgb(215, 215, 215)'
    }])

    $('#timeslotModal').on('show.bs.modal', function () {
    	jQuery(window).resize();
    	var district = timeslotDistrict;
    	var districtBuinessWeekdays;

    	if (district == "0") {
    		districtBuinessWeekdays = [ 1, 3 ]
    	} else if (district == "1" || district == "2") {
    		districtBuinessWeekdays = [ 2, 4 ]
    	} else {
    		districtBuinessWeekdays = [ 5 ]
    	}

    	$('#calendar').fullCalendar('option', {
    		businessHours: [{
				dow: districtBuinessWeekdays,
				start: '12:00',
				end: '18:00',
			}]
    	});
	});

	$('#timeslotModal').on('shown.bs.modal', function () {
		$('#calendar').fullCalendar('prev');
		$('#calendar').fullCalendar('today');
		//$("#calendar").fullCalendar('rerenderEvents');
		//$("#calendar").fullCalendar('render');
	});

	$('#button-book').confirmation({
		title: 'Confirm?',
		placement: 'top',
		popout: true,
		rootSelector: $('#button-book'),
		onConfirm: function() {
			if (selectedDate != null && timeslotId != null && $('#receivable-agree').is(':checked')) {
				var url, idData;
				if (calendarModalWork == 'create') {
					url = '/createbooking/';
					idData = 'order_id=' + timeslotId;
				} else {
					url = '/editbooking/';
					idData = 'booking_id=' + timeslotId;
				}
				
				$.ajax({
	                url: url,
	                type: 'POST',
	                data: idData + '&date=' + selectedDate.format("YYYY-MM-DD") + '&timeslot=' + selectedDate.format("HH:mm:ss"),
	                headers: {"X-CSRFToken": getCookie("csrftoken")},
	                success: function(data) {
	                    if (data.result == true) {
	                        location.reload();
	                    } else {
	                    	var message = data.message ? data.message : 'Booking Failed';
	                    	generalNotify('warning', message, $('#timeslotModal'));
	                    }
	                }
	            });
	        }
		}
	});

	$('#button-book').on('show.bs.confirmation', function() {
		var dateString = selectedDate.format("YYYY-MM-DD");
		var startString = selectedDate.format("HH:mm");
		var endString = selectedDate.add(2, 'h').format("HH:mm");

		selectedDate.subtract(2, 'h')

		var confirmString = 'Confirm booking on ' + dateString + ' ' + startString + '-' + endString;

		$(this).attr('data-original-title', confirmString);
		$(this).attr('data-title', confirmString);
	});

	$('[data-toggle="tooltip"]').tooltip({container: 'body'});
});