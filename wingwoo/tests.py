from django.test import TestCase

from django_nyt.models import NotificationType, Settings, Subscription, Notification
from django_nyt.utils import notify, subscribe

# Create your tests here.