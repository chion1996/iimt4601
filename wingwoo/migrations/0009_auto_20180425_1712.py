# Generated by Django 2.0.4 on 2018-04-25 09:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wingwoo', '0008_auto_20180413_0003'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactMessage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('email', models.CharField(max_length=100)),
                ('content', models.CharField(max_length=10240)),
                ('read', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.IntegerField(choices=[(0, 'Book Measurement'), (1, 'Wait For Quotation'), (2, 'Waiting For Payment'), (3, 'Manufacturing'), (4, 'Ready To Deliver'), (5, 'Received'), (6, 'Unfulfilled')], default=0),
        ),
    ]
