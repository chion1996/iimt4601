$(function(){
    var bookingModalSubmit = function() {
        var data = $(this).parent().parent().parent().serialize();
        var currentTabContentId = $(this).parent().parent().parent().parent().attr('id');

        $.ajax({
            url: '/quoteorder/',
            type: 'POST',
            data: data,
            headers: {"X-CSRFToken": getCookie("csrftoken")},
            success: function(data) {
                if (data.result == true) {
                    $("#to-quote-orders-grid").jsGrid("loadData");
                    $("#waiting-orders-grid").jsGrid("loadData");
                    updateBookingModal($("#timeslot-id").val(), currentTabContentId);
                    generalNotify('success', 'Updated Successfully', $('#detailModal'));
                } else {
                    generalNotify('danger', 'Operation Failed!', $('#detailModal'));
                }
            }
        });
    };

    var updateBookingModal = function(event_id, tabContentId) {
            $.ajax({
                url: '/timeslotdetail/',
                type: 'POST',
                data: 'id=' + event_id,
                headers: {"X-CSRFToken": getCookie("csrftoken")},
                success: function(data) {
                    console.log(data);
                    if (data.status == "Finished") {
                        $("#btn-finish-booking").prop("disabled", true);
                    } else {
                        $("#btn-finish-booking").prop("disabled", false);
                    }

                    if (data.orders) {
                        $('#detailModal').find('input#member').val(data.member);
                        $('#detailModal').find('input#address').val(data.address);

                        var orderTabList = $("#order-tablist");
                        var orderTabContent = $("#order-tabcontent");

                        orderTabList.html('');
                        orderTabContent.html('');

                        var orders = data.orders;

                        var listStr = '<li><a href="#order_" aria-controls="order_" role="tab" data-toggle="tab" aria-expanded="true">Order#</a></li>';
                        var listHTML = $.parseHTML(listStr);
                        var listObject = $(listHTML);

                        var divStr = 
`<div role="tabpanel" class="tab-pane" id="order_"></div>`;
                        var generalDetailStr = 
`<div class="row">
    <div class="form-group col-xs-4">
        <label for="order_id" class="control-label">Order No:</label>
        <input type="text" class="form-control" id="order_id" disabled>
    </div>
    <div class="form-group col-xs-4">
        <label for="type" class="control-label">Type:</label>
        <input type="text" class="form-control" id="type" disabled>
    </div>
    <div class="form-group col-xs-4">
        <label for="status" class="control-label">Status:</label>
        <input type="text" class="form-control" id="status" disabled>
    </div>
</div>

<div class="row">
    <div class="form-group col-xs-4">
        <label for="product" class="control-label">Product:</label>
        <input type="text" class="form-control" id="product" disabled>
    </div>
    <div class="form-group col-xs-4">
        <label for="boat" class="control-label">Boat:</label>
        <input type="text" class="form-control" id="boat" disabled>
    </div>
    <div class="form-group col-xs-4">
        <label for="calculated-price" class="control-label">Price:</label>
        <input type="number" min="0.0" step="0.01" class="form-control" id="calculated-price" disabled>
    </div>
</div>`;
                        var purchaseDetailStr = 
`<div class="row">
    <div class="form-group col-xs-6">
        <label for="color" class="control-label">Color:</label>
        <div class="input-group">
            <div class="input-group-addon"><img src="" id="color-img"></div>
            <input type="text" class="form-control" id="color" disabled>
        </div>
    </div>
    <div class="form-group col-xs-3">
        <label for="quantity" class="control-label">Quantity:</label>
        <input type="number" step="1" class="form-control" id="quantity" disabled>
    </div>
</div>`;
                        var carpetDetailStr = 
`<div class="row">
    <div class="form-group col-xs-8">
        <label class="control-label">Pattern:</label>
        <div class="input-group">
            <div class="input-group-addon"><img src="" id="pattern-img"></div>
            <input type="text" class="form-control" id="carpet_type" disabled>
            <input type="text" class="form-control" id="pattern" disabled>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-xs-4">
        <label for="waterproof" class="control-label">Waterproof:</label>
        <input type="checkbox" value="" id="waterproof" disabled>
    </div>
    <div class="form-group col-xs-4">
        <label for="underlay" class="control-label">Underlay:</label>
        <input type="checkbox" value="" id="underlay" disabled>
    </div>
    <div class="form-group col-xs-4">
        <label for="edge" class="control-label">Edge:</label>
        <input type="checkbox" value="" id="edge" disabled>
    </div>
</div>`;
                        var repairDetailStr = 
`<div class="row">
    <div class="form-group col-xs-12">
        <label for="image" class="control-label">Image:</label>
        <img src="" id="repair-img">
    </div>
</div>`;
                        var sizeInputStr = 
`<form>
<div class="row submit-button-row">
    <div class="form-group col-xs-6">
        <input name="order_id" type="hidden" class="order-id" value="">
        <label class="control-label">Size:</label>
        <div class="input-group">
            <input name="length" type="number" min="0.0" step="0.01" class="form-control" id="length" placeholder="L">
            <input name="width" type="number" min="0.0" step="0.01" class="form-control" id="width" placeholder="W">
            <input name="height" type="number" min="0.0" step="0.01" class="form-control" id="height" placeholder="H">
        </div>
    </div>

    <div class="form-group col-xs-3 price-input-group">
        <label for="price" class="control-label">Price:</label>
        <input name="price" type="number" min="0.0" step="0.01" class="form-control" id="price" placeholder="Price">
    </div>

    <div class="form-group col-xs-3">
        <label class="control-label button-invisible-label">123123123123123123</label>
        <button type="button" class="btn btn-success btn-submit-quote">Submit</button>
    </div>
</div>
</form>`;

                        var divObject = $($.parseHTML(divStr));
                        var generalDetailObject = $($.parseHTML(generalDetailStr));
                        var purchaseDetailObject = $($.parseHTML(purchaseDetailStr));
                        var carpetDetailObject = $($.parseHTML(carpetDetailStr));
                        var repairDetailObject = $($.parseHTML(repairDetailStr));
                        var sizeInputObject = $($.parseHTML(sizeInputStr));

                        for (var i = 0; i < orders.length; i++) {
                            var order = orders[i];
                            var orderHTMLId = 'order_' + i;

                            var listObjectClone = listObject.clone();

                            if (i == 0)
                                listObjectClone.addClass('active');

                            listObjectClone.find("a")
                            .attr('href', '#' + orderHTMLId)
                            .attr('aria-controls', orderHTMLId)
                            .text('Order#' + order.id);

                            orderTabList.append(listObjectClone);

                            var divObjectClone = divObject.clone();
                            var generalDetailObjectClone = generalDetailObject.clone();

                            if (i == 0)
                                divObjectClone.addClass('active');

                            divObjectClone.attr('id', orderHTMLId);

                            generalDetailObjectClone.find('input#order_id').val(order.id);
                            generalDetailObjectClone.find('input#type').val(order.order_type);
                            generalDetailObjectClone.find('input#status').val(order.status);
                            generalDetailObjectClone.find('input#product').val(order.product);
                            generalDetailObjectClone.find('input#boat').val(order.boat.model);
                            generalDetailObjectClone.find('input#calculated-price').val(parseFloat(order.price).toFixed(2));
                            divObjectClone.append(generalDetailObjectClone);

                            if (order.color) {
                                var purchaseDetailObjectClone = purchaseDetailObject.clone();
                                purchaseDetailObjectClone.find('input#color').val(order.color.code + ' ' + order.color.name);
                                purchaseDetailObjectClone.find('img#color-img').attr('src', order.color.image);
                                purchaseDetailObjectClone.find('input#quantity').val(order.quantity);
                                divObjectClone.append(purchaseDetailObjectClone);
                            }

                            if (order.pattern) {
                                var carpetDetailObjectClone = carpetDetailObject.clone();
                                carpetDetailObjectClone.find('input#carpet_type').val(order.carpet_type);
                                carpetDetailObjectClone.find('input#pattern').val(order.pattern.code + ' ' + order.pattern.name);
                                carpetDetailObjectClone.find('img#pattern-img').attr('src', order.pattern.image);
                                if (order.waterproof == true)
                                    carpetDetailObjectClone.find('input#waterproof').attr('checked', 'checked');
                                if (order.underlay == true)
                                    carpetDetailObjectClone.find('input#underlay').attr('checked', 'checked');
                                if (order.edge == true)
                                    carpetDetailObjectClone.find('input#edge').attr('checked', 'checked');
                                divObjectClone.append(carpetDetailObjectClone);
                            }

                            if (order.image && order.image != '') {
                                var repairDetailObjectClone = repairDetailObject.clone();
                                repairDetailObjectClone.find('img#repair-img').attr('src', order.image);
                                divObjectClone.append(repairDetailObjectClone);
                            }

                            var sizeInputObjectClone = sizeInputObject.clone();
                            if (order.order_type != 'Repair') {
                                sizeInputObjectClone.find('.price-input-group').remove();
                            } else {
                                if (order.price) {
                                    sizeInputObjectClone.find('input#price').val(order.price);
                                }
                            }

                            sizeInputObjectClone.find('input.order-id').val(order.id);
                            
                            if (order.size) {
                                sizeInputObjectClone.find('input#length').val(order.size.length);
                                sizeInputObjectClone.find('input#width').val(order.size.width);
                                sizeInputObjectClone.find('input#height').val(order.size.height);
                            }

                            sizeInputObjectClone.find('button.btn-submit-quote').off().on('click', bookingModalSubmit);
                            
                            divObjectClone.append(sizeInputObjectClone);

                            orderTabContent.append(divObjectClone);
                        }
                        
                        if (tabContentId != null)
                            $('#orders a[href="#' + tabContentId + '"]').tab('show');
                    }
                }
            });
    }

    $('#orders').tab('show');

    $('#calendar').fullCalendar({
        themeSystem: 'bootstrap4',
        header: {
            left: 'month,agendaWeek,list',
            right: 'today,prev,next',
            center: 'title'
        },
        titleFormat: 'MMMM D, YYYY',
        buttonText: {
            today: 'Today',
            month: 'Month',
            week: 'Week',
            day: 'Day',
            list: 'List'
        },
        defaultView: 'agendaWeek',
        allDaySlot: false,
        slotDuration: '02:00:00',
        slotLabelInterval: '02:00:00',
        slotLabelFormat: 'kk:mm A',
        minTime: '12:00:00',
        maxTime: '18:00:00',
        contentHeight: 'auto',
        displayEventTime: true,
        displayEventEnd: true,
        nowIndicator: false,
        events: {
            url: '/schedule',
            cache: true
        },
        loading: function(isLoading, view) {
            if (isLoading) {
                $('#calendar').LoadingOverlay("show");
            } else {
                $("#calendar").LoadingOverlay("hide", true);
            }
        },
        eventClick: function(calEvent, jsEvent, view) {
            $('#detailModal').find('h4').text(calEvent.title);
            $('#detailModal').find('input#date').val(calEvent.start.format("YYYY-MM-DD"));
            $('#detailModal').find('input#timeslot').val(calEvent.start.format("HH:mm") + " - " + calEvent.end.format("HH:mm"));
            $('#detailModal').find('input#timeslot-id').val(calEvent.id);
            $('#detailModal').find('input#timeslot-type').val(calEvent.className);
            if (calEvent.className == 'ot-measurementbooking') {
                $('#detailModal').find('.form-group').not('.fixed-detail').show();
                $('#btn-finish-booking').show();
                updateBookingModal(calEvent.id, null);
            }
            else {
                $('#detailModal').find('.form-group').not('.fixed-detail').hide();
                $('#btn-finish-booking').hide();
            }
            $('#detailModal').modal('show');
        },
        dayClick: function(date, jsEvent, view) {
            if (view.name == 'agendaWeek') {
                $.ajax({
                    url: '/timeslotok/',
                    type: 'POST',
                    data: 'date=' + date.format("YYYY-MM-DD") + '&timeslot=' + date.format("HH:mm:ss"),
                    headers: {"X-CSRFToken": getCookie("csrftoken")},
                    success: function(data) {
                        if (data.result == true) {
                            $('#createModal').find('input#date').val(date.format("YYYY-MM-DD"));
                            $('#createModal').find('input#timeslot').val(date.format("HH:mm"));
                            $('#createModal').modal('show');
                        }
                    }
                });
            } else if (view.name == 'month') {
                $('#calendar').fullCalendar('changeView', 'agendaWeek');
                $('#calendar').fullCalendar('gotoDate', date);    
            }
        }
    })

    $('#btn-create-event').on('click', function() {
        var date = $('#createModal').find('input#date').val();
        var timeslot = $('#createModal').find('input#timeslot').val();
        location.href = "/admin/wingwoo/event/add?date=" + date + "&timeslot=" + timeslot;
    });

    $('#btn-create-booking').on('click', function() {
        var date = $('#createModal').find('input#date').val();
        var timeslot = $('#createModal').find('input#timeslot').val();
        location.href = "/admin/wingwoo/measurementbooking/add?date=" + date + "&timeslot=" + timeslot;
    });

    $('#btn-edit-event').on('click', function() {
        var id = $('#detailModal').find('input#timeslot-id').val();
        var className = $('#detailModal').find('input#timeslot-type').val()
        var type = className.substring(3, className.length);
        location.href = "/admin/wingwoo/" + type + "/" + id + "/change";
    });

    $('#btn-finish-booking').on('click', function() {
        var timeslotId = $('#timeslot-id').val();
        $.ajax({
            url: '/finishedbooking/',
            type: 'POST',
            data: 'timeslot_id=' + timeslotId,
            headers: {"X-CSRFToken": getCookie("csrftoken")},
            success: function(data) {
                if (data.result == true) {
                    $('#btn-finish-booking').prop('disabled', true);
                    $.notify({
                        icon: 'fa fa-exclamation-triangle',
                        title: '',
                        message: 'Updated Successfully'
                    },{
                        type: 'success',
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        },
                        allow_dismiss: true,
                        placement: {
                            from: 'top',
                            align: 'center'
                        },
                        mouse_over: true,
                        element: $('#detailModal')
                    });
                } else {
                    alert("Operation Failed!");
                }
            }
        });
    });
});

/*
                        $('.btn-page').click ( function(){ loadOrder($(this)) } );
                        
                        function loadOrder(elem){
                            var i = $(elem).attr("id");
                            $('#detailModal').find('#order-detail').empty();
                            //console.log(i);
                            $('#detailModal').find('#order-detail').append(
                                //data for each order
                                "Order Number: " + 
                                "\nBoat: " + data.orders[i].boat +
                                "\nOrder Type: " + data.orders[i].order_type[0] +
                                "\nProduct: " + data.orders[i].product 
                                );

                            if (data.orders[i].order_type[0] == "Purchase"){

                                $('#detailModal').find('#order-detail').append (
                                    "\nQuantity: " + data.orders[i].quantity
                                    );

                                if (data.orders[i].product != "Carpet"){
                                    $('#detailModal').find('#order-detail').append (
                                    "\nColor: " + data.orders[i].color.code + " " + data.orders[i].color.name
                                    );

                                } else {
                                    $('#detailModal').find('#order-detail').append (
                                    "\nCarpet Type: " + data.orders[i].carpet_type + 
                                    "\nCarpet Pattern: " + data.orders[i].pattern.code + " " + data.orders[i].pattern.name +
                                    "\nCustomization: Edge - (" + data.orders[i].edge + "); Underlay - (" + data.orders[i].underlay + "); Waterproof - (" + data.orders[i].waterproof + ")"
                                    );
                                }
                                
                            }
                        }

                        $('#detailModal').find('#pagebtn').append(
                                "<button class='btn-page' id='" + i + "'>Order " + (i+1) + "</button>" 
                            );
*/