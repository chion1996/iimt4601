from suit.apps import DjangoSuitConfig
from suit.menu import ParentItem, ChildItem

class SuitConfig(DjangoSuitConfig):
    layout = 'vertical'
    menu = (
        ParentItem('Users', children=[
            ChildItem(model='auth.user'),
            ChildItem('User groups', 'auth.group'),
            ChildItem('Members', 'wingwoo.member'),
            ChildItem('Member Boats', model='wingwoo.boat'),
        ], icon='fa fa-users'),

        ParentItem('Products', children=[
            #ChildItem('Products', model='wingwoo.product'),
            ChildItem('General Products', model='wingwoo.generalproduct'),
            ChildItem('Carpet Products', model='wingwoo.carpetproduct'),
            ChildItem('Product Features', model='wingwoo.productfeature'),
            ChildItem('Colors', model='wingwoo.color'),
            ChildItem('Carpet Pattern', model='wingwoo.carpetpattern'),
        ], icon='fa fa-truck'),

        ParentItem('Orders', children=[
            #ChildItem('Orders', model='wingwoo.order'),
            #ChildItem('Purchse Orders', model='wingwoo.purchaseorder'),
            ChildItem('General Purchse Orders', model='wingwoo.generalpurchaseorder'),
            ChildItem('Carpet Purchse Orders', model='wingwoo.carpetpurchaseorder'),
            ChildItem('Repair Orders', model='wingwoo.repairorder'),
        ], icon='fa fa-shopping-bag'),

        ParentItem('Schedule', children=[
            #ChildItem('Occupied Timeslots', model='wingwoo.occupiedtimeslot'),
            ChildItem('Events', model='wingwoo.event'),
            ChildItem('Measurement Bookings', model='wingwoo.measurementbooking'),
        ], icon='fa fa-calendar'),

        ParentItem('Addresses', children=[
            ChildItem(model='wingwoo.address'),
        ], icon='fa fa-globe'),

        ParentItem('Others', children=[
            ChildItem('Account Receivables', model='wingwoo.accountreceivable'),
            ChildItem('Member Logs', url='/admin/memberlog/'),
        ], icon='fa fa-file-text-o'),

        ParentItem('Notfication', children=[
            ChildItem('NotificationType', model='django_nyt.notificationtype'),
            ChildItem('Settings', model='django_nyt.settings'),
            ChildItem('Subscription', model='django_nyt.subscription'),
            ChildItem('Notification', model='django_nyt.notification'),
        ], icon='fa fa-bell'),

        ParentItem('Messages', children=[
            ChildItem('Messages', url='/admin/contactmessage/'),
        ], icon='fa fa-commenting'),
        
        # ParentItem('Right Side Menu', children=[
        #     ChildItem('Password change', url='admin:password_change'),
        #     ChildItem('Open Google', url='http://google.com', target_blank=True),
        #     ChildItem('Custom view', url='/admin/custom/'),
        # ], align_right=True, icon='fa fa-cog'),
    )

    def ready(self):
        super(SuitConfig, self).ready()