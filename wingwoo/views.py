import datetime

from decimal import *

from django.contrib import admin, messages
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse, Http404
from django.contrib.auth import login, authenticate, update_session_auth_hash
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, PasswordChangeForm
from django.core.paginator import Paginator
from django.core import serializers
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.core.mail import send_mail, BadHeaderError
from django.template import Context
from django.template.loader import get_template
from templated_email import send_templated_mail

from django_nyt.models import NotificationType, Settings, Subscription, Notification
from django_nyt.utils import notify, subscribe

from .forms import *
from .models import *

from .tests import*

# Create your views here.
def index(request):
	product_objects = Product.objects.all()
	products = []
	for product_object in product_objects:
		product = {
			'href': product_object.name.lower(),
			'title': product_object.name,
			'image': product_object.images.all()[0],
			'description': product_object.description
		}
		products.append(product)

	context = {
		'page': 'home',
		'products': products
	}

	if request.method == 'POST':
		form = ContactForm(request.POST)
		context['form'] = form
		if form.is_valid():
			name = form.cleaned_data.get('name')
			email = form.cleaned_data.get('email')
			content = form.cleaned_data.get('content')
			#send_mail(
			# 	subject = "New contact form submission from " + name,
			# 	message = content,
			# 	from_email='wingwoosailmaker@gmail.com',
			#	recipient_list = [email],
			# 	fail_silently = False,
			# 	html_message = content
			#)
			# send_templated_mail(
			# 	template_name='contact',
			# 	from_email='wingwoosailmaker@gmail.com',
			# 	recipient_list=['wingwoosailmaker@gmail.com'],
			# 	context= {
			# 		'name':name,
			# 		'email':email,
			# 		'content':content
			# 	},
			# )

			ContactMessage.create(
				name = name,
				email = email,
				content = content
			)

			return redirect('index')
		else:
			return render(request, 'wingwoo/index.html', context)
	else:
		form = ContactForm()
		context['form'] = form	

	return render(request, 'wingwoo/index.html', context)

# Registration
def process_login(request):
	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(request, username=username, password=password)

		if user is not None:
			try:
				member = Member.objects.get(user = user)
			except ObjectDoesNotExist:
				return JsonResponse({'result': False}, safe=False)

			login(request, user)
			MemberLog.create(member, 'Logged in')
			return JsonResponse({'result': True}, safe=False)
		else:
			return JsonResponse({'result': False}, safe=False)

def process_register(request):
	if request.method == 'POST':
		form = RegisterForm(request.POST)

		if form.is_valid():
			user = form.save()
			user.refresh_from_db()
			user.save()
			member = Member.objects.create(user=user)
			member.phone = form.cleaned_data.get('phone')
			member.name = form.cleaned_data.get('last_name') + " " + form.cleaned_data.get('first_name')
			member.save()
			MemberLog.create(member, 'Registered')

			noti_setting = Settings.objects.create(user=user, is_default=True)
			subscribe(noti_setting, user.username+"_normal", object_id=user.id)

			username = form.cleaned_data.get('username')
			raw_password = form.cleaned_data.get('password1')
			user = authenticate(username=username, password=raw_password)

			login(request, user)
			MemberLog.create(member, 'Logged in')
			return JsonResponse({'result': True}, safe=False)
		else:
			return render(request, 'forms/form_base.html', {'form': form})

#Navigation

def about(request):
	return render(request, 'wingwoo/about.html')

def products(request):
	product_objects = Product.objects.all()

	products = []
	for product_object in product_objects:
		product = {
			'href': product_object.name.lower(),
			'title': product_object.name,
			'image': product_object.images.all()[0],
			'description': product_object.description
		}
		products.append(product)

	context = {
		'page': 'products',
		'products': products 
	}
	return render(request, 'wingwoo/products.html', context)

def product_detail(request, category):
	try:
		print(category.title())
		product = Product.objects.get(name = category.title())
	except ObjectDoesNotExist:
		raise Http404("Product does not exist")

	context = {
		'page': 'products',
		'price': product.purchase_unit_price,
		'category': product.name,
		'images': product.images.all(),
		'features': product.feature.all(),
		'repairurl': '/repairorder/' + product.name.lower() + '/'
	}

	if category.title() == 'Carpet':
		context['purchaseurl'] = '/carpetpurchaseorder/'
		context['product'] = CarpetProduct.objects.get(id = product.id)
	else:
		context['purchaseurl'] = '/generalpurchaseorder/' + product.name.lower() + '/'
		context['product'] = GeneralProduct.objects.get(id = product.id)

	if request.user.is_authenticated:
		try:
			member_id = request.user.member.id
			context['is_authenticated'] = True
		except ObjectDoesNotExist:
			member_id = None
			context['is_authenticated'] = False
	else:
		member_id = None
		context['is_authenticated'] = False

	context['gpform'] = GeneralPurchaseOrderForm(member_id, None)
	context['rpform'] = RepairOrderForm(member_id, None)
	context['cpform'] = CarpetPurchaseOrderForm(member_id, None)
	
	return render(request, 'wingwoo/product_detail.html', context)
	
def contact(request):
	if request.method == 'POST':
		form = ContactForm(request.POST)

		if form.is_valid():
			name = form.cleaned_data.get('name')
			email = form.cleaned_data.get('email')
			content = form.cleaned_data.get('content')

			send_mail(
				"New contact form submission",
				content,
				email,
				['wingwoosailmaker@gmail.com'],
				fail_silently=False,
			)

			return redirect('contact')
		else:
			return render(request, 'wingwoo/contact.html', {'form': form})

	else:
		form = ContactForm()
	return render(request, 'wingwoo/contact.html', {'form': form})	

#Forms
def generalPurchaseOrderForm(request, category):
	if request.user.is_authenticated:
		member = Member.objects.get(user = request.user)
		if request.method == 'POST':
			if int(request.POST.get('member')) != member.id:
				raise Http404("Mismatch ids")

			form = GeneralPurchaseOrderForm(member.id, request.POST)
			if form.is_valid():
				product =  Product.objects.get(name = category.title())
				boat = form.cleaned_data.get('boat')
				length = Decimal(0.0) if form.cleaned_data.get('length') is None else Decimal(form.cleaned_data.get('length'))
				width = Decimal(0.0) if form.cleaned_data.get('width') is None else Decimal(form.cleaned_data.get('width'))
				height = Decimal(0.0) if form.cleaned_data.get('height') is None else Decimal(form.cleaned_data.get('height'))
				known_size = (not form.cleaned_data.get('known_size'))
				quantity = form.cleaned_data.get('quantity')
				color = form.cleaned_data.get('color')
				GeneralPurchaseOrder.create(member,product,boat,length,width,height,known_size,quantity,color)
				return redirect('orders')
			else:
				return render(request, 'forms/GeneralPurchaseOrder.html', {'form': form})
		else:
			form = GeneralPurchaseOrderForm(member.id, None)
		return render(request, 'forms/GeneralPurchaseOrder.html',{'form': form})
	else:
		raise Http404("Login Required")
	

def carpetPurchaseOrderForm(request):
	if request.user.is_authenticated:
		member = Member.objects.get(user = request.user)
		if request.method == 'POST':
			if int(request.POST.get('member')) != member.id:
				raise Http404("Mismatch ids")
			
			print(request.POST)
			form = CarpetPurchaseOrderForm(member.id, request.POST)
			if form.is_valid():
				product =  Product.objects.get(name = 'Carpet')
				boat = form.cleaned_data.get('boat')
				length = Decimal(0.0) if form.cleaned_data.get('length') is None else Decimal(form.cleaned_data.get('length'))
				width = Decimal(0.0) if form.cleaned_data.get('width') is None else Decimal(form.cleaned_data.get('width'))
				height = Decimal(0.0) if form.cleaned_data.get('height') is None else Decimal(form.cleaned_data.get('height'))
				quantity = form.cleaned_data.get('quantity')
				pattern = form.cleaned_data.get('pattern')
				known_size = (not form.cleaned_data.get('known_size'))
				waterproof = form.cleaned_data.get('waterproof')
				underlay = form.cleaned_data.get('underlay')
				edge = form.cleaned_data.get('edge')
				CarpetPurchaseOrder.create(member, product, boat, length, width, height, quantity, pattern, known_size, waterproof, underlay, edge)
				return redirect('orders')
			else:
				return render(request, 'forms/CarpetPurchaseOrder.html', {'form': form})
		else:
			form = CarpetPurchaseOrderForm(member.id,None)

		return render(request, 'forms/CarpetPurchaseOrder.html',{'form': form})
	else:
		raise Http404("Login Required")
	

def repairOrderForm(request,category):
	if request.user.is_authenticated:
		member = Member.objects.get(user = request.user)
		if request.method == 'POST':
			if int(request.POST.get('member')) != member.id:
				raise Http404("Mismatch ids")
			
			print(request.POST)

			form = RepairOrderForm(member.id, request.POST)
			if form.is_valid():
				product = Product.objects.get(name = category.title())
				boat = form.cleaned_data.get('boat')
				length = Decimal(0.0) if form.cleaned_data.get('length') is None else Decimal(form.cleaned_data.get('length'))
				width = Decimal(0.0) if form.cleaned_data.get('width') is None else Decimal(form.cleaned_data.get('width'))
				height = Decimal(0.0) if form.cleaned_data.get('height') is None else Decimal(form.cleaned_data.get('height'))
				if form.cleaned_data.get('length') is None or form.cleaned_data.get('width') is None or form.cleaned_data.get('height') is None:
					known_size = False
				else:
					known_size = (not form.cleaned_data.get('known_size'))
				#image = form.cleaned_data.get('image')
				image = request.FILES.get('image', None)
				RepairOrder.create(member,product,boat,length,width,height,known_size,image)
				return redirect('orders')
			else:
				return render(request, 'forms/RepairOrder.html', {'form': form})
		else:
			form = RepairOrderForm(member.id,None)
		
		return render(request, 'forms/RepairOrder.html',{'form': form})
	else:
		raise Http404("Login Required")

def boat(request):
	if request.method == 'POST':
		form = BoatForm(request.POST)
		if form.is_valid():
			address = form.cleaned_data.get('address')
			model = form.cleaned_data.get('model')
			member = Member.objects.get(user = request.user)
			boat = Boat.create(member,address,model)
			return JsonResponse({'result': True, 'boat_id': boat.id, 'boat_label': str(boat)}, safe=False)
		else:
			return render(request, 'forms/form_base.html', {'form': form})

def remove_boat(request):
	if request.method == 'POST':
		print(request.POST.get('boat',''))
		boatid = request.POST.get('boat','')
		boat = Boat.objects.get(id=boatid)
		try:
			Boat.remove(boat)
		except Exception as e:
			return JsonResponse({'result': False, 'message': str(e)}, safe=False)
			
		return JsonResponse({'result': True, 'boat_id': boatid}, safe=False)

def edit_boat(request):
	if request.method == 'POST':
		print(request.POST.get('boatid',''))
		boatid = request.POST.get('boatid','')
		address = request.POST.get('address','') 
		boataddress = Address.objects.get(id=address)
		model = request.POST.get('model','')
		boat = Boat.objects.get(id=boatid)
		try:
			Boat.edit(boat,boataddress,model)
		except Exception as e:
			return JsonResponse({'result': False, 'message': str(e)}, safe=False)
			
		return JsonResponse({'result': True, 'boat_id': boat.id, 'boat_label': str(boat)}, safe=False)

def get_boat(request):
	if request.method == 'POST':
		boatid = request.POST.get('boat','')
		boat = Boat.objects.get(id=boatid)
		return JsonResponse({'result': True,  'boat_id':boatid, 'boat_address': boat.address.id, 'boat_label': boat.model}, safe=False)

def get_form(request):
	if request.method == 'POST':
		form_name = request.POST['form_name']
		if form_name == 'register':
			form = RegisterForm()
		elif form_name == 'boat':
			form = BoatForm()
		elif form_name == 'editBoat':
			form = EditBoatForm(None)
		else:
			form = AuthenticationForm()

		return render(request, 'forms/form_base.html', {'form': form})

def is_datetime_available(request):
	if request.method == 'POST':
		date_string = request.POST.get('date', '')
		timeslot_string = request.POST.get('timeslot', '')

		if (date_string != '') and (timeslot_string != ''):
			date = datetime.datetime.strptime(date_string, '%Y-%m-%d')
			timeslot_choice = OccupiedTimeslot.get_timeslot_choice(timeslot_string)

			if OccupiedTimeslot.is_datetime_available(date, timeslot_choice)['result'] == True:
				return JsonResponse({ "result": True })
			else:
				return JsonResponse({ "result": False })

def schedule(request):
	if request.method == 'GET':
		if request.user.is_authenticated:
			district = request.GET.get('district', '')
			start_date = request.GET.get('start', '')
			end_date = request.GET.get('end', '')

			if start_date != '' and end_date != '':
				district = int(district) if (district != '') else None
				start_date = datetime.datetime.strptime(start_date[0:10], '%Y-%m-%d')
				end_date = datetime.datetime.strptime(end_date[0:10], '%Y-%m-%d')

				events = OccupiedTimeslot.get_timeslots_as_event_data(district, start_date, end_date, request.user)

				return JsonResponse(events, safe=False)

def get_timeslot_detail(request):
	if request.method == 'POST':
		if request.user.is_authenticated:
			timeslot_id_string = request.POST.get('id', '')

			if timeslot_id_string != '':
				timeslot_id = int(timeslot_id_string)
				detail = OccupiedTimeslot.get_detail_by_id(timeslot_id)

				return JsonResponse(detail, safe=False)

def get_order_options(request):
	if request.method == 'POST':
		member_id_string = request.POST.get('member_id', '')
		measurement_booking_id_string = request.POST.get('mb_id', '')

		data = []

		if measurement_booking_id_string != '':
			measurement_booking_id = int(measurement_booking_id_string)
			selected_orders = Order.objects.filter(Q(measurement_booking__id = measurement_booking_id))
			for order in selected_orders:
				data.append({'label': str(order), 'value': str(order.id), 'selected': True})

		if member_id_string == '':
			orders = Order.objects.filter(Q(measurement_booking = None) & Q(status = ORDER_STATUS_BOOK_MEASUREMENT))
		else:
			member_id = int(member_id_string)
			orders = Order.objects.filter(Q(measurement_booking = None) & Q(status = ORDER_STATUS_BOOK_MEASUREMENT) & Q(member__id = member_id))
		

		for order in orders:
			data.append({'label': str(order), 'value': str(order.id)})

		return JsonResponse(data, safe=False)

def orders(request):
	if request.user.is_authenticated:
		member = Member.objects.get(user = request.user)
		# orders = Order.objects.get(member=member)
		# orders = orders.get_detail()
		# paginator = Paginator(orders, 10)
		# data = serializers.serialize('json', list(paginator.page(1)), fields=('created_at','order_type','product','boat','status','locked','measurement_booking'))
		# context = {
		# 	'data': data
		# }
		context = {
			'page': 'orders',
			'color_queryset': Color.objects.all(),
			'pattern_queryset': CarpetPattern.objects.all()
		}
		return render(request, 'wingwoo/vieworders.html', context)
	else:
		return JsonResponse({'message': 'unauthenticated'})

def vieworders(request, is_paid, is_purchase):
	if request.method == 'POST' and request.user.is_authenticated:
		try:
			member = Member.objects.get(user = request.user)
		except ObjectDoesNotExist:
			return JsonResponse({'result': False}, safe=False)

		data = []

		queries = [Q(member = member)]

		if (is_paid >= 0 and is_paid <= 1) and (is_purchase >= 0 and is_purchase <= 1):
			order_type_choice = ORDER_TYPE_PURCHASE if (is_purchase == 1) else ORDER_TYPE_REPAIR
			queries.append(Q(order_type = order_type_choice))

			if is_paid == 1:
				queries.append(Q(status__gte = ORDER_STATUS_MANUFACTURING))
			else:
				queries.append(Q(status__lte = ORDER_STATUS_WAIT_FOR_PAYMENT))

		filter_id = request.POST.get('id', '')
		filter_product = request.POST.get('product', '')
		filter_boat = request.POST.get('boat[model]', '')
		filter_status = request.POST.get('status', '')
		filter_length_from = request.POST.get('size[length][from]', '')
		filter_length_to = request.POST.get('size[length][to]', '')
		filter_width_from = request.POST.get('size[width][from]', '')
		filter_width_to = request.POST.get('size[width][to]', '')
		filter_height_from = request.POST.get('size[height][from]', '')
		filter_height_to = request.POST.get('size[height][to]', '')
		filter_quantity_from = request.POST.get('extra[quantity][from]', '')
		filter_quantity_to = request.POST.get('extra[quantity][to]', '')
		filter_colorpattern = request.POST.get('extra[colorpattern]', '')
		filter_waterproof = request.POST.get('extra[customization][waterproof]', '')
		filter_underlay = request.POST.get('extra[customization][underlay]', '')
		filter_edge = request.POST.get('extra[customization][edge]', '')
		filter_price_from = request.POST.get('price[from]', '')
		filter_price_to = request.POST.get('price[to]', '')

		if filter_id != '': queries.append(Q(id = int(filter_id)))
		if filter_product != '': queries.append(Q(product__name = filter_product))
		if filter_boat != '': queries.append(Q(boat__model__contains = filter_boat))
		if filter_status != '': queries.append(Q(status = filter_status))
		if filter_length_from != '': queries.append(Q(size__length__gte = Decimal(filter_length_from)))
		if filter_length_to != '': queries.append(Q(size__length__lte = Decimal(filter_length_to)))
		if filter_width_from != '': queries.append(Q(size__width__gte = Decimal(filter_width_from)))
		if filter_width_to != '': queries.append(Q(size__width__lte = Decimal(filter_width_to)))
		if filter_height_from != '': queries.append(Q(size__height__gte = Decimal(filter_height_from)))
		if filter_height_to != '': queries.append(Q(size__height__lte = Decimal(filter_height_to)))
		if filter_quantity_from != '': queries.append(Q(purchaseorder__quantity__gte = int(filter_quantity_from)))
		if filter_quantity_to != '': queries.append(Q(purchaseorder__quantity__lte = int(filter_quantity_to)))
		if filter_colorpattern != '':
			queries.append(
				Q(
					Q(purchaseorder__purchase_order_type = PURCHASE_ORDER_TYPE_GENERAL) &
					Q(purchaseorder__generalpurchaseorder__color__name__contains = filter_colorpattern)
				) | 
				Q(
					Q(purchaseorder__purchase_order_type = PURCHASE_ORDER_TYPE_CARPET) &
					Q(purchaseorder__carpetpurchaseorder__pattern__name__contains = filter_colorpattern)
				)
			)
		if filter_waterproof != '' and filter_waterproof == 'true':
			queries.append(
				Q(
					Q(purchaseorder__purchase_order_type = PURCHASE_ORDER_TYPE_CARPET) &
					Q(purchaseorder__carpetpurchaseorder__waterproof = True)
				)
			)

		if filter_underlay != '' and filter_underlay == 'true':
			queries.append(
				Q(
					Q(purchaseorder__purchase_order_type = PURCHASE_ORDER_TYPE_CARPET) &
					Q(purchaseorder__carpetpurchaseorder__underlay = True)
				)
			)

		if filter_edge != '' and filter_edge == 'true':
			queries.append(
				Q(
					Q(purchaseorder__purchase_order_type = PURCHASE_ORDER_TYPE_CARPET) &
					Q(purchaseorder__carpetpurchaseorder__edge = True)
				)
			)

		extra_filtering = False
		if filter_price_from != '' or filter_price_to != '':
			extra_filtering = True
			
		query = queries.pop()
		for item in queries:
			query &= item

		orders = Order.objects.filter(query)

		orders = orders.order_by("id")

		sort_field = request.POST.get('sortField', '')
		sort_order = request.POST.get('sortOrder', '')

		extra_sorting = False
		if (sort_field != '' and sort_order != ''):
			valid_field = True

			if sort_field == 'boat.model':
				sort_field = 'boat__model'
			elif sort_field == 'product':
				sort_field = 'product__name'
			elif sort_field in ["size.length", "size.width", "size.height"]:
				sort_field = sort_field.replace('.', '__')
			elif sort_field == 'extra.quantity':
				sort_field = 'purchaseorder__quantity'
			else:
				if sort_field not in ["id", "status", "extra.quantity"]:
					if sort_field in ["extra.colorpattern", "price"]:
						extra_sorting = True
					else:
						valid_field = False			

			if valid_field:
				if sort_order == 'desc' and not extra_sorting:
					sort_field = '-' + sort_field

				if not extra_sorting:
					orders = orders.order_by(sort_field)

		total_count = orders.count()

		page_index = int(request.POST.get('pageIndex', 1))
		page_size = int(request.POST.get('pageSize', 10))

		if (not extra_sorting) and (not extra_filtering):
			paginator = Paginator(orders, page_size)

			if page_index > paginator.num_pages:
				page_index = paginator.num_pages

			orders = paginator.page(page_index)
		
		for order in orders:
			detail = order.get_detail(False)

			if filter_price_from != '':
				if detail['price'] < Decimal(filter_price_from):
					total_count = total_count - 1	
					continue

			if filter_price_to != '':
				if detail['price'] > Decimal(filter_price_to):
					total_count = total_count - 1
					continue

			extra = detail['extra']
			colorpattern = {}
			customization = {}
			
			if 'color' in extra: colorpattern = extra.pop('color')
			if 'pattern' in extra: colorpattern = extra.pop('pattern')

			if 'waterproof' in extra: customization['waterproof'] = extra.pop('waterproof')
			if 'underlay' in extra: customization['underlay'] = extra.pop('underlay')
			if 'edge' in extra: customization['edge'] = extra.pop('edge')

			extra['colorpattern'] = colorpattern
			extra['customization'] = customization
			detail['extra'] = extra

			data.append(detail)

		if (extra_sorting or extra_filtering) and len(data) > 0:
			if extra_sorting:
				if sort_field == 'price':
					#key = lambda x: (float(x[sort_field]), len(x[sort_field]))
					key = lambda x: x[sort_field]
				elif sort_field == 'extra.colorpattern':
					key = lambda x: x['extra']['colorpattern']['name']
				else:
					key = lambda x: x[sort_field]

				if sort_order == 'desc':
					data = sorted(data, key=key, reverse=True)
				else:
					data = sorted(data, key=key, reverse=False)

			i = range(0, len(data), page_size)[page_index - 1]
			data = data[i:i+page_size]

		#paginator = Paginator(data, page_size)
		#data = paginator.page(page_index)

		context = {
			'message' : 'ok',
			'data': data,
			'total_count': total_count
		}

		return JsonResponse(context, safe=False)

	return JsonResponse({'message': 'unauthenticated or unexpected format'})

def admin_orders(request, status):
	if request.method == 'POST' and request.user.is_authenticated and request.user.is_staff:
		data = []

		filter_id = request.POST.get('id', '')
		filter_order_type = request.POST.get('order_type', '')
		filter_member = request.POST.get('member', '')
		filter_product = request.POST.get('product', '')
		filter_boat = request.POST.get('boat', '')

		queries = [Q(status = status)]
		if filter_id != '': queries.append(Q(id = int(filter_id)))
		if filter_order_type != '': queries.append(Q(order_type = int(filter_order_type)))
		if filter_member != '': queries.append(Q(member__name__contains = filter_member))
		if filter_product != '': queries.append(Q(product__name = filter_product))

		extra_filtering = False
		if filter_boat != '':
			extra_filtering = True

		query = queries.pop()
		for item in queries:
			query &= item

		orders = Order.objects.filter(query).order_by("id")

		sort_field = request.POST.get('sortField', '')
		sort_order = request.POST.get('sortOrder', '')

		extra_sorting = False
		if (sort_field != '' and sort_order != ''):
			valid_field = True

			if sort_field == 'product':
				sort_field = 'product__name'
			else:
				if sort_field not in ["id", "order_type"]:
					if sort_field in ["member"]:
						extra_sorting = True
					else:
						valid_field = False

			if valid_field:
				if sort_order == 'desc' and not extra_sorting:
					sort_field = '-' + sort_field

				if not extra_sorting:
					orders = orders.order_by(sort_field)

		total_count = orders.count()

		page_index = int(request.POST.get('pageIndex', 1))
		page_size = int(request.POST.get('pageSize', 10))

		if (not extra_sorting) and (not extra_filtering):
			paginator = Paginator(orders, page_size)

			if page_index > paginator.num_pages:
				page_index = paginator.num_pages

			orders = paginator.page(page_index)

		for order in orders:
			if filter_boat != '':
				formatted_boat = order.boat.address.get_str() + ' ' + order.boat.model
				if formatted_boat.find(filter_boat) == -1:
					total_count = total_count - 1
					continue
			
			detail = order.get_detail(True)
			data.append(detail)

		if (extra_sorting or extra_filtering) and len(data) > 0:
			if extra_sorting:
				if sort_field == 'member':
					key = lambda x: x['member'].upper()

				if sort_order == 'desc':
					data = sorted(data, key=key, reverse=True)
				else:
					data = sorted(data, key=key, reverse=False)

			i = range(0, len(data), page_size)[page_index - 1]
			data = data[i:i+page_size]

		context = {
			'message' : 'ok',
			'data': data,
			'total_count': total_count
		}

		return JsonResponse(context, safe=False)

	return JsonResponse({'result': False}, safe=False)

def view_bookings(request):
	if request.method == 'POST' and request.user.is_authenticated:
		try:
			member = Member.objects.get(user = request.user)
		except ObjectDoesNotExist:
			return JsonResponse({'result': False}, safe=False)

		data = []

		queries = [Q(member = member)]

		filter_id = request.POST.get('id', '')
		filter_date = request.POST.get('date', '')
		filter_timeslot = request.POST.get('timeslot', '')
		filter_address = request.POST.get('address', '')
		filter_status = request.POST.get('status', '')

		if filter_id != '': queries.append(Q(id = int(filter_id)))
		if filter_date != '': queries.append(Q(date = datetime.datetime.strptime(filter_date, '%Y-%m-%d')))
		if filter_timeslot != '': queries.append(Q(timeslot = int(filter_timeslot)))
		if filter_status != '': queries.append(Q(status = int(filter_status)))

		extra_filtering = False
		if filter_address != '':
			extra_filtering = True

		query = queries.pop()
		for item in queries:
			query &= item

		bookings = MeasurementBooking.objects.filter(query).order_by('-date', 'timeslot', 'id');

		sort_field = request.POST.get('sortField', '')
		sort_order = request.POST.get('sortOrder', '')

		extra_sorting = False
		if (sort_field != '' and sort_order != ''):
			if sort_field == 'address':
				extra_sorting = True
			else:
				if sort_field in ["id", "date", "timeslot", "status"]:		
					if sort_order == 'desc':
						sort_field = '-' + sort_field
					
					bookings = bookings.order_by(sort_field)


		total_count = bookings.count()

		page_index = int(request.POST.get('pageIndex', 1))
		page_size = int(request.POST.get('pageSize', 10))

		if (not extra_sorting) and (not extra_filtering):
			paginator = Paginator(bookings, page_size)

			if page_index > paginator.num_pages:
				page_index = paginator.num_pages

			bookings = paginator.page(page_index)


		for booking in bookings:
			detail = booking.get_detail(False)

			if filter_address != '':
				if booking.get_address().get_str().find(filter_address) == -1:
					total_count = total_count - 1	
					continue

			data.append(detail)

		if (extra_sorting or extra_filtering) and len(data) > 0:
			if extra_sorting:
				if sort_field == 'address':
					#key = lambda x: (float(x[sort_field]), len(x[sort_field]))
					key_district = lambda x: x[sort_field]['district']
					key_pier = lambda x: x[sort_field]['pier']

				if sort_order == 'desc':
					data = sorted(data, key=key_district, reverse=True)
					data = sorted(data, key=key_pier, reverse=True)
				else:
					data = sorted(data, key=key_district, reverse=False)
					data = sorted(data, key=key_pier, reverse=False)

			i = range(0, len(data), page_size)[page_index - 1]
			data = data[i:i+page_size]

		context = {
			'message': 'ok',
			'data': data,
			'total_count': total_count
		}

		return JsonResponse(context, safe=False)

	return JsonResponse({'message': 'unauthenticated or unexpected format'})

def quote_order(request):
	if request.method == 'POST':
		if request.user.is_authenticated and request.user.is_staff:
			order_id_string = request.POST.get('order_id', '')
			length_string = request.POST.get('length', '')
			width_string = request.POST.get('width', '')
			height_string = request.POST.get('height', '')
			price_string = request.POST.get('price', '')

			if order_id_string != '' and length_string != '' and width_string != '' and height_string != '':
				order_id = int(order_id_string)
				length = Decimal(length_string)
				width = Decimal(width_string)
				height = Decimal(height_string)

				try:
					order = Order.objects.get(id = order_id)
				except ObjectDoesNotExist:
					return JsonResponse({'result': False, 'message': 'Order does not exist'}, safe=False)

				if order.order_type == ORDER_TYPE_REPAIR:
					if price_string == '':
						return JsonResponse({'result': False, 'message': 'Please input all the data'}, safe=False)
					else:
						price = Decimal(price_string)
				else:
					price = Decimal('0.0')
				
				order.quote(length, width, height, price, request.user)

				return JsonResponse({'result': True}, safe=False)
			else:
				return JsonResponse({'result': False, 'message': 'Please input all the data'}, safe=False)
	
	return JsonResponse({'result': False}, safe=False)

def made_order(request):
	if request.method == 'POST':
		if request.user.is_authenticated and request.user.is_staff:
			order_id_string = request.POST.get('order_id', '')

			if order_id_string != '':
				order_id = int(order_id_string)

				try:
					order = Order.objects.get(id = order_id)
					order.made(request.user)
				except ObjectDoesNotExist:
					return JsonResponse({'result': False}, safe=False)

				return JsonResponse({'result': True}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def received_payment(request):
	if request.method == 'POST':
		if request.user.is_authenticated and request.user.is_staff:
			order_id_string = request.POST.get('order_id', '')

			if order_id_string != '':
				order_id = int(order_id_string)

				try:
					order = Order.objects.get(id = order_id)
					order.received_payment(request.user)
				except ObjectDoesNotExist:
					return JsonResponse({'result': False}, safe=False)

				return JsonResponse({'result': True}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def delivered_order(request):
	if request.method == 'POST':
		if request.user.is_authenticated and request.user.is_staff:
			order_id_string = request.POST.get('order_id', '')

			if order_id_string != '':
				order_id = int(order_id_string)

				try:
					order = Order.objects.get(id = order_id)
					order.delivered(request.user)
				except ObjectDoesNotExist:
					return JsonResponse({'result': False}, safe=False)

				return JsonResponse({'result': True}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def reverse_order(request):
	if request.method == 'POST':
		if request.user.is_authenticated and request.user.is_staff:
			order_id = request.POST.get('order_id', '')

			if order_id != '':
				order_id = int(order_id)

				try:
					order = Order.objects.get(id = order_id)
					order.reverse(request.user)
				except ObjectDoesNotExist:
					return JsonResponse({'result': False}, safe=False)

				return JsonResponse({'result': True}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def finished_booking(request):
	if request.method == 'POST':
		if request.user.is_authenticated and request.user.is_staff:
			timeslot_id_string = request.POST.get('timeslot_id', '')

			if timeslot_id_string != '':
				timeslot_id = int(timeslot_id_string)

				try:
					booking = MeasurementBooking.objects.get(id = timeslot_id)
					booking.finish(request.user)
				except ObjectDoesNotExist:
					return JsonResponse({'result': False}, safe=False)

				return JsonResponse({'result': True}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def get_price_detail(request):
	if request.method == 'POST':
		data = {}
		data['product_id'] = request.POST.get('product_id', '')
		data['quantity'] = request.POST.get('quantity', '')
		data['length'] = request.POST.get('length', '')
		data['width'] = request.POST.get('width', '')
		data['height'] = request.POST.get('height', '')
		data['pattern'] = request.POST.get('pattern', '')
		data['waterproof'] = request.POST.get('waterproof', '')
		data['underlay'] = request.POST.get('underlay', '')
		data['edge'] = request.POST.get('edge', '')

		formatted_data = {};
		for key, value in data.items():
			if value == '':
				if (key == 'waterproof') or (key == 'underlay') or (key == 'edge'):
					data[key] = False
				else:
					data[key] = None
			else:
				if (key == 'product_id') or (key == 'quantity') or (key == 'pattern'):
					data[key] = int(value)
				elif (key == 'length') or (key == 'width') or (key == 'height'):
					data[key] = Decimal(value)
				elif (key == 'waterproof') or (key == 'underlay') or (key == 'edge'):
					data[key] = True

		if data['product_id'] != None:
			try:
				product = Product.objects.get(id = data['product_id'])
			except ObjectDoesNotExist:
				return JsonResponse({'result': False}, safe=False)
			
			if data['length'] != None and data['width'] != None and data['height'] != None:
				if product.product_type == PRODUCT_TYPE_GENERAL:
					detail = product.get_price_detail(data['length'], data['width'], data['height'], data['pattern'], data['waterproof'], data['underlay'], data['edge'], data['quantity'])
					return JsonResponse({'result': True, 'detail': detail}, safe=False)
				else:
					if data['pattern'] != None and data['waterproof'] != None and data['underlay'] != None and data['edge'] != None:
						try:
							data['pattern'] = CarpetPattern.objects.get(id = data['pattern'])
						except ObjectDoesNotExist:
							return JsonResponse({'result': False}, safe=False)

						detail = product.get_price_detail(data['length'], data['width'], data['height'], data['pattern'], data['waterproof'], data['underlay'], data['edge'], data['quantity'])
						return JsonResponse({'result': True, 'detail': detail}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def create_booking(request):
	if request.method == 'POST':
		if request.user.is_authenticated:
			order_id_string = request.POST.get('order_id', '')
			date_string = request.POST.get('date', '')
			time_string = request.POST.get('timeslot', '')

			if order_id_string != '' and date_string != '' and time_string != '':
				order_id = int(order_id_string)

				try:
					order = Order.objects.get(id = order_id)
				except ObjectDoesNotExist:
					return JsonResponse({'result': False}, safe=False)

				date = datetime.datetime.strptime(date_string, '%Y-%m-%d')
				timeslot = OccupiedTimeslot.get_timeslot_choice(time_string)

				try:
					member = request.user.member
				except ObjectDoesNotExist:
					return JsonResponse({'result': False}, safe=False)

				if timeslot != '' and order.member == member:
					try:
						MeasurementBooking.create(member, order, date, timeslot)
					except Exception as e:
						return JsonResponse({'result': False, 'message': str(e)}, safe=False)

					return JsonResponse({'result': True}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def remove_upload(request):
	data = {
		'initialPreview': [],
		'initialPreviewConfig': [],
		'append': False
	}
	return JsonResponse(data, safe=False)

def edit_repair_order(request):
	if request.method == 'POST':
		if request.user.is_authenticated:
			print(request.POST)
			print(request.FILES.get('image', ''))

			form = RepairOrderEditForm(request.POST, request.FILES)

			if form.is_valid():
				order_id = request.POST.get('id', '')
				length = request.POST.get('length', '0.00')
				width = request.POST.get('width', '0.00')
				height = request.POST.get('height', '0.00')
				image = request.FILES.get('image', None)
				image_changed = request.POST.get('image_changed', '');

				if order_id != '' and length != '' and width != '' and height != '' and image_changed != '':
					order_id = int(order_id)
					length = Decimal(length)
					width = Decimal(width)
					height = Decimal(height)
					image_changed = True if (image_changed == '1') else False

					modified_by = request.user

					try:
						repair_order = RepairOrder.objects.get(id = order_id)
					except ObjectDoesNotExist:
						return JsonResponse({'result': False}, safe=False)

					if repair_order.member.user == modified_by:
						repair_order.edit(length, width, height, modified_by, image, image_changed)

						response = {
							'result': True
						}
						response['data'] = repair_order.order_ptr.get_detail(False)
						return JsonResponse(response, safe=False)
					else:
						return JsonResponse({'result': False}, safe=False)
			else:
				return JsonResponse({'result': False, 'message': 'Invalid input'}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def edit_general_purchase_order(request):
	if request.method == 'POST':
		if request.user.is_authenticated:
			print(request.POST)

			form = GeneralPurchaseOrderEditForm(request.POST)

			if form.is_valid():
				order_id = request.POST.get('id', '')
				length = request.POST.get('length', '0.00')
				width = request.POST.get('width', '0.00')
				height = request.POST.get('height', '0.00')
				quantity = request.POST.get('quantity', '')
				color_id = request.POST.get('color', '')

				if order_id != '' and quantity != '' and color_id != '':
					order_id = int(order_id)
					length = Decimal(length)
					width = Decimal(width)
					height = Decimal(height)
					quantity = int(quantity)
					color_id = int(color_id)

					modified_by = request.user

					try:
						general_purchase_order = GeneralPurchaseOrder.objects.get(id = order_id)
						color = Color.objects.get(id = color_id)
					except ObjectDoesNotExist:
						return JsonResponse({'result': False}, safe=False)

					if general_purchase_order.member.user == modified_by:
						general_purchase_order.edit(length, width, height, modified_by, quantity, color)
						response = {
							'result': True
						}
						response['data'] = general_purchase_order.order_ptr.get_detail(False)
						return JsonResponse(response, safe=False)
					else:
						return JsonResponse({'result': False}, safe=False)
			else:
				return JsonResponse({'result': False, 'message': 'Invalid input'}, safe=False)

			return JsonResponse({'result': True}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def edit_carpet_purchase_order(request):
	if request.method == 'POST':
		if request.user.is_authenticated:
			print(request.POST)

			form = CarpetPurchaseOrderEditForm(request.POST)

			if form.is_valid():
				order_id = request.POST.get('id', '')
				length = request.POST.get('length', '0.00')
				width = request.POST.get('width', '0.00')
				height = request.POST.get('height', '0.00')
				quantity = request.POST.get('quantity', '')
				pattern_id = request.POST.get('pattern', '')
				waterproof = request.POST.get('waterproof', '')
				underlay = request.POST.get('underlay', '')
				edge = request.POST.get('edge', '')

				if order_id != '' and quantity != '' and pattern_id != '' and waterproof != '' and underlay != '' and edge != '':
					order_id = int(order_id)
					length = Decimal(length)
					width = Decimal(width)
					height = Decimal(height)
					quantity = int(quantity)
					pattern_id = int(pattern_id)
					waterproof = True if (waterproof == 'true') else False
					underlay = True if (underlay == 'true') else False
					edge = True if (edge == 'true') else False

					modified_by = request.user

					try:
						carpet_purchase_order = CarpetPurchaseOrder.objects.get(id = order_id)
						pattern = CarpetPattern.objects.get(id = pattern_id)
					except ObjectDoesNotExist:
						return JsonResponse({'result': False}, safe=False)

					if carpet_purchase_order.member.user == modified_by:
						carpet_purchase_order.edit(length, width, height, modified_by, quantity, pattern, waterproof, underlay, edge)
						response = {
							'result': True
						}
						response['data'] = carpet_purchase_order.order_ptr.get_detail(False)
						return JsonResponse(response, safe=False)
					else:
						return JsonResponse({'result': False}, safe=False)
			else:
				return JsonResponse({'result': False, 'message': 'Invalid input'}, safe=False)

			return JsonResponse({'result': True}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def can_delete_order(request):
	if request.method == 'POST':
		if request.user.is_authenticated:

			order_id = request.POST.get('id', '')

			if order_id != '':
				order_id = int(order_id)

				try:
					member = request.user.member
					order = Order.objects.get(id = order_id)
				except ObjectDoesNotExist:
					return JsonResponse({'result': False}, safe=False)

				if member == order.member:
					removable_data = order.get_removable_data()
					if removable_data['removable'] == True:
						return JsonResponse({'result': True, 'only_related': removable_data['only_related']}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def delete_order(request):
	if request.method == 'POST':
		if request.user.is_authenticated:

			order_id = request.POST.get('id', '')

			if order_id != '':
				order_id = int(order_id)

				try:
					member = request.user.member
					order = Order.objects.get(id=request.POST.get('id'))
				except ObjectDoesNotExist:
					return JsonResponse({'result': False}, safe=False)

				if member == order.member:
					try:
						order.remove(request.user)
					except Exception as e:
						return JsonResponse({'result': False, 'message': str(e)}, safe=False)
					
				return JsonResponse({'result': True}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def view_profile(request):
	if request.method == 'POST':
		form  = EditUserForm(request.POST, instance=request.user)
		memberform  = EditMemberForm(request.POST, instance=request.user.member)
		boatform = MemberBoatForm(request.user.member.id, None)
		forms = {
			'form': form,
			'memberform': memberform,
			'boatform': boatform
		}
		if form.is_valid() and memberform.is_valid():
			request.user.member.name =request.user.last_name + " " + request.user.first_name
			form.save()
			memberform.save()
			# return render(request, 'wingwoo/view_profile.html', forms) 
			return JsonResponse({'result': True},safe=False)
	else:
		form = EditUserForm(instance=request.user)
		memberform  = EditMemberForm( instance=request.user.member)
		boatform = MemberBoatForm(request.user.member.id, None)
		forms = {
			'form': form,
			'memberform': memberform,
			'boatform': boatform,
		}

	forms['page'] = 'profile'
	return render(request, 'wingwoo/view_profile.html', forms)

def can_delete_booking(request):
	if request.method == 'POST':
		if request.user.is_authenticated:

			booking_id = request.POST.get('id', '')

			if booking_id != '':
				booking_id = int(booking_id)

				try:
					member = request.user.member
					booking = MeasurementBooking.objects.get(id = booking_id)
				except ObjectDoesNotExist:
					return JsonResponse({'result': False}, safe=False)

				if member == booking.member:
					removable = booking.is_deletable()['result']
					return JsonResponse({'result': removable}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def delete_booking(request):
	if request.method == 'POST':
		if request.user.is_authenticated:

			booking_id = request.POST.get('id', '')

			if booking_id != '':
				booking_id = int(booking_id)

				try:
					member = request.user.member
					booking = MeasurementBooking.objects.get(id = booking_id)
				except ObjectDoesNotExist:
					return JsonResponse({'result': False}, safe=False)

				if member == booking.member:
					try:
						booking.remove(request.user)
					except Exception as e:
						return JsonResponse({'result': False, 'message': str(e)}, safe=False)
					
					return JsonResponse({'result': True}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def edit_booking(request):
	if request.method == 'POST':
		if request.user.is_authenticated:
			booking_id = request.POST.get('booking_id', '')
			date = request.POST.get('date', '')
			time = request.POST.get('timeslot', '')

			if booking_id != '' and date != '' and time != '':
				booking_id = int(booking_id)

				try:
					member = request.user.member
					booking = MeasurementBooking.objects.get(id = booking_id)
				except ObjectDoesNotExist:
					return JsonResponse({'result': False}, safe=False)

				date = datetime.datetime.strptime(date + ' ' + time, '%Y-%m-%d %H:%M:%S')
				timeslot = OccupiedTimeslot.get_timeslot_choice(time)

				if timeslot != '' and member == booking.member:
					try:
						booking.edit(date, timeslot)
					except Exception as e:
						return JsonResponse({'result': False, 'message': str(e)}, safe=False)

					return JsonResponse({'result': True}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def create_receivable(request):
	if request.method == 'POST':
		if request.user.is_authenticated and request.user.is_staff:
			order_id = request.POST.get('order_id', '')

			if order_id != '':
				order_id = int(order_id)

				try:
					order = Order.objects.get(id = order_id)
				except ObjectDoesNotExist:
					return JsonResponse({'result': False}, safe=False)

				try:
					order.create_receivable()
				except Exception as e:
					return JsonResponse({'result': False, 'message': str(e)}, safe=False)

				return JsonResponse({'result': True}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def admin_member_log(request):
	if request.user.is_authenticated and request.user.is_staff:
		context = admin.site.each_context(request)
		context.update({
			'title': 'Member Logs'
		})

		template = 'admin/memberlog.html'
		return render(request, template, context)

	raise Http404()

def get_filtered_log_queryset(filter_type, filter_value):
	logs = MemberLog.objects.all().order_by('created_at')

	if filter_type != '' and filter_value != '':
		filter_type = int(filter_type)

		if filter_type == 0:
			filter_value = int(filter_value)
			member = Member.objects.get(id = filter_value)
			logs = logs.filter(member = member)
		else:
			logs = logs.filter(member__name__contains = filter_value)

	return logs

def admin_get_membet_log(request):
	if request.method == 'POST':
		if request.user.is_authenticated and request.user.is_staff:
			filter_type = request.POST.get('filter_type', '')
			filter_value = request.POST.get('filter_value', '')

			try:
				logs = get_filtered_log_queryset(filter_type, filter_value)
				logs = logs.order_by('-created_at')

				page_index = int(request.POST.get('page', 1))
				paginator = Paginator(logs, 30)

				if paginator.num_pages < page_index:
					return JsonResponse({'result': False, 'message': 'No more log entry'}, safe=False)

				if page_index > paginator.num_pages:
					page_index = paginator.num_pages

				logs = paginator.page(page_index)

				data = []

				for log in logs:
					data.append(str(log))

				return JsonResponse({'result': True, 'data': data}, safe=False)
			except Exception as e:
				return JsonResponse({'result': False, 'message': str(e)}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def export_member_log(request):
	if request.user.is_authenticated and request.user.is_staff:
		try:
			logs = get_filtered_log_queryset('', '')
		except:
			raise Http404()

		return render(request, 'admin/memberlog_export.txt', {'logs': logs}, content_type="text/plain")

	raise Http404()

def export_member_log_id(request, member_id):
	if request.user.is_authenticated and request.user.is_staff:
		try:
			logs = get_filtered_log_queryset(0, member_id)
		except:
			raise Http404()

		return render(request, 'admin/memberlog_export.txt', {'logs': logs}, content_type="text/plain")

	raise Http404()

def export_member_log_name(request, member_name):
	if request.user.is_authenticated and request.user.is_staff:
		try:
			logs = get_filtered_log_queryset(1, member_name)
		except:
			raise Http404()

		return render(request, 'admin/memberlog_export.txt', {'logs': logs}, content_type="text/plain")

	raise Http404()

def admin_contact_message(request):
	if request.user.is_authenticated and request.user.is_staff:
		context = admin.site.each_context(request)
		context.update({
			'title': 'Contact Message'
		})

		template = 'admin/contactmessage.html'
		return render(request, template, context)

	raise Http404()

def get_contact_message(request, read):
	if request.method == 'POST' and request.user.is_authenticated and request.user.is_staff:
		data = []

		queries = [Q(read = bool(read))]

		filter_id = request.POST.get('id', '')
		filter_name = request.POST.get('name', '')
		filter_email = request.POST.get('email', '')
		filter_content = request.POST.get('content', '')
		filter_created_at = request.POST.get('created_at', '')

		if filter_id != '': queries.append(Q(id = int(filter_id)))
		if filter_name != '': queries.append(Q(name__contains = filter_name))
		if filter_email != '': queries.append(Q(email__contains = filter_email))
		if filter_content != '': queries.append(Q(content__contains = filter_content))
		if filter_created_at != '':
			date = datetime.datetime.strptime(filter_created_at, '%Y-%m-%d')
			datetime_range = (
				datetime.datetime.combine(date, datetime.time.min),
				datetime.datetime.combine(date, datetime.time.max)
			)
			queries.append(Q(created_at__range = datetime_range))

		query = queries.pop()
		for item in queries:
			query &= item

		messages = ContactMessage.objects.filter(query).order_by("-id")

		sort_field = request.POST.get('sortField', '')
		sort_order = request.POST.get('sortOrder', '')

		if (sort_field != '' and sort_order != ''):
			if sort_order == 'desc':
				sort_field = '-' + sort_field

			messages = messages.order_by(sort_field)

		total_count = messages.count()

		page_index = int(request.POST.get('pageIndex', 1))
		page_size = int(request.POST.get('pageSize', 10))

		paginator = Paginator(messages, page_size)

		if page_index > paginator.num_pages:
			page_index = paginator.num_pages

		messages = paginator.page(page_index)

		for message in messages:
			detail = message.get_detail()
			data.append(detail)

		context = {
			'message' : 'ok',
			'data': data,
			'total_count': total_count
		}

		return JsonResponse(context, safe=False)

	return JsonResponse({'result': False}, safe=False)

def read_contact_message(request):
	if request.method == 'POST' and request.user.is_authenticated and request.user.is_staff:
		message_id = request.POST.get('message_id', '')

		if message_id != '':
			message_id = int(message_id)

			try:
				message = ContactMessage.objects.get(id = message_id)
				message.process_read()
			except ObjectDoesNotExist:
				return JsonResponse({'result': False}, safe=False)

			return JsonResponse({'result': True}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def unread_contact_message(request):
	if request.method == 'POST' and request.user.is_authenticated and request.user.is_staff:
		message_id = request.POST.get('message_id', '')

		if message_id != '':
			message_id = int(message_id)

			try:
				message = ContactMessage.objects.get(id = message_id)
				message.process_unread()
			except ObjectDoesNotExist:
				return JsonResponse({'result': False}, safe=False)
			
			return JsonResponse({'result': True}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def delete_contact_message(request):
	if request.method == 'POST' and request.user.is_authenticated and request.user.is_staff:
		message_id = request.POST.get('message_id', '')

		if message_id != '':
			message_id = int(message_id)

			try:
				message = ContactMessage.objects.get(id = message_id)
				message.delete()
			except ObjectDoesNotExist:
				return JsonResponse({'result': False}, safe=False)
			
			return JsonResponse({'result': True}, safe=False)

	return JsonResponse({'result': False}, safe=False)

def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('view_profile')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'registration/change_password.html', {'form': form})	