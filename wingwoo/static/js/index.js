$(document).ready(function() {
    $('#fullpage').fullpage({
        anchors: ['top', 'ourvision', 'products', 'howwework', 'contact', 'bottom'],
        navigation: true,
        css3: true,
        fitToSection: true,
        navigationPosition: 'right',
        navigationTooltips: ['Top', 'Our Vision', 'Products', 'How We Work', 'Contact', 'Bottom'],
        responsiveWidth: 992,
        onLeave: function(index, nextIndex, direction) {
            if (index <= 5)
                $("#top-nav").children().eq(index - 1).removeClass("active")
            if (nextIndex <=5)
                $("#top-nav").children().eq(nextIndex - 1).addClass("active")
        }
    });
});