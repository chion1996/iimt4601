function getCookie(c_name)
{
    if (document.cookie.length > 0)
    {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1)
        {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}

function generalNotify(type, message, element) {
    var options = {
        icon: 'fa fa-exclamation-triangle',
        title: '',
        message: message
    };

    var settings = {
        type: type,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        allow_dismiss: true,
        placement: {
            from: 'top',
            align: 'center'
        },
        mouse_over: true
    };

    if (element != null)
        settings['element'] = element;

    $.notify(options, settings);
}

$(document).ready(function() {
    var pull        = $('#pull');
        menu        = $('nav > ul + ul');
        menuHeight  = menu.height();
        
    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle();
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 320 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
});