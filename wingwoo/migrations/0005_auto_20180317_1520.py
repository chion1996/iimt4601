# Generated by Django 2.0.1 on 2018-03-17 07:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wingwoo', '0004_auto_20180317_0348'),
    ]

    operations = [
        migrations.CreateModel(
            name='CarpetProduct',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('shaggy_pattern_purchase_unit_price', models.DecimalField(decimal_places=2, default=0.0, max_digits=10)),
                ('pile_pattern_purchase_unit_price', models.DecimalField(decimal_places=2, default=0.0, max_digits=10)),
                ('waterproof_purchase_unit_price', models.DecimalField(decimal_places=2, default=0.0, max_digits=10)),
                ('underlay_purchase_unit_price', models.DecimalField(decimal_places=2, default=0.0, max_digits=10)),
                ('edge_purchase_unit_price', models.DecimalField(decimal_places=2, default=0.0, max_digits=10)),
            ],
        ),
        migrations.CreateModel(
            name='GeneralProduct',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('purchase_unit_price_for_color', models.DecimalField(decimal_places=2, default=0.0, max_digits=10)),
            ],
        ),
        migrations.RemoveField(
            model_name='product',
            name='purchase_unit_price_for_color',
        ),
    ]
