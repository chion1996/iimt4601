from django.urls import path, re_path
from django.contrib.auth import authenticate, get_user_model, login, logout
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('', views.index, name='index'),

    #registration
    path('register/', views.process_register , name='register'),
    path('login/', views.process_login, name='login'),
    path('logout/', auth_views.logout, {'next_page': '/'}, name='logout'),

    #navigation
    path('about/',views.about, name='about'),
    path('products/',views.products, name='products'),
    path('products/<str:category>/', views.product_detail, name="product_detail"),
    path('contact/',views.contact, name='contact'),

    #Form
    path('generalpurchaseorder/<str:category>/',views.generalPurchaseOrderForm, name="generalpurchaseorder"),
    path('carpetpurchaseorder/',views.carpetPurchaseOrderForm, name="carpetpurchaseorder"),
    path('repairorder/<str:category>/',views.repairOrderForm, name="repairorder"),
    path('boat/', views.boat, name="boat"),
    path('remove_boat/', views.remove_boat, name="remove_boat"),
    path('edit_boat', views.edit_boat, name='edit_boat'),
    path('get_boat', views.get_boat, name='get_boat'),
    path('getform/', views.get_form, name='getform'),

    #Other methods
    path('orders/', views.orders, name='orders'),
    path('vieworders/<int:is_paid>/<int:is_purchase>/', views.vieworders, name='vieworders'),
    path('timeslotok/', views.is_datetime_available, name='timeslotok'),
    path('schedule/', views.schedule, name='schedule'),
    path('timeslotdetail/', views.get_timeslot_detail, name='timeslotdetail'),
    path('getorderoptions/', views.get_order_options, name='getorderoptions'),
    path('quoteorder/', views.quote_order, name='quoteorder'),
    path('madeorder/', views.made_order, name='madeorder'),
    path('receivedpayment/', views.received_payment, name='receivedpayment'),
    path('deliveredorder/', views.delivered_order, name='deliveredorder'),
    path('reverseorderstatus/', views.reverse_order, name='reverseorder'),
    path('finishedbooking/', views.finished_booking, name='finishedbooking'),

    path('getpricedetail/', views.get_price_detail, name='getpricedetail'),
    path('createbooking/', views.create_booking, name='createbooking'),
    path('removeupload/', views.remove_upload, name='removeupload'),
    
    path('editrepairorder/', views.edit_repair_order, name='editrepairorder'),
    path('editgeneralpurchaseorder/', views.edit_general_purchase_order, name='editgeneralpurchaseorder'),
    path('editcarpetpurchaseorder/', views.edit_carpet_purchase_order, name='editcarpetpurchaseorder'),
    
    path('candeleteorder/', views.can_delete_order, name="candeleteorder"),
    path('deleteorder/',views.delete_order, name="deleteorder"),
    
    path('password/', views.change_password, name='change_password'),
    path('password_reset/', auth_views.password_reset, name='password_reset'),
    path('password_reset/done/', auth_views.password_reset_done, name='password_reset_done'),
    re_path('reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    path('reset/done/', auth_views.password_reset_complete, name='password_reset_complete'),

    path('viewbookings/', views.view_bookings, name='view_bookings'),

    path('viewprofile/', views.view_profile, name='view_profile'),

    path('candeletebooking/', views.can_delete_booking, name="candeletebooking"),
    path('deletebooking/',views.delete_booking, name="deletebooking"),
    path('editbooking/', views.edit_booking, name="editbooking"),

    path('adminorders/<int:status>/', views.admin_orders, name="adminorders"),
    path('createreceivable/', views.create_receivable, name="createreceivable"),

    path('admin/memberlog/', views.admin_member_log, name="memberlog"),
    path('admin/memberlog/get/', views.admin_get_membet_log, name="getmemberlog"),
    path('admin/memberlog/export/', views.export_member_log, name="exportmemberlog"),
    path('admin/memberlog/export/<int:member_id>', views.export_member_log_id, name="exportmemberlogid"),
    path('admin/memberlog/export/<str:member_name>', views.export_member_log_name, name="exportmemberlogname"),

    path('admin/contactmessage/', views.admin_contact_message, name="contactmessage"),
    path('admin/getcontactmessages/<int:read>/', views.get_contact_message, name="getcontactmessage"),
    path('admin/readcontactmessage/', views.read_contact_message, name="readcontactmessage"),
    path('admin/unreadcontactmessage/', views.unread_contact_message, name="unreadcontactmessage"),
    path('admin/deletecontactmessage/', views.delete_contact_message, name="deletecontactmessage"),
]

"""path('products/cover/' ,views.cover, name='cover'),
    path('products/top/' ,views.top, name='top'),
    path('products/cushion/' ,views.cushion, name='cushion'),"""