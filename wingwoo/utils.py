import datetime, time
from decimal import *
from django.core.mail import send_mail

def sort_dimension(length, width, height):
	numbers = [length, width, height]
	numbers.sort(key = float, reverse=True)
	return numbers

def get_max_length(length, width, height):
	numbers = sort_dimension(length, width, height)
	return Decimal(numbers[0])

def get_max_area(length, width, height):
	numbers = sort_dimension(length, width, height)
	return Decimal(numbers[0] * numbers[1])

def get_max_perimeter(length, width, height):
	numbers = sort_dimension(length, width, height)
	return Decimal((numbers[0] + numbers[1]) * Decimal(2.0))

def utc2local(utc):
	epoch = time.mktime(utc.timetuple())
	offset = datetime.datetime.fromtimestamp (epoch) - datetime.datetime.utcfromtimestamp (epoch)
	return utc + offset